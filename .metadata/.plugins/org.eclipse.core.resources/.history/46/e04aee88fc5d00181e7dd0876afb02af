'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from Codecamy_Python_Library import setChapter
from Codecamy_Python_Library import setLesson
from Codecamy_Python_Library import printChapter
from Codecamy_Python_Library import printLesson
from Codecamy_Python_Library import incrementLevel
from Codecamy_Python_Library import decrementLevel
from Codecamy_Python_Library import printOutput
from Codecamy_Python_Library import printNewLine

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter( "Loops", 8 )
printChapter()

setLesson( "Loops", 1 )
printLesson()

# Set Scope level
incrementLevel()

useFunction = False

# 1 While you're here
printOutput( "While you're here" )
incrementLevel()

count = 0

if count < 10:
    printOutput( "Hello, I am an if statement and count is %d" % count )

while count < 10:
    printOutput( "Hello, I am a while and count is %d" % count )
    count += 1

decrementLevel()

# 2 Condition
printOutput( "Condition" )
incrementLevel()

loop_condition = True

while loop_condition:
    printOutput( "I am a loop" )
    loop_condition = False

decrementLevel()

# 3 While you're at it
printOutput( "While you're at it" )
incrementLevel()

count = 1

while count <= 10:
    printOutput( '%d squared is %d' % ( count, count ** 2 ) )
    count += 1

decrementLevel()

# 4 Simple errors
printOutput( "Simple errors" )
incrementLevel()

def enjoying():
    choiceEnjoying = raw_input( '\t\tEnjoying the course? (y/n): ' )
    printOutput( "Initial choice %s" % choiceEnjoying )
    # Fill in the condition (before the colon)y
    while ( ( choiceEnjoying.upper() != "Y" ) and ( choiceEnjoying.upper() != "N" ) and ( choiceEnjoying.upper() != "YES" ) and ( choiceEnjoying.upper() != "NO" ) ):
        choiceEnjoying = raw_input( "\t\tSorry, I didn't catch that. Enter again: " )
        printOutput( "Loop choice %s" % choiceEnjoying )

    printOutput( "Your successful choice was %s" % choiceEnjoying )

if ( useFunction ) :
    enjoying()
else :
    printOutput( "Skipping output so no user input is not needed." )

decrementLevel()

# 5 Infinite loops
printOutput( "Infinite loops" )
incrementLevel()
count = 0

while count < 10:
    printOutput( "Your count is %s" % count )
    count += 1

decrementLevel()

# 6 Break
printOutput( "Break" )
incrementLevel()
count = 0

while True:
    printOutput( "Your count is %s" % count )
    count += 1
    if count >= 10:
        break

decrementLevel()

# 7 While / else
printOutput( "While / else" )
incrementLevel()
import random

printOutput( "Lucky Numbers! 3 numbers will be generated." )
printOutput( "If one of them is a '5', you lose!" )

count = 0
while count < 3:
    num = random.randint( 1, 6 )
    printOutput( "Your number is %s" % num )
    if num == 5:
        printOutput( "Sorry, you lose!" )
        break
    count += 1
else:
    printOutput( "You win!" )

decrementLevel()

# 8 Your own while / else
printOutput( "Your own while / else" )
incrementLevel()

from random import randint

def guessing():
    # Generates a number from 1 through 10 inclusive
    random_number = randint( 1, 10 )

    guesses_left = 3
    # Start your game!
    while guesses_left > 0:
        guess = int( raw_input( "\t\tYour guess: " ) )
        if random_number == guess:
            printOutput( "You win!" )
            break
        guesses_left -= 1
    else:
        printOutput( "Sorry, you lose!" )

if ( useFunction ) :
    guessing()
else :
    printOutput( "Skipping output so no user input is not needed." )


decrementLevel()

# 9 For your health
printOutput( "For your health" )
incrementLevel()

printOutput( "Counting..." )

for i in range( 20 ):
    printOutput( "Your count is %s" % i )

decrementLevel()

# 10 For your hobbies
printOutput( "For your hobbies" )
incrementLevel()

def hobbies():
    hobbiesList = []

    for i in range( 3 ):
        hobby = str( raw_input( "\t\tTell me one of your favorite hobbies:  " ) )
        printOutput( "Your hobby is %s" % hobby )
        hobbiesList.append( hobby )

    printOutput( hobbiesList )

if ( useFunction ) :
    hobbies()
else :
    printOutput( "Skipping output so no user input is not needed." )

decrementLevel()

# 11 For your strings
printOutput( "For your strings" )
incrementLevel()

thing = "spam!"
printOutput( "Your word is %s" % thing )

for char in thing:
    printOutput( "Your character is %s" % char )

word = "eggs!"
printOutput( "Your word is %s" % word )

for char in word:
    printOutput( "Your character is %s" % char )

decrementLevel()

# 12 For your A
printOutput( "For your A" )
incrementLevel()

phrase = "A bird in the hand..."
printOutput( "Your phrase is %s" % phrase )

newString = ""
for char in phrase:
    if char.upper() == "A":
        print "X",
        newString += "X"
    else :
        print char,
        newString += char

printNewLine()

printOutput( "Your new string is %s" % newString )

decrementLevel()

# 13 For your lists
printOutput( "For your lists" )
incrementLevel()

numbers = [7, 9, 12, 54, 99]

print "This list contains: "
printOutput( numbers )

for num in numbers:
    printOutput( "Your number is %d" % num )

for num in numbers:
    printOutput( "Your number squared is  is %d" % num ** 2 )
decrementLevel()

# 14 Looping over a dictionary
printOutput( "Looping over a dictionary" )
incrementLevel()

fruitDictionary = {'a': 'apple', 'b': 'berry', 'c': 'cherry'}

printOutput( "Standard Dictionary" )
printOutput( fruitDictionary )
for key in fruitDictionary:
    printOutput( 'Key: %s Value: %s' % ( key, fruitDictionary[key] ) )

import collections

printOutput( "Ordered Dictionary" )

fruitTupple = [( "a", "apple" ),
        ( "b", "berry" ),
        ( "c", "cherry" )]

newFruitDictionary = collections.OrderedDict( fruitTupple )
printOutput( newFruitDictionary )

for key in newFruitDictionary:
    printOutput( 'Key: %s Value: %s' % ( key, newFruitDictionary[key] ) )

decrementLevel()

# 15 Counting as you go
printOutput( "Counting as you go" )
incrementLevel()
choices = ['pizza', 'pasta', 'salad', 'nachos']

printOutput( "Your choices are:" )
printOutput( choices )
for index, item in enumerate( choices ):
    printOutput( 'Key: %s Value: %s' % ( index + 1, item ) )

decrementLevel()

# 16 Multiple lists
printOutput( "Multiple lists" )
incrementLevel()

list_a = [3, 9, 17, 15, 19]
list_b = [2, 4, 8, 10, 30, 40, 50, 60, 70, 80, 90]

for a, b in zip( list_a, list_b ):
    if a >= b :
        printOutput( "The element in list_a '%d' is greater or equal the element in list_b. '%d'" % ( a, b ) )
    else :
        printOutput( "The element in list_a '%d' is less than the element in list_b. '%d'" % ( a, b ) )
decrementLevel()

# 17 For / else
printOutput( "For / else" )
incrementLevel()

def fruitTest( FRUIT_LIST ):
    printOutput( "Your fruits are:" )
    printOutput( FRUIT_LIST )

    printOutput( "You have..." )
    for f in FRUIT_LIST:
        if f == 'tomato':
            printOutput( "A tomato is not a fruit!" )  # (It actually is.)
            break
        printOutput( "A %s" % f )

    else:
        printOutput( "A fine selection of fruits!" )

fruits = ['banana', 'apple', 'orange', 'tomato', 'pear', 'grape']
fruitTest( fruits )

decrementLevel()

# 18 Change it up
printOutput( "Change it up" )
incrementLevel()

fruits2 = ['banana', 'apple', 'orange', 'pear', 'grape']
fruitTest( fruits2 )

decrementLevel()

# 19 Create your own
printOutput( "Create your own" )
incrementLevel()

def divdeTest( COUNT, DIVIDE_BY ):
    printOutput( "Test to see if %d numbers are divisible by '%d'" % ( COUNT, DIVIDE_BY ) )
    for i in range( COUNT ):
        number = int( raw_input( "\t\tEnter a number:  " ) )
        if ( ( number % 3 ) == 0 ):
            printOutput( "Your number '%d' is divisible by '%d'" % ( number, DIVIDE_BY ) )
            break
        else :
            printOutput( "Your number '%d' is not divisible by '%d'" % ( number, DIVIDE_BY ) )
    else:
        printOutput( "None of your numbers are divisible by '%d'" % ( DIVIDE_BY ) )

if ( useFunction ) :
    divdeTest( 3, 3 )
else :
    printOutput( "Skipping output so no user input is not needed." )

decrementLevel()

# Decrement Scope level
decrementLevel()
