'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from Codecamy_Python_Library import setChapter
from Codecamy_Python_Library import setLesson
from Codecamy_Python_Library import printChapter
from Codecamy_Python_Library import printLesson
from Codecamy_Python_Library import incrementLevel
from Codecamy_Python_Library import decrementLevel
from Codecamy_Python_Library import printOutput
from Codecamy_Python_Library import determine_type

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter( "Lists and Functions", 7 )
printChapter()

setLesson( "Lists and Functions", 1 )
printLesson()

# Set Scope level
incrementLevel()

# 1 List accessing
printOutput( "List accessing" )
incrementLevel()

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( numbers[1], LABEL="2nd element: ", DESCRIPTIVE="" )

decrementLevel()

# 2 List element modification
printOutput( "List element modification" )
incrementLevel()

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbers[1] = numbers[1] * 5

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

decrementLevel()

# 3 Appending to a list
printOutput( "Appending to a list" )
incrementLevel()

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbers.append( 4 )

printOutput( numbers, LABEL="numbers.append: ", DESCRIPTIVE="" )

decrementLevel()

# 4 Removing elements from lists
printOutput( "Removing elements from lists" )
incrementLevel()

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( numbers.pop( 0 ), LABEL="numbers.pop", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( numbers.remove( 5 ), LABEL="numbers.remove(5)", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

del( numbers[0] )

printOutput( numbers, LABEL="del()", DESCRIPTIVE="" )

decrementLevel()

# 5 Changing the functionality of a function
printOutput( "Changing the functionality of a function" )
incrementLevel()

def my_function( x ):
    return x * 3

number = 5

printOutput( number, LABEL="number: ", DESCRIPTIVE="" )
printOutput( my_function( number ), LABEL="my_function: ", DESCRIPTIVE="" )

decrementLevel()

# 6 More than one argument
printOutput( "More than one argument" )
incrementLevel()

def add_function ( x, y ):
    return x + y

number1 = 5
number2 = 13

printOutput( number1, LABEL="number1: ", DESCRIPTIVE="" )
printOutput( number2, LABEL="number2: ", DESCRIPTIVE="" )
printOutput( add_function( number1, number2 ), LABEL="add_function: ", DESCRIPTIVE="" )

decrementLevel()

# 7 Strings in functions
printOutput( "Strings in functions" )
incrementLevel()

def string_function( value ):
    return determine_type( value ) + "world"

printOutput( "hello", LABEL="string: ", DESCRIPTIVE="" )
printOutput( string_function( "hello" ), LABEL="string_function", DESCRIPTIVE="" )
printOutput( string_function( 5 ), LABEL="string_function", DESCRIPTIVE="" )

decrementLevel()

# 8 Passing a list to a function
printOutput( "Passing a list to a function" )
incrementLevel()

def list_function( x ):
    return x

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( list_function( numbers ), LABEL="list_function: ", DESCRIPTIVE="" )

decrementLevel()

# 9 Using an element from a list in a function
printOutput( "Using an element from a list in a function" )
incrementLevel()

def list_function_1( x ):
    return x[1]

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( list_function_1( numbers ), LABEL="list_function_1: ", DESCRIPTIVE="" )

decrementLevel()

# 10 Using an element from a list in a function
printOutput( "Using an element from a list in a function" )
incrementLevel()

def list_function_2( x ):
    x[1] = x[1] + 3
    return x

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbersNew = list_function_2( numbers )

printOutput( numbersNew, LABEL="numbersNew: ", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

decrementLevel()

# 11 List manipulation in functions
printOutput( "List manipulation in functions" )
incrementLevel()

def list_extender( lst ):
    lst.append( 9 )
    return lst

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbersNew = list_extender( numbers )

printOutput( numbersNew, LABEL="numbersNew: ", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

decrementLevel()

# 12 Printing out a list item by item in a function
printOutput( "Printing out a list item by item in a function" )
incrementLevel()

def print_list( x ):
    incrementLevel()
    for i in range( 0, len( x ) ):
        printOutput( x[i], LABEL="x[i]:", DESCRIPTIVE="" )
    decrementLevel()

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
print_list( numbers )

decrementLevel()

# 13 Modifying each element in a list in a function
printOutput( "Modifying each element in a list in a function" )
incrementLevel()

def double_list( x ):
    for i in range( 0, len( x ) ):
        x[i] = x[i] * 2
    return x

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
numbersNew = double_list( numbers )

printOutput( numbersNew, LABEL="numbersNew: ", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

decrementLevel()

# 14 Passing a range into a function
printOutput( "Passing a range into a function" )
incrementLevel()

def my_function_1( x ):
    for i in range( 0, len( x ) ):
        x[i] = x[i] * 2
    return x

printOutput( range( 3 ) , LABEL="range: ", DESCRIPTIVE="" )
printOutput( my_function_1( range( 3 ) ) , LABEL="printLessonCounterValue: ", DESCRIPTIVE="" )

decrementLevel()

# 15 Iterating over a list in a function
printOutput( "Iterating over a list in a function" )
incrementLevel()

def total ( lst ) :
    result = 0
    for item in lst :
        result += item
    return result

numbers = [3, 5, 7]

printOutput( numbers , LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( total( numbers ) , LABEL="total: ", DESCRIPTIVE="" )

decrementLevel()

# 16 Using strings in lists in functions
printOutput( "Using strings in lists in functions" )
incrementLevel()

def join_strings ( words ) :
    result = ""
    for item in words :
        result += item
    return result

strings = ["Michael", "Lieberman"]

printOutput( strings , LABEL="strings: ", DESCRIPTIVE="" )
printOutput( join_strings( strings ) , LABEL="join_strings: ", DESCRIPTIVE="" )

decrementLevel()

# 17 Using two lists as two arguments in a function
printOutput( "Using two lists as two arguments in a function" )
incrementLevel()

def join_lists( list_1, list_2 ):
    return list_1 + list_2

numbers1 = [1, 2, 3]
numbers2 = [4, 5, 6]

printOutput( numbers1 , LABEL="numbers1: ", DESCRIPTIVE="" )
printOutput( numbers2 , LABEL="numbers2: ", DESCRIPTIVE="" )
printOutput( join_lists( numbers1, numbers2 ) , LABEL="join_lists: ", DESCRIPTIVE="" )

decrementLevel()

# 18 Using two lists as two arguments in a function
printOutput( "Using two lists as two arguments in a function" )
incrementLevel()

def flatten ( list_outer ):
    result = []
    for item in list_outer:
        result += item
    return result

numbers = [[1, 2, 3], [4, 5, 6, 7, 8, 9]]

printOutput( numbers , LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( flatten( numbers ) , LABEL="flatten: ", DESCRIPTIVE="" )

decrementLevel()

# Decrement Scope level
decrementLevel()
