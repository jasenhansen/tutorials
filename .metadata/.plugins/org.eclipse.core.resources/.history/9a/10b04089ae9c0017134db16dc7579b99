#####################################################
#
# Path Merge tool: Old master + Transactions -> New master
#
# 17.8.15: Original
# 17.9.13: Modify replace to create new if does not exist
#
#####################################################
# https://stackoverflow.com/questions/5658369/how-to-input-a-regex-in-string-replace
# TODO: line = re.sub(r"</?\[\d+>", "", line) see example above
HELP = """
    python PathMerge.py oldMaster.json transaction.json newMaster.json
"""
import json,os,sys,re

OLD_MASTER = None
INTEGER = re.compile("^[-+]?[0-9]+$")
def isInteger(s): return (INTEGER.match(s) is not None)
def isMatchExpr(s): return len(s.split("=")) == 2
def loadJson(path): return json.loads(open(path, 'r').read())

######################################################################
# Update old master file with transaction file giving new master file
######################################################################
def update(pathMasterOld, pathTransaction):
    global OLD_MASTER
    newMaster = loadJson(pathMasterOld)
    OLD_MASTER = loadJson(pathMasterOld)
    transaction = loadJson(pathTransaction)

    for request in transaction["requests"]:
        path = request["path"].split(".")
        execAction(request,path,newMaster)

    return json.dumps(newMaster, indent=2)

def extendArray(array):
    cell = array[0]
    if isinstance(cell,list):
        array.append([])
    elif isinstance(cell,dict):
        array.append({})
    else:
        raise Exception("Cannot create new instance of type " + type(cell))

def fromArray(array, target):
    assert target.startswith("[") and target.endswith("]")
    expr = target[1:-1]
    if isInteger(expr):
        index=int(expr)
        if len(array) <= index:
            extendArray(array)
            return array[-1]
        else:
            return array[int(expr)]
    if isMatchExpr(expr):
        key, value = expr.split("=")
        matches = [block for block in array if value == block.get(key)]
        if len(matches) > 0:
            return matches[0]
        else:
            # If not found, create new object
            newObject = { key:value }
            array.append(newObject)
            return newObject

    raise Exception("Invalid list element finder expression " + target)

def findNode(root, path):
    location = root
    if len(path) == 0:
        return location
    head = path[0]
    tail = path[1:]
    if head.startswith("["):
        location = fromArray(location, head)
    else:
        location = location.get(head)
    findNode(location, tail)

def execAction(request, path, location):
    # Main function logic
    if isinstance(location, list):
#        execListAction(request, path, location)
        print("LIST")
    elif isinstance(location, dict):
        execDictAction(request, path, location)
    else:
        raise Exception("Location must be list or dict, found " + type(location))
    sys.exit(0)

# Execute action on a list
def execListAction(request, path, location):
    targetPoint = path[0]
    if (len(path) == 1):
        action = request["action"]
        payload = request["payload"]
        index = findNodeIndexInList(location, targetPoint)
        if action == "insert":
            location.insert(index, payload)
        elif action == "patch":
            location[index] = findNode(OLD_MASTER,request["source"])
        elif action == "replace":
            if location.get(index):
                location[index] = payload
            else:
                location.append(payload)
        elif action == "edit":
            assert isinstance(location[index],str), "Cannot edit non-string"
            location[index] = re.sub(request["match"], location[index], payload)
        elif action == "append":
            assert isinstance(location[index],str), "Cannot append non-string"
            location[index]+=payload
        elif action == "prepend":
            assert isinstance(location[index],str), "Cannot prepend non-string"
            location[index] = payload + location[index]
        elif action == "rtrim":
            assert isinstance(location[index], str), "Cannot append non-string"
            assert isinstance(request['payload'], int), "Cannot rtrim non-integer"
            n = int(request['payload'])
            length = len(location[index])
            location[index] = location[index][0:length-n]
        elif action == "delete":
            location.pop(index)
        elif action == "load":
            location[index] = loadJson(payload)
        elif action == "export":
            value = json.dumps(location, indent=2)
            with open(request["file"], "w") as out:
                out.write(value)
        else:
            raise Exception("Unknown action type: " + action)

    else:
        location = fromArray(location, targetPoint)
        execAction(request,path[1:],location)

# Execute action on a dict
def execDictAction(request, path, location):

    
    def extendMapCollection(location, path, payload):
        if len(path) > 1 and isInteger(path[1]):
            location[path[0]] = []
        elif len(path) > 1:
            location[path[0]] = {}
        else:
            raise Exception("DBG: What do I do here?")



    targetPoint = path[0]

    print("Start")
    print("request ", request)
    print("path ", path)
    print("location ", location)
    print("targetPoint ", targetPoint)
    print("len ", len(path))
    
    if (len(path) == 1):
        action = request["action"]        
        if action in ("insert", "replace"):
            location[targetPoint] = request['payload']
        elif action == "patch":
            location[targetPoint] = findNode(OLD_MASTER,request["source"])
        elif action == "edit":
            assert isinstance(location[targetPoint], type(str)), "Cannot edit non-string"
            location[targetPoint] = re.sub(request["match"], location[targetPoint], request['payload'])
        elif action == "append-string":
            assert isinstance(location[targetPoint],str), "Cannot append non-string"
            location[targetPoint]+=request['payload']
        elif action == "prepend-string":
            assert isinstance(location[targetPoint],str), "Cannot prepend non-string"
            location[targetPoint] = request['payload'] + location[targetPoint]
        elif action == "rtrim":
            assert isinstance(location[targetPoint], str), "Cannot append non-string"
            assert isinstance(request['payload'], int), "Cannot rtrim non-integer"
            n = int(request['payload'])
            length=len(location[targetPoint])
            location[targetPoint] = location[targetPoint][0:length-n]
        elif action == "delete":
            location.pop(targetPoint)
        elif action == "load":
            location[targetPoint] = loadJson(request["payload"])
        elif action == "export":
            value = json.dumps(location, indent=2)
            with open(request["file"], "w") as out:
                out.write(value)
        else:
            raise Exception("Unknown action type: " + action)
        
        
    else: # Continue recursion down path
        if not location.get(targetPoint):
            # If dead end, continue the path (if possible)
            extendMapCollection(location, path, request["payload"])
        execAction(request, path[1:], location[targetPoint])

    print("action ", action)
    print("payload ", request['payload'])
    print("location ", location)
    sys.exit(0)

def findNodeIndexInList(values, expr):
    assert expr.startswith("[") and expr.endswith("]"), "Not an array expression: " + expr
    assert isinstance(values,list), "findNode requires a list, got " + type(values) + ":" + values.toString()

    payload = expr[1:-1]
    if isInteger(payload):
        return int(payload)
    if isMatchExpr(payload):
        key, value = expr.split("=")
        for index, dict in enumerate(values):
            if dict[key] == value:
                return index
        raise Exception("Did not find " + expr + " in " + str(values))
    raise Exception("invalid list search exception:" + expr)







# Test defaults
here = os.path.dirname(__file__)
pathMasterOld = here + '/MasterOld.json'
pathTransactions = here + '/Transaction.json'
pathMasterNew = here + '/MasterNew.json'



TRANSACTION_ROOT = "C:/build-master/testScripts/transaction_test/transaction/"
TRANSACTION_FILE = "transaction.FORCE-RNG-ENABLED.build_history.json"

pathTransactions = TRANSACTION_ROOT + TRANSACTION_FILE


SOURCE_ROOT = "C:/build-master/testScripts/transaction_test/build_history/"
SOURCE_FILE = "oldMaster.json"
pathMasterOld = SOURCE_ROOT + SOURCE_FILE


TARGET_ROOT = "C:/build-master/testScripts/transaction_test/build_history/"
TARGET_FILE = "MasterNew.json"
pathMasterNew = TARGET_ROOT + TARGET_FILE

#if "--help" in sys.argv:
#    print(HELP)
#    sys.exit(0)
#if not "--test" in sys.argv:
#    try:
#            pathMasterOld, pathTransactions, pathMasterNew = [path for path in sys.argv if path.endswith(".json")]
#    except:
#            print(HELP)
#            print ("invalid call sequence" + str(sys.argv))
#            raise
# Setup complete.    Run.
out = update(pathMasterOld, pathTransactions)
#with open(pathMasterNew, "w") as newMaster:
#    newMaster.write(out)
#newMaster.close()