'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from Python_Library import setChapter
from Python_Library import setLesson
from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import incrementLevel
from Python_Library import decrementLevel
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter( "Python Syntax", 1 )
printChapter()

setLesson( "Tip Calculator", 2 )
printLesson()

# Set Scope level
incrementLevel()

# 1 The Meal
printOutput( "The Meal" )
incrementLevel()

meal = 44.50
printOutput( "The meal costs $%.2f" % ( meal ) )

decrementLevel()

# 2 The Tax
printOutput( "The Tax" )
incrementLevel()

meal = 44.50
tax = .0675
tax_total = meal * tax

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )

decrementLevel()

# 3 The Tip
printOutput( "The Tip" )
incrementLevel()

meal = 44.50
tax = .0675
tax_total = meal * tax
tip = .15

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )
printOutput( "The tip is {0:.0%}".format( tip ) )

decrementLevel()

# 4 Reassign in a Single Line
printOutput( "Reassign in a Single Line" )
incrementLevel()

meal = 44.50
tax = .0675
tax_total = meal * tax
sub_total = meal + tax_total
tip = .15
tip_total = sub_total * tip

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )
printOutput( "The sub_total is $%.2f" % ( sub_total ) )
printOutput( "The tip is {0:.0%}".format( tip ) )
printOutput( "The tip costs $%.2f" % ( tip_total ) )

decrementLevel()

# 5 The Total
printOutput( "The Total" )
incrementLevel()

meal = 44.50
tax = .0675
tax_total = meal * tax
sub_total = meal + tax_total
tip = .15
tip_total = sub_total * tip
total = sub_total + tip_total

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )
printOutput( "The sub total is $%.2f" % ( sub_total ) )
printOutput( "The tip is {0:.0%}".format( tip ) )
printOutput( "The tip costs $%.2f" % ( tip_total ) )
printOutput( "The total is $%.2f" % ( total ) )
decrementLevel()

# Decrement Scope level
decrementLevel()
