'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import json
import os.path

class mathReader ( object ):

    def __init__( self, sourcePathRoot, targetPathRoot ):
        self.sourcePathRoot = sourcePathRoot
        self.targetPathRoot = targetPathRoot
        self.game_dict = {}

    def addGameToGameDictionary( self, gameID ):
        self.game_dict[ gameID ] = {}
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = ''
        self.game_dict[ gameID ][ 'pathToTarget' ] = ''
        self.game_dict[ gameID ][ 'paymodels' ] = []
        self.game_dict[ gameID ][ 'playdata' ] = []
        self.game_dict[ gameID ][ 'progressivepaymodels' ] = []

    def loadJSON ( self, pathToFile, fileName ):

        with open( '%s/%s.json' % ( pathToFile, fileName ), 'r' ) as file:
            values = json.loads( file.read() )

        return values

    def insertPaymodel ( self, math, sequence, gameID ):
        paymodel = '%s.%s.json' % ( math, sequence )
        pathToPaymodel = '%s/%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math, paymodel )
        pathToPlaydata = '%s/baseline-playdata/baseline-playdata.%s' % ( self.game_dict.get( gameID ).get( 'pathToConfigurations' ), paymodel )

        if os.path.isfile( pathToPaymodel ):
            self.game_dict[ gameID ][ 'paymodels' ].append( '%s/%s' % ( math, paymodel ) )
        if os.path.isfile( pathToPlaydata ):
            self.game_dict[ gameID ][ 'playdata' ].append( 'baseline-playdata/baseline-playdata.%s' % ( paymodel ) )

    def insertProgressivePaymodel ( self, progressiveID, gameID ):
        pathToProgressivePaymodel = '%s-progressive/progressive-%s.json' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), progressiveID[ 'progressiveMathId' ] )

        if os.path.isfile( pathToProgressivePaymodel ):
            self.game_dict[ gameID ][ 'progressivepaymodels' ].append( 'progressive-%s.json' % ( progressiveID[ 'progressiveMathId' ] ) )

    def processC101Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c101Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c101Values[ 'components' ]
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID, gameID )

    def processC102Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c102Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        components = c102Values[ 'components' ]
        for component in components:
            self.processPaymodel ( component['paymodel'] )

    def processC103Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence )

        c103Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c103Values[ 'components' ]
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID )

    def processPaymodel ( self, paymodel, gameID ):
        math, sequence = paymodel.split( '.' )

        complexPaymodels = [ 'C101', 'C102', 'C103' ]
        simplePaymodels = [ 'CTR', 'D101', 'D105', 'D106', 'D107', 'DNT', 'ITD', 'P101', 'S101' ]

        if math in complexPaymodels:
            getattr( self, 'process%sPaymodel' % ( math ) )( math, sequence, gameID )
        elif math in simplePaymodels:
            self.insertPaymodel ( math, sequence, gameID )
        else:
            print ( 'Undefined math type \'%s\'' % ( math ) )

    def getConfigurationPaymodels ( self, gameID ):
        configValues = self.loadJSON ( self.game_dict.get( gameID ).get( 'pathToConfigurations' ), 'game%d' % ( gameID ) )

        if 'validMathIds' in configValues:
            paymodelID = configValues[ 'validMathIds' ]

            for paymodelIDFromConfiguration in paymodelID:
                self.processPaymodel ( paymodelIDFromConfiguration, gameID )

        if 'progressiveConfiguration' in configValues:
            progressiveID = configValues[ 'progressiveConfiguration' ][ 'progressivePaymodelMappings' ]

            for progressiveIDFromConfiguration in progressiveID:
                self.insertProgressivePaymodel ( progressiveIDFromConfiguration, gameID )

    def processGameConfiguration ( self, gameID ):

        self.addGameToGameDictionary ( gameID )
        self.game_dict[ gameID ][ 'pathToConfigurations' ] = '%s/configurations/games/%d/configs-game' % ( self.sourcePathRoot, gameID )
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = '%s/math-paymodels' % ( self.sourcePathRoot )
        self.game_dict[ gameID ][ 'pathToTarget' ] = '%s/%d' % ( self.targetPathRoot, gameID )

        self.getConfigurationPaymodels( gameID )

    def printPaymodelsGame ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'paymodels' ]:
            print ( '%s' % ( paymodel ) )

    def printPlaydataGame ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'playdata' ]:
            print ( '%s' % ( paymodel ) )

    def printProgressivePaymodelsGame ( self, gameID ):
        for progressivepaymodel in self.game_dict[ gameID ][ 'progressivepaymodels' ]:
            print ( '%s' % ( progressivepaymodel ) )

if __name__ == '__main__':

    # Place holder data until I have inputs set up
    sourcePathRoot = 'C:/build-master/.data/.staging/ts'
    targetPathRoot = 'G:/controlled/games'

    mathReaderObject = mathReader( sourcePathRoot, targetPathRoot )

#    mathReaderObject.processGameConfiguration ( 202 )
#    mathReaderObject.printPaymodelsGame ( 202 )
#    mathReaderObject.printPlaydataGame ( 202 )
#    mathReaderObject.printProgressivePaymodelsGame ( 202 )
    mathReaderObject.processGameConfiguration ( 2112 )
    mathReaderObject.printPaymodelsGame ( 2112 )
    mathReaderObject.printPlaydataGame ( 2112 )
    mathReaderObject.printProgressivePaymodelsGame ( 2112 )


#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 1002 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 1102 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 1202 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 1402 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 1802 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 1902 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 2102 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 2112 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 2302 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 2502 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 2602 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 2702 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 2902 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 3102 )
#     processGameConfiguration ( self, sourcePathRoot, targetPathRoot, 3202 )
