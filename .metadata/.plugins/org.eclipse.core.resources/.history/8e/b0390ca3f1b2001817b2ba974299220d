'''
Created on September 7, 2018

@author: Jasen Hansen
'''

from learnpython_Python_Library import setChapter
from learnpython_Python_Library import setLesson
from learnpython_Python_Library import printChapter
from learnpython_Python_Library import printLesson
from learnpython_Python_Library import incrementLevel
from learnpython_Python_Library import decrementLevel
from learnpython_Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Variables_and_Types

setChapter( "Learn the Basics", 1 )
printChapter()

setLesson( "Variables and Types", 2 )
printLesson()

# Set Scope level
incrementLevel()

# 1 Numbers
printOutput( "Numbers" )
incrementLevel()
myint = 7
printOutput( myint, DESCRIPTIVE="" )

myfloat = 7.0
printOutput( myfloat, DESCRIPTIVE="" )
myfloat = float( 7 )
printOutput( myfloat, DESCRIPTIVE="" )

decrementLevel()

printOutput( "Simple operators can be executed on numbers" )
incrementLevel()

one = 1
two = 2
three = one + two
printOutput( three, DESCRIPTIVE="" )

decrementLevel()

printOutput( "Strings" )
incrementLevel()

mystring = 'hello'
printOutput( mystring, DESCRIPTIVE="" )
mystring = "hello"
printOutput( mystring, DESCRIPTIVE="" )
mystring = "Don't worry about apostrophes"
printOutput( mystring, DESCRIPTIVE="" )

decrementLevel()

printOutput( "Simple operators can be executed on strings" )
incrementLevel()

hello = "hello"
world = "world"
helloworld = hello + " " + world
printOutput( helloworld, DESCRIPTIVE="" )

decrementLevel()

printOutput( "Assignments can be done on more than one variable 'simultaneously' on the same line like this" )
incrementLevel()

a, b = 3, 4
printOutput( a, DESCRIPTIVE="" )
printOutput( b, DESCRIPTIVE="" )

decrementLevel()

# 3 Exercise
printOutput( "Mixing operators between numbers and strings is not supported" )
incrementLevel()

# This will not work!
one = 1
two = 2
hello = "hello"

printOutput( one, DESCRIPTIVE="" )
printOutput( two, DESCRIPTIVE="" )
printOutput( hello, DESCRIPTIVE="" )

printOutput( "This will not work: '1' + '2' + 'hello'", DESCRIPTIVE="" )

decrementLevel()

# Exercise
printOutput( "Exercise" )
incrementLevel()

# change this code
mystring = "hello"
myfloat = 10.0
myint = 20

# testing code
if mystring == "hello":
    printOutput( "String: %s" % mystring, DESCRIPTIVE="" )
if isinstance( myfloat, float ) and myfloat == 10.0:
    printOutput( "Float: %f" % myfloat, DESCRIPTIVE="" )
if isinstance( myint, int ) and myint == 20:
    printOutput( "Integer: %d" % myint, DESCRIPTIVE="" )

decrementLevel()

# Decrement Scope level
decrementLevel()
