'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import json
import os.path

class mathReader ( object ):

    def __init__( self, sourcePathRoot, targetPathRoot ):
        self.sourcePathRoot = sourcePathRoot
        self.targetPathRoot = targetPathRoot
        self.game_dict = {}

    def addGameToGameDictionary( self, gameID ):
        self.game_dict[ gameID ] = {}
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = ''
        self.game_dict[ gameID ][ 'pathToTarget' ] = ''
        self.game_dict[ gameID ][ 'paymodels' ] = []
        self.game_dict[ gameID ][ 'playdata' ] = []
        self.game_dict[ gameID ][ 'progressivepaymodels' ] = []
        self.game_dict[ gameID ][ 'gameMath' ] = []

    def loadJSON ( self, pathToFile, fileName ):
        with open( '%s/%s.json' % ( pathToFile, fileName ), 'r' ) as file:
            values = json.loads( file.read() )

        return values

    def insertPaymodel ( self, math, sequence, gameID ):
        paymodel = '%s.%s.json' % ( math, sequence )
        pathToPaymodel = '%s/%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math, paymodel )
        pathToPlaydata = '%s/baseline-playdata/baseline-playdata.%s' % ( self.game_dict.get( gameID ).get( 'pathToConfigurations' ), paymodel )

        if os.path.isfile( pathToPaymodel ):
            if not '%s/%s' % ( math, paymodel ) in self.game_dict[ gameID ][ 'paymodels' ]:
                self.game_dict[ gameID ][ 'paymodels' ].append( '%s/%s' % ( math, paymodel ) )

        if os.path.isfile( pathToPlaydata ):
            if not 'baseline-playdata/baseline-playdata.%s' % ( paymodel ) in self.game_dict[ gameID ][ 'playdata' ]:
                self.game_dict[ gameID ][ 'playdata' ].append( 'baseline-playdata/baseline-playdata.%s' % ( paymodel ) )

    def insertProgressivePaymodel ( self, progressiveID, gameID ):
        pathToProgressivePaymodel = '%s-progressive/progressive-%s.json' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), progressiveID[ 'progressiveMathId' ] )

        if os.path.isfile( pathToProgressivePaymodel ):
            if not 'progressive-%s.json' % ( progressiveID[ 'progressiveMathId' ] ) in self.game_dict[ gameID ][ 'progressivepaymodels' ]:
                self.game_dict[ gameID ][ 'progressivepaymodels' ].append( 'progressive-%s.json' % ( progressiveID[ 'progressiveMathId' ] ) )

    def processC101Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c101Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c101Values[ 'components' ]
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID, gameID )

    def processC102Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c102Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        components = c102Values[ 'components' ]
        for component in components:
            self.processPaymodel ( component[ 'paymodel' ], gameID )

    def processC103Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c103Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c103Values[ 'components' ]
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID, gameID )

    def processPaymodel ( self, paymodel, gameID ):
        math, sequence = paymodel.split( '.' )

        complexPaymodels = [ 'C101', 'C102', 'C103' ]
        simplePaymodels = [ 'CTR', 'D101', 'D105', 'D106', 'D107', 'DNT', 'ITD', 'P101', 'S101' ]

        if math in complexPaymodels:
            getattr( self, 'process%sPaymodel' % ( math ) )( math, sequence, gameID )
        elif math in simplePaymodels:
            self.insertPaymodel ( math, sequence, gameID )
        else:
            print ( 'Undefined math type \'%s\'' % ( math ) )

    def processMath ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'paymodels' ]:
            math = paymodel[:paymodel.index( '/' )]

            if not  math in self.game_dict[ gameID ][ 'gameMath' ]:
                self.game_dict[ gameID ][ 'gameMath' ].append( math )


    def getConfigurationPaymodels ( self, gameID ):
        configValues = self.loadJSON ( self.game_dict.get( gameID ).get( 'pathToConfigurations' ), 'game%d' % ( gameID ) )

        if 'validMathIds' in configValues:
            paymodelID = configValues[ 'validMathIds' ]

            for paymodelIDFromConfiguration in paymodelID:
                self.processPaymodel ( paymodelIDFromConfiguration, gameID )

        if 'progressiveConfiguration' in configValues:
            progressiveID = configValues[ 'progressiveConfiguration' ][ 'progressivePaymodelMappings' ]

            for progressiveIDFromConfiguration in progressiveID:
                self.insertProgressivePaymodel ( progressiveIDFromConfiguration, gameID )

    def processGameConfiguration ( self, gameID ):
        self.addGameToGameDictionary ( gameID )
        self.game_dict[ gameID ][ 'pathToConfigurations' ] = '%s/configurations/games/%d/configs-game' % ( self.sourcePathRoot, gameID )
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = '%s/math-paymodels' % ( self.sourcePathRoot )
        self.game_dict[ gameID ][ 'pathToTarget' ] = '%s/%d' % ( self.targetPathRoot, gameID )

        self.getConfigurationPaymodels( gameID )
        self.processMath( gameID )

    def printPaymodelsGame ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'paymodels' ]:
            print ( '%s' % ( paymodel ) )

    def printPlaydataGame ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'playdata' ]:
            print ( '%s' % ( paymodel ) )

    def printProgressivePaymodelsGame ( self, gameID ):
        for progressivepaymodel in self.game_dict[ gameID ][ 'progressivepaymodels' ]:
            print ( '%s' % ( progressivepaymodel ) )

    def printGameMath ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'gameMath' ]:
            print ( '%s' % ( paymodel ) )

    def printAll ( self, gameID ):
        if gameID in self.game_dict:
            self.printPaymodelsGame ( gameID )
            self.printPlaydataGame ( gameID )
            self.printProgressivePaymodelsGame ( gameID )
            self.printGameMath ( gameID )

    
    def copyPaymodelsGame ( gameID ) ( self, gameID ):
        print ("Test")

if __name__ == '__main__':

    # Place holder data until I have inputs set up
    sourcePathRoot = 'C:/build-master/.data/.staging/ts'
    targetPathRoot = 'G:/controlled/games'

    mathReaderObject = mathReader( sourcePathRoot, targetPathRoot )

    gameID = 202
    mathReaderObject.processGameConfiguration ( gameID )
    mathReaderObject.printAll ( gameID )

#    gameID = 1002
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 1102
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 1202
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 1402
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 1802
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 1902
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 2102
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 2112
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )



#    gameID = 2302
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 2502
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 2602
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 2702
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 2902
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 3102
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )

#    gameID = 3202
#    mathReaderObject.processGameConfiguration ( gameID )
#    mathReaderObject.printAll ( gameID )
