'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from library import setChapter
from library import setLesson
from library import printChapter
from library import printLesson
from library import incrementLevel
from library import decrementLevel
from library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter("Python Syntax", 1)
printChapter()

setLesson("Python Syntax", 1)
printLesson()

# Set Scope level
incrementLevel()

# 1 Welcome!
printOutput("Welcome!")
incrementLevel()

printOutput("Welcome to Python!")
decrementLevel()

# 2 Variables
printOutput("Variables")
incrementLevel()

my_variable = 10

printOutput(my_variable, LABEL="Integer value: ", DESCRIPTIVE="")
decrementLevel()

# 3 Booleans
printOutput("Booleans")
incrementLevel()

my_int = 7
my_float = 1.23
my_bool = True

printOutput(my_int, LABEL="Integer value: ", DESCRIPTIVE="")
printOutput(my_float, LABEL="Float value: ", DESCRIPTIVE="")
printOutput(my_bool, LABEL="Boolean value: ", DESCRIPTIVE="")
decrementLevel()

# 4 You've Been Reassigned
printOutput("You've Been Reassigned")
incrementLevel()

my_int = 7

printOutput(my_int, LABEL="Initial value: ", DESCRIPTIVE="")

my_int = 3

printOutput(my_int, LABEL="Reassigned value: ", DESCRIPTIVE="")
decrementLevel()

# 5 Whitespace
printOutput("Whitespace")

# 6 Whitespace Means Right Space
printOutput("Whitespace Means Right Space")
incrementLevel()
def spam():
    eggs = 12
    return eggs

printOutput(spam(), LABEL="Function Output: ", DESCRIPTIVE="")
decrementLevel()

# 7 A Matter of Interpretation
printOutput("A Matter of Interpretation")
incrementLevel()

spam = True
eggs = False

printOutput(spam, LABEL="spam: ", DESCRIPTIVE="")
printOutput(eggs, LABEL="eggs: ", DESCRIPTIVE="")
decrementLevel()

# 8 Single Line Comments
printOutput("Single Line Comments")

# I am a comment

# 9 Multi-Line Commentss
printOutput("Multi-Line Comments")
"""
This is a multi like comment
"""

# 10 Math
printOutput("Math")
incrementLevel()

big_1 = 100
big_2 = 200
total = big_1 + big_2

printOutput(total, LABEL="total: ", DESCRIPTIVE="")
decrementLevel()

# 11 Exponentiation
printOutput("Exponentiation")
incrementLevel()

eggs = 10

printOutput(eggs, LABEL="eggs: ", DESCRIPTIVE="")

eggs = eggs ** 2

printOutput(eggs, LABEL="eggs: ", DESCRIPTIVE="")
decrementLevel()

# 12 Modulo
printOutput("Modulo")
incrementLevel()

spam = 6 % 5

printOutput(spam, LABEL="spam: ", DESCRIPTIVE="")
decrementLevel()

# 13 Bringing It All Together
printOutput("Bringing It All Together")
incrementLevel()

monty = True
python = 1.234
monty_python = python ** 2

printOutput(monty, LABEL="monty: ", DESCRIPTIVE="")
printOutput(python, LABEL="python: ", DESCRIPTIVE="")
printOutput(monty_python, LABEL="monty_python: ", DESCRIPTIVE="")
decrementLevel()

# Decrement Scope level
decrementLevel()
