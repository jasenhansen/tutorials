'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from Codecamy_Python_Library import setChapter
from Codecamy_Python_Library import setLesson
from Codecamy_Python_Library import printChapter
from Codecamy_Python_Library import printLesson
from Codecamy_Python_Library import incrementLevel
from Codecamy_Python_Library import decrementLevel
from Codecamy_Python_Library import printOutput
from Codecamy_Python_Library import functionCall
from Codecamy_Python_Library import functionCleanup

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter( "Lists & Dictionaries", 5 )
printChapter()

setLesson( "Python Lists and Dictionaries", 1 )
printLesson()

# Set Scope level
incrementLevel()

# 1 Introduction to Lists
printOutput( "Introduction to Lists" )
incrementLevel()

zoo_animals = ["pangolin", "cassowary", "sloth", "cat"];
if len( zoo_animals ) > 3:
    printOutput( "The first animal at the zoo is the: '%s'" % ( zoo_animals[0] ) )
    printOutput( "The second animal at the zoo is the: '%s'" % ( zoo_animals[1] ) )
    printOutput( "The third animal at the zoo is the: '%s'" % ( zoo_animals[2] ) )
    printOutput( "The fourth animal at the zoo is the: '%s'" % ( zoo_animals[3] ) )

decrementLevel()

# 2 Access by Index
printOutput( "Access by Index" )
incrementLevel()

numbers = [5, 6, 7, 8]

printOutput( "Adding the numbers at indices 0 (%d) and 2 (%d) is '%s'" % ( numbers[0] , numbers[2], numbers[0] + numbers[2] ) )
printOutput( "Adding the numbers at indices 1 (%d) and 3 (%d) is '%s'" % ( numbers[1] , numbers[3], numbers[1] + numbers[3] ) )

decrementLevel()

# 3 New Neighbors
printOutput( "New Neighbors" )
incrementLevel()

zoo_animals = ["pangolin", "cassowary", "sloth", "tiger"]

printOutput( "Original Zoo Animals" )
for idx, val in enumerate( zoo_animals ):
    printOutput( "Zoo Animal %d is '%s' " % ( idx + 1 , val ) )

zoo_animals[2] = "hyena"
zoo_animals[3] = "cat"

printOutput( "New Zoo Animals" )
for idx, val in enumerate( zoo_animals ):
    printOutput( "Zoo Animal %d is '%s' " % ( idx + 1 , val ) )

decrementLevel()

# 4 Late Arrivals & List Length
printOutput( "Late Arrivals & List Length" )
incrementLevel()

suitcase = []
suitcase.append( "sunglasses" )
suitcase.append( "pants" )
suitcase.append( "shoes" )
suitcase.append( "shirts" )
list_length = len( suitcase )  # Set this to the length of suitcase

printOutput( list_length, LABEL="Items in the suitcase: " )
printOutput( suitcase, LABEL="suitcase: " )

for idx, val in enumerate( suitcase ):
    printOutput( "Item %d in the suitcase is '%s' " % ( idx + 1 , val ) )

decrementLevel()

# 5 List Slicing
printOutput( "List Slicing" )
incrementLevel()

suitcase = ["sunglasses", "hat", "passport", "laptop", "suit", "shoes"]
first = suitcase[0:2]
middle = suitcase[2:4]
last = suitcase[4:6]

printOutput( "The full suitcase: '%s'" % ( suitcase ) )
incrementLevel()

for idx, val in enumerate( suitcase ):
    printOutput( "Item %d in the full suitcase is '%s' " % ( idx + 1 , val ) )

decrementLevel()

printOutput( "The first slice of the suitcase: '%s'" % ( first ) )
incrementLevel()

for idx, val in enumerate( first ):
    printOutput( "Item %d in the first slice of the suitcase is '%s' " % ( idx + 1 , val ) )

decrementLevel()

printOutput( "The middle slice of the suitcase: '%s'" % ( middle ) )
incrementLevel()

for idx, val in enumerate( middle ):
    printOutput( "Item %d in the middle slice of the suitcase is '%s' " % ( idx + 1 , val ) )

decrementLevel()

printOutput( "The last slice of the suitcase: '%s'" % ( last ) )
incrementLevel()

for idx, val in enumerate( last ):
    printOutput( "Item %d in the last slice of the suitcase is '%s' " % ( idx + 1 , val ) )

decrementLevel()

decrementLevel()

# 6 Slicing Lists and Strings
printOutput( "Slicing Lists and Strings" )
incrementLevel()

animals = "catdogfrog"
cat = animals[:3]
dog = animals[3:6]
frog = animals[6:]

printOutput( "The letters of animals: '%s'" % ( animals ) )
incrementLevel()

for idx, val in enumerate( animals ):
    printOutput( "Letter %d in animals is '%s' " % ( idx + 1 , val ) )

decrementLevel()

printOutput( "The letters of cat: '%s'" % ( cat ) )
incrementLevel()

for idx, val in enumerate( cat ):
    printOutput( "Letter %d in cat is '%s' " % ( idx + 1 , val ) )

decrementLevel()

printOutput( "The letters of dog: '%s'" % ( dog ) )
incrementLevel()

for idx, val in enumerate( dog ):
    printOutput( "Letter %d in dog is '%s' " % ( idx + 1 , val ) )

decrementLevel()

printOutput( "The letters of frog: '%s'" % ( frog ) )
incrementLevel()

for idx, val in enumerate( frog ):
    printOutput( "Letter %d in frog is '%s' " % ( idx + 1 , val ) )

decrementLevel()

decrementLevel()

# 7 Maintaining Order
printOutput( "Maintaining Order" )
incrementLevel()

animals = ["aardvark", "badger", "duck", "emu", "fennec fox"]

printOutput( "The original animals are: '%s'" % ( animals ) )

animal = "duck"
duck_index = animals.index( animal )

printOutput( "The index of '%s' animals is '%d'" % ( animal, duck_index ) )

animals.insert( duck_index, "cobra" )
printOutput( "The new animals are: '%s'" % ( animals ) )

decrementLevel()

# 8 For One and All
printOutput( "For One and All" )
incrementLevel()

my_list = [1, 9, 3, 8, 5, 7]

printOutput( "The list is: '%s'" % ( my_list ) )
incrementLevel()


decrementLevel()

decrementLevel()

# 9 More with 'for'
printOutput( "More with 'for'" )
incrementLevel()

start_list = [5, 3, 1, 2, 4]
square_list = []

printOutput( "The starting list is: '%s'" % ( start_list ) )
printOutput( "The starting square list is: '%s'" % ( square_list ) )

for item in start_list:
    square_list.append( item ** 2 )

printOutput( "The unsorted square list is: '%s'" % ( square_list ) )

square_list.sort()

printOutput( "The sorted square list is: '%s'" % ( square_list ) )

decrementLevel()

# 10 This Next Part is Key
printOutput( "This Next Part is Key" )
incrementLevel()

residents = {'Puffin' : 104, 'Sloth' : 105, 'Burmese Python' : 106}

printOutput( "The residents are: '%s'" % ( residents ) )

for idx, val in enumerate( residents ):
    printOutput( "Resident %d in residents is '%s' with an address of '%s' " % ( idx + 1, val , residents[val] ) )

resident = 'Puffin'
printOutput( "Resident '%s' has address of '%s' " % ( resident, residents[resident] ) )
resident = 'Sloth'
printOutput( "Resident '%s' has address of '%s' " % ( resident, residents[resident] ) )
resident = 'Burmese Python'
printOutput( "Resident '%s' has address of '%s' " % ( resident, residents[resident] ) )
decrementLevel()

# 11 New Entries
printOutput( "New Entries" )
incrementLevel()

menu = {}  # Empty dictionary
printOutput( "The starting menu is: '%s'" % ( menu ) )

menu['Chicken Alfredo'] = 14.50  # Adding new key-value pair
printOutput( "The modified menu is: '%s'" % ( menu ) )

menu_item = 'Chicken Alfredo'
printOutput( "The item '%s' has a price of '$%.2f' " % ( menu_item, menu[menu_item] ) )

menu['Eggplant Parmesan'] = 8.50
menu['Cordon Bleu'] = 15.00
menu['Cat'] = 5.50

printOutput( "The modified menu is: '%s'" % ( menu ) )

for idx, val in enumerate( menu ):
    printOutput( "Item %d on the menu is '%s' with an price off '$%.2f' " % ( idx + 1, val , menu[val] ) )

printOutput( "The modified menu has %s itms" % ( str( len( menu ) ) ) )

decrementLevel()

# 12 Changing Your Mind
printOutput( "Changing Your Mind" )
incrementLevel()

zoo_animals = { 'Unicorn' : 'Cotton Candy House', 'Sloth' : 'Rainforest Exhibit', 'Bengal Tiger' : 'Jungle House', 'Atlantic Puffin' : 'Arctic Exhibit', 'Rockhopper Penguin' : 'Arctic Exhibit'}
printOutput( "The starting Zoo Animals are: '%s'" % ( zoo_animals ) )

del zoo_animals['Unicorn']
del zoo_animals['Sloth']
del zoo_animals['Bengal Tiger']
printOutput( "The removed Zoo Animals are: '%s'" % ( zoo_animals ) )

zoo_animals['Rockhopper Penguin'] = "Flightless Birds"
printOutput( "The modified Zoo Animals are: '%s'" % ( zoo_animals ) )

decrementLevel()

# 13 Remove a Few Things
printOutput( "Remove a Few Things" )
incrementLevel()

backpack = ['xylophone', 'dagger', 'tent', 'bread loaf']

printOutput( backpack, LABEL="backpack: " )

backpack.remove( 'dagger' )

printOutput( backpack, LABEL="backpack: " )
printOutput( "My backpack: '%s'" % ( backpack ) )

decrementLevel()

# 14 It's Dangerous to Go Alone! Take This
printOutput( "It's Dangerous to Go Alone! Take This" )
incrementLevel()

inventory = {
    'gold' : 500,
    'pouch' : ['flint', 'twine', 'gemstone'],  # Assigned a new list to 'pouch' key
    'backpack' : ['xylophone', 'dagger', 'bedroll', 'bread loaf']
}

printOutput( "My inventory: '%s'" % ( inventory ) )

inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']

printOutput( "My new inventory: '%s'" % ( inventory ) )

inventory['pouch'].sort()

printOutput( "My newer inventory: '%s'" % ( inventory ) )

inventory['pocket'] = ['seashell', 'strange berry', 'lint']

printOutput( "My yet newer inventory: '%s'" % ( inventory ) )

inventory['backpack'].sort()

printOutput( "My even newer inventory: '%s'" % ( inventory ) )

inventory['backpack'].remove( 'dagger' )

printOutput( "My almost final inventory: '%s'" % ( inventory ) )

inventory['gold'] = inventory['gold'] + 50

printOutput( "My newest inventory: '%s'" % ( inventory ) )
decrementLevel()

# Decrement Scope level
decrementLevel()

