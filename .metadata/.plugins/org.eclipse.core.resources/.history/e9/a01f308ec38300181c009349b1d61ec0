'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from collections import OrderedDict
import json
import os.path

def printEntry ( tab, key, value ):
    tabs = '\t' * tab
    if ( type( value ) == str ):
        print ( "%s \"%s\" : \"%s\"" % ( tabs, key, value ) )
    elif ( type( value ) == int ):
        print ( "%s \"%s\" : %d" % ( tabs, key, value ) )
    elif ( type( value ) == bool ):
        print ( "%s \"%s\" : %r" % ( tabs, key, value ) )
    elif ( type( value ) == list ):
        print ( "%s \"%s\" : %s" % ( tabs, key, value ) )
    elif ( type( value ) is OrderedDict ):
        print ( "%s \"%s\" : {" % ( tabs, key ) )
        for key, value in value.items():
            printEntry ( tab + 1, key, value )
        print ( "%s }" % ( tabs + '\t' ) )
    else:
        print ( type( value ), "  Undefined" )

def printJSON ( values ):
    for key, value in values.items():
        printEntry ( 0, key, value )

def copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
    paymodel = '%s.%s.json' % ( math, sequence )
    pathToPaymodel = '%s/%s/%s' % ( pathToPaymodels, math, paymodel )
    pathToPlaydata = '%s/baseline-playdata/baseline-playdata.%s' % ( pathToConfigs, paymodel )

    print ( '\tProcess %s' % ( paymodel ) )
    if os.path.isfile( pathToPaymodel ):
        print ( '\t\tpaymodel \'%s\' found' % ( pathToPaymodel ) )
    if os.path.isfile( pathToPlaydata ):
        print ( '\t\tplaydata \'%s\' found' % ( pathToPlaydata ) )

    print ( '\t\tpathToTarget \'%s\'' % ( pathToTarget ) )

def loadJSON ( pathToFile, fileName ):

    with open( '%s/%s.json' % ( pathToFile, fileName ), 'r' ) as file:
        values = json.loads( file.read() )

    return values

def processC101Paymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )

    c101Values = loadJSON ( '%s/%s' % ( pathToPaymodels, math ), '%s.%s' % ( math, sequence ) )

    childPaymodels = c101Values['components']
    for paymodelID in childPaymodels:
        processPaymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, paymodelID )

def processC102Paymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )

    c102Values = loadJSON ( '%s/%s' % ( pathToPaymodels, math ), '%s.%s' % ( math, sequence ) )

    components = c102Values['components']
    for component in components:
        processPaymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, component["paymodel"] )

def processC103Paymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )

    c103Values = loadJSON ( '%s/%s' % ( pathToPaymodels, math ), '%s.%s' % ( math, sequence ) )

    childPaymodels = c103Values['components']
    for paymodelID in childPaymodels:
        processPaymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, paymodelID )

def processPaymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, paymodel ):
    math, sequence = paymodel.split( "." )

    complexPaymodels = [ "C101", "C102", "C103" ]
    simplePaymodels = [ "CTR", "D101", "D105", "D106", "D107", "DNT", "ITD", "P101", "S101" ]
    if math in complexPaymodels:

        getattr( self, 'process%sPaymodel' % ( math ) )( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )

#        if math == "C101":
#            processC101Paymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )
#        elif math == "C102":
#            processC102Paymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )
#        elif math == "C103":
#            processC103Paymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )
    elif math in simplePaymodels:
        copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )
    else:
        print ( "Undefined math type \'%s\'" % ( math ) )

def getConfigPaymodels ( self, pathToConfigs, pathToPaymodels, pathToTarget, gameID, values ):
    idValue = values['validMathIds']
    print ( "paymodels for gameID %d : %s" % ( gameID, idValue ) )
    for paymodelIDfromConfig in idValue:
        processPaymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, paymodelIDfromConfig )


def processGame ( sourcePathRoot, pathToTarget, gameID ):
    project = 'ts'

    pathToConfigs = '%s/%s/configurations/games/%d/configs-game' % ( sourcePathRoot, project, gameID )

    configValues = loadJSON ( pathToConfigs, 'game%d' % ( gameID ) )

    pathToTarget = '%s/%d' % ( pathToTarget, gameID )
    pathToPaymodels = '%s/%s/math-paymodels' % ( sourcePathRoot, project )
    getConfigPaymodels( self, pathToConfigs, pathToPaymodels, pathToTarget, gameID, configValues )

if __name__ == '__main__':
    sourcePathRoot = "C:/build-master/.data/.staging"
    targetPathRoot = "G:/controlled/games"

    gameID = 202
    processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1002
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1102
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1202
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1402
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1802
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1902
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2102
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2112
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2302
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2502
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2602
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2702
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2902
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 3102
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 3202
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
