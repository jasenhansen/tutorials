'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from Python_Library import setChapter
from Python_Library import setLesson
from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import incrementLevel
from Python_Library import decrementLevel
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter( "Advanced Topics in Python", 10 )
printChapter()

setLesson( "Introduction to Bitwise Operators", 2 )
printLesson()

# Set Scope level
incrementLevel()

useFunction = False

# 1 Just a Little BIT
printOutput( "Just a Little BIT" )
incrementLevel()

printOutput( "Right Shift: '5 >> 4' is '%s'" % ( 5 >> 4 ) )
printOutput( "Left Shift: '5 << 1' is '%s'" % ( 5 << 1 ) )
printOutput( "Bitwise AND: '8 & 5' is '%s'" % ( 8 & 5 ) )
printOutput( "Bitwise OR: '9 | 4' is '%s'" % ( 9 | 4 ) )
printOutput( "Bitwise XOR: '12 ^ 42' is '%s'" % ( 12 ^ 42 ) )
printOutput( "Bitwise NOT: '~88' is '%s'" % ( ~88 ) )

decrementLevel()

# 2 Lesson I0: The Base 2 Number System
printOutput( "Lesson I0: The Base 2 Number System" )
incrementLevel()

printOutput( "Binary: '0b1' is '%s'" % ( 0b1 ) )
printOutput( "Binary: '0b10' is '%s'" % ( 0b10 ) )
printOutput( "Binary: '0b11' is '%s'" % ( 0b11 ) )
printOutput( "Binary: '0b100' is '%s'" % ( 0b100 ) )
printOutput( "Binary: '0b101' is '%s'" % ( 0b101 ) )
printOutput( "Binary: '0b110' is '%s'" % ( 0b110 ) )
printOutput( "Binary: '0b111' is '%s'" % ( 0b111 ) )
printOutput( "******" )
printOutput( "Binary: '0b1 + 0b11' is '%s'" % ( 0b1 + 0b11 ) )
printOutput( "Binary: '0b11 * 0b11' is '%s'" % ( 0b11 * 0b11 ) )

decrementLevel()

# 3 I Can Count to 1100!
printOutput( "I Can Count to 1100!" )
incrementLevel()

printOutput( "'0b1' in Binary is '%s'" % ( 0b1 ) )
printOutput( "'0b10' in Binary is '%s'" % ( 0b10 ) )
printOutput( "'0b11' in Binary is '%s'" % ( 0b11 ) )
printOutput( "'0b100' in Binary is '%s'" % ( 0b100 ) )
printOutput( "'0b101' in Binary is '%s'" % ( 0b101 ) )
printOutput( "'0b110' in Binary is '%s'" % ( 0b110 ) )
printOutput( "'0b111' in Binary is '%s'" % ( 0b111 ) )
printOutput( "'0b1000' in Binary is '%s'" % ( 0b1000 ) )
printOutput( "'0b1001' in Binary is '%s'" % ( 0b1001 ) )
printOutput( "'0b1010' in Binary is '%s'" % ( 0b1010 ) )
printOutput( "'0b1011' in Binary is '%s'" % ( 0b1011 ) )
printOutput( "'0b1100' in Binary is '%s'" % ( 0b1100 ) )

decrementLevel()

# 4 The bin() Function
printOutput( "The bin() Function" )
incrementLevel()

printOutput( "Binary of '%d'is '%s'" % ( 1, bin( 1 ) ) )
printOutput( "Binary of '%d'is '%s'" % ( 2, bin( 2 ) ) )
printOutput( "Binary of '%d'is '%s'" % ( 3, bin( 3 ) ) )
printOutput( "Binary of '%d'is '%s'" % ( 4, bin( 4 ) ) )
printOutput( "Binary of '%d'is '%s'" % ( 5, bin( 5 ) ) )
printOutput( "Octal of '%d'is '%s'" % ( 1, oct( 1 ) ) )
printOutput( "Octal of '%d'is '%s'" % ( 2, oct( 2 ) ) )
printOutput( "Octal of '%d'is '%s'" % ( 3, oct( 3 ) ) )
printOutput( "Octal of '%d'is '%s'" % ( 4, oct( 4 ) ) )
printOutput( "Octal of '%d'is '%s'" % ( 5, oct( 5 ) ) )
printOutput( "Hexadecimal of '%d'is '%s'" % ( 1, hex( 1 ) ) )
printOutput( "Hexadecimal of '%d'is '%s'" % ( 2, hex( 2 ) ) )
printOutput( "Hexadecimal of '%d'is '%s'" % ( 3, hex( 3 ) ) )
printOutput( "Hexadecimal of '%d'is '%s'" % ( 4, hex( 4 ) ) )
printOutput( "Hexadecimal of '%d'is '%s'" % ( 5, hex( 5 ) ) )

decrementLevel()

# 5 int()'s Second Parameter
printOutput( "int()'s Second Parameter" )
incrementLevel()

printOutput( "Result of 'int('1',2)'is '%d'" % ( int( "1", 2 ) ) )
printOutput( "Result of 'int('10',2)'is '%d'" % ( int( "10", 2 ) ) )
printOutput( "Result of 'int('111',2)'is '%d'" % ( int( "111", 2 ) ) )
printOutput( "Result of 'int('0b100',2)'is '%d'" % ( int( "0b100", 2 ) ) )
printOutput( "Result of 'int(bin(5),2)'is '%d'" % ( int( bin( 5 ), 2 ) ) )
printOutput( "Result of 'int('11001001',2)'is '%d'" % ( int( "11001001", 2 ) ) )

decrementLevel()

# 6 Slide to the Left! Slide to the Right!
printOutput( "Slide to the Left! Slide to the Right!" )
incrementLevel()

shift_right = 0b1100
shift_left = 0b1

printOutput( "Result of '0b000001 << 2' is '%s'" % ( bin( 0b000001 << 2 ) ) )
printOutput( "Result of '1 << 2' is '%s'" % ( 1 << 2 ) )
printOutput( "Result of '0b000101 << 3' is '%s'" % ( bin( 0b000101 << 3 ) ) )
printOutput( "Result of '5 << 3' is '%s'" % ( 5 << 3 ) )
printOutput( "Result of '0b0010100 >> 3' is '%s'" % ( bin( 0b0010100 >> 3 ) ) )
printOutput( "Result of '20 >> 3' is '%s'" % ( 20 >> 3 ) )
printOutput( "Result of '0b0000010 >> 2' is '%s'" % ( bin( 0b0000010 >> 2 ) ) )
printOutput( "Result of '2 >> 2' is '%s'" % ( 2 >> 2 ) )

shift_right2 = shift_right >> 2
shift_left2 = shift_left << 2
printOutput( "Result of '%d >> 2' is '%s'" % ( shift_right, bin( shift_right2 ) ) )
printOutput( "Result of '%d << 2' is '%s'" % ( shift_left, bin( shift_left2 ) ) )

decrementLevel()

# 7 A BIT of This AND That
printOutput( "A BIT of This AND That" )
incrementLevel()

printOutput( "Result of '%s & %s' is '%s'" % ( bin( 0b1110 ), bin( 0b101 ), bin( 0b1110 & 0b101 ) ) )

decrementLevel()

# 8 A BIT of This OR That
printOutput( "A BIT of This OR That" )
incrementLevel()

printOutput( "Result of '%s | %s' is '%s'" % ( bin( 0b1110 ), bin( 0b101 ), bin( 0b1110 | 0b101 ) ) )

decrementLevel()

# 9 This XOR That?
printOutput( "This XOR That?" )
incrementLevel()

printOutput( "Result of '%s ^ %s' is '%s'" % ( bin( 0b1110 ), bin( 0b101 ), bin( 0b1110 ^ 0b101 ) ) )

decrementLevel()

# 10 See? This is NOT That Hard!
printOutput( "See? This is NOT That Hard!" )
incrementLevel()

printOutput( "Result of '~ %s' is '%s'" % ( 1, ~1 ) )
printOutput( "Result of '~ %s' is '%s'" % ( 2, ~2 ) )
printOutput( "Result of '~ %s' is '%s'" % ( 3, ~3 ) )
printOutput( "Result of '~ %s' is '%s'" % ( 42, ~42 ) )
printOutput( "Result of '~ %s' is '%s'" % ( 123, ~123 ) )

decrementLevel()

# 11 The Man Behind the Bit Mask
printOutput( "The Man Behind the Bit Mask " )
incrementLevel()

num = 0b1100
mask = 0b0100
desired = num & mask
printOutput( "Result of '%s & %s' is '%s'" % ( bin( num ), bin( mask ), bin( desired ) ) )

def check_bit4( inputValue ):
    mask = 0b1000
    desired = inputValue & mask
    printOutput( "Result of '%s & %s' is '%s'" % ( bin( inputValue ), bin( mask ), desired ) )
    if ( desired > 0 ):
        return "on"
    else:
        return "off"

decrementLevel()

# 12
printOutput( "Turn It On" )
incrementLevel()

a = 0b10111011
mask = 0b0100
desired = a | mask
printOutput( "Result of '%s | %s' is '%s'" % ( bin( a ), bin( mask ), bin( desired ) ) )

decrementLevel()

# 13 Just Flip Out
printOutput( "Just Flip Out" )
incrementLevel()
a = 0b11101110
mask = 0b11111111
desired = a ^ mask

printOutput( "Result of '%s ^ %s' is '%s'" % ( bin( a ), bin( mask ), bin( desired ) ) )

decrementLevel()

# 14 Slip and Slide
printOutput( "Slip and Slide" )
incrementLevel()

def flip_bit ( number, n ) :
    mask = 0b1
    shifted_mask = mask << ( n - 1 )
    return bin( number ^ shifted_mask )

base_number = 0b1
base_number = 0b111

# 10.2.1.1:    Right Shift: '5 >> 4' is '0'
#    Left Shift: '5 << 1' is '10'
for i in range( 1, 8 ):
    printOutput( "Base value '%s' with Bit '%d' flipped is '%s'" % ( bin( base_number ), i, flip_bit ( base_number, i ) ) )

decrementLevel()

# Decrement Scope level
decrementLevel()
