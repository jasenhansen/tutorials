'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from collections import OrderedDict
import json
import os.path
import sys

class mathReader ( object ):

    def printEntry ( self, tab, key, value ):
        tabs = '\t' * tab
        if ( type( value ) == str ):
            print ( "%s \"%s\" : \"%s\"" % ( tabs, key, value ) )
        elif ( type( value ) == int ):
            print ( "%s \"%s\" : %d" % ( tabs, key, value ) )
        elif ( type( value ) == bool ):
            print ( "%s \"%s\" : %r" % ( tabs, key, value ) )
        elif ( type( value ) == list ):
            print ( "%s \"%s\" : %s" % ( tabs, key, value ) )
        elif ( type( value ) is OrderedDict ):
            print ( "%s \"%s\" : {" % ( tabs, key ) )
            for key, value in value.items():
                mathReader.printEntry ( tab + 1, key, value )
            print ( "%s }" % ( tabs + '\t' ) )
        else:
            print ( type( value ), "  Undefined" )

    def printJSON ( self, values ):
        for key, value in values.items():
            mathReader.printEntry ( 0, key, value )

    def copyPaymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
        paymodel = '%s.%s.json' % ( math, sequence )
        pathToPaymodel = '%s/%s/%s' % ( pathToPaymodels, math, paymodel )
        pathToPlaydata = '%s/baseline-playdata/baseline-playdata.%s' % ( pathToConfigs, paymodel )

        print ( '\tProcess %s' % ( paymodel ) )
        if os.path.isfile( pathToPaymodel ):
            print ( '\t\tpaymodel \'%s\' found' % ( pathToPaymodel ) )
        if os.path.isfile( pathToPlaydata ):
            print ( '\t\tplaydata \'%s\' found' % ( pathToPlaydata ) )

        print ( '\t\tpathToTarget \'%s\'' % ( pathToTarget ) )

    def loadJSON ( self, pathToFile, fileName ):

        with open( '%s/%s.json' % ( pathToFile, fileName ), 'r' ) as file:
            values = json.loads( file.read() )

        return values

    def processC101Paymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
        mathReader.copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )

        c101Values = mathReader.loadJSON ( '%s/%s' % ( pathToPaymodels, math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c101Values['components']
        for paymodelID in childPaymodels:
            mathReader.processPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, paymodelID )

    def processC102Paymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
        mathReader.copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )

        c102Values = mathReader.loadJSON ( '%s/%s' % ( pathToPaymodels, math ), '%s.%s' % ( math, sequence ) )

        components = c102Values['components']
        for component in components:
            mathReader.processPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, component["paymodel"] )

    def processC103Paymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, math, sequence ):
        mathReader.copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )

        c103Values = mathReader.loadJSON ( '%s/%s' % ( pathToPaymodels, math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c103Values['components']
        for paymodelID in childPaymodels:
            mathReader.processPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, paymodelID )


    def processPaymodel ( self, pathToConfigs, pathToPaymodels, pathToTarget, paymodel ):
        math, sequence = paymodel.split( "." )

        complexPaymodels = [ "C101", "C102", "C103" ]
        simplePaymodels = [ "CTR", "D101", "D105", "D106", "D107", "DNT", "ITD", "P101", "S101" ]

        if math in complexPaymodels:
            getattr( sys.modules[__name__], 'process%sPaymodel' % ( math ) )( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )
        elif math in simplePaymodels:
            mathReader.copyPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, math, sequence )
        else:
            print ( "Undefined math type \'%s\'" % ( math ) )

    def getConfigPaymodels ( self, pathToConfigs, pathToPaymodels, pathToTarget, gameID ):
        configValues = mathReader.loadJSON ( pathToConfigs, 'game%d' % ( gameID ) )

        idValue = configValues['validMathIds']
        print ( "paymodels for gameID %d : %s" % ( gameID, idValue ) )
        for paymodelIDfromConfig in idValue:
            mathReader.processPaymodel ( pathToConfigs, pathToPaymodels, pathToTarget, paymodelIDfromConfig )


    def processGame ( self, sourcePathRoot, pathToTarget, gameID ):

        project = 'ts'
        pathToConfigs = '%s/%s/configurations/games/%d/configs-game' % ( sourcePathRoot, project, gameID )
        pathToTarget = '%s/%d' % ( pathToTarget, gameID )
        pathToPaymodels = '%s/%s/math-paymodels' % ( sourcePathRoot, project )

        print ( sourcePathRoot )
        print ( pathToTarget )
        print ( gameID )
        print ( project )
        print ( pathToConfigs )
        print ( pathToTarget )
        print ( pathToPaymodels )
#        mathReader.getConfigPaymodels( pathToConfigs, pathToPaymodels, pathToTarget, gameID )


if __name__ == '__main__':
    sourcePathRoot = "C:/build-master/.data/.staging"
    targetPathRoot = "G:/controlled/games"

    foo = mathReader()

    foo.processGame ( sourcePathRoot, targetPathRoot, 202 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1002 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1102 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1202 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1402 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1802 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1902 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2102 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2112 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2302 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2502 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2602 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2702 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2902 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 3102 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 3202 )
