'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from /Python/Codecamy_Python_Library.py import setChapter
from /Python/Codecamy_Python_Library.py import setLesson
from /Python/Codecamy_Python_Library.py import printChapter
from /Python/Codecamy_Python_Library.py import printLesson
from /Python/Codecamy_Python_Library.py import incrementLevel
from /Python/Codecamy_Python_Library.py import decrementLevel
from /Python/Codecamy_Python_Library.py import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter( "Python Syntax", 1 )
printChapter()

setLesson( "Python Syntax", 1 )
printLesson()

# Set Scope level
incrementLevel()

# 1 Welcome!
printOutput( "Welcome!" )
incrementLevel()

printOutput( "Welcome to Python!" )

decrementLevel()

# 2 Variables
printOutput( "Variables" )
incrementLevel()

my_variable = 10

printOutput( my_variable, LABEL="Integer value: ", DESCRIPTIVE="" )

decrementLevel()

# 3 Booleans
printOutput( "Booleans" )
incrementLevel()

my_int = 7
my_float = 1.23
my_bool = True

printOutput( my_int, LABEL="Integer value: ", DESCRIPTIVE="" )
printOutput( my_float, LABEL="Float value: ", DESCRIPTIVE="" )
printOutput( my_bool, LABEL="Boolean value: ", DESCRIPTIVE="" )

decrementLevel()

# 4 You've Been Reassigned
printOutput( "You've Been Reassigned" )
incrementLevel()

my_int = 7
printOutput( my_int, LABEL="Initial value: ", DESCRIPTIVE="" )

my_int = 3
printOutput( my_int, LABEL="Reassigned value: ", DESCRIPTIVE="" )

decrementLevel()

# 5 Whitespace
printOutput( "Whitespace" )

# 6 Whitespace Means Right Space
printOutput( "Whitespace Means Right Space" )
incrementLevel()

def spam():
    eggs = 12
    return eggs

printOutput( "Calling 'spam()' with no input causes output '%s'" % ( spam() ) )

decrementLevel()

# 7 A Matter of Interpretation
printOutput( "A Matter of Interpretation" )
incrementLevel()

spam = True
eggs = False

printOutput( "'spam' is '%s'" % ( spam ) )
printOutput( "'eggs' is '%s'" % ( eggs ) )

decrementLevel()

# 8 Single Line Comments
printOutput( "Single Line Comments" )

# I am a comment

# 9 Multi-Line Commentss
printOutput( "Multi-Line Comments" )
"""
This is a multi like comment
"""

# 10 Math
printOutput( "Math" )
incrementLevel()

big_1 = 100
big_2 = 200
total = big_1 + big_2

printOutput( "'%d + %d'  is '%d'" % ( big_1, big_2, total ) )

decrementLevel()

# 11 Exponentiation
printOutput( "Exponentiation" )
incrementLevel()

eggs = 10

printOutput( "'eggs' is '%s'" % ( eggs ) )

eggs = eggs ** 2

printOutput( "'eggs ** 2' is '%s'" % ( eggs ) )

decrementLevel()

# 12 Modulo
printOutput( "Modulo" )
incrementLevel()

value_1 = 6
value_2 = 5
spam = 6 % 5

printOutput( "'%d %% %d'  is '%d'" % ( value_1, value_2, spam ) )

decrementLevel()

# 13 Bringing It All Together
printOutput( "Bringing It All Together" )
incrementLevel()

monty = True
python = 1.234
value_1 = 2
monty_python = python ** value_1

printOutput( "'monty' is '%s'" % ( monty ) )
printOutput( "'python' is '%s'" % ( python ) )
printOutput( "'monty_python = python ** %s' is '%s'" % ( value_1, eggs ) )

decrementLevel()

# Decrement Scope level
decrementLevel()
