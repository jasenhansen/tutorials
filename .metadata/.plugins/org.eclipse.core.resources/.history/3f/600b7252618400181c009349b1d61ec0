'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from collections import OrderedDict
import json
import os.path

class mathReader ( object ):

    def __init__( self, sourcePathRoot, targetPathRoot ):
        self.sourcePathRoot = sourcePathRoot
        self.targetPathRoot = targetPathRoot
        self.self.game_dict = dict()

    def addGameToGameDictionary( self, gameID ):
        self.sourcePathRoot = sourcePathRoot
        self.targetPathRoot = targetPathRoot
        self.self.game_dict = dict()
        self.game_dict[ gameID ] = dict()
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = ''
        self.game_dict[ gameID ][ 'pathToTarget' ] = ''
        self.game_dict[ gameID ]["paymodels"] = []
        self.game_dict[ gameID ]["playdata"] = []

    def initGameDictionary ( self, sourcePathRoot, targetPathRoot ):
        self.sourcePathRoot = sourcePathRoot
        self.targetPathRoot = targetPathRoot

    def printEntry ( self, tab, key, value ):
        tabs = '\t' * tab
        if ( type( value ) == str ):
            print ( "%s \"%s\" : \"%s\"" % ( tabs, key, value ) )
        elif ( type( value ) == int ):
            print ( "%s \"%s\" : %d" % ( tabs, key, value ) )
        elif ( type( value ) == bool ):
            print ( "%s \"%s\" : %r" % ( tabs, key, value ) )
        elif ( type( value ) == list ):
            print ( "%s \"%s\" : %s" % ( tabs, key, value ) )
        elif ( type( value ) is OrderedDict ):
            print ( "%s \"%s\" : {" % ( tabs, key ) )
            for key, value in value.items():
                self.printEntry ( tab + 1, key, value )
            print ( "%s }" % ( tabs + '\t' ) )
        else:
            print ( type( value ), "  Undefined" )

    def printJSON ( self, values ):
        for key, value in values.items():
            self.printEntry ( 0, key, value )

    def copyPaymodel ( self, math, sequence ):
        paymodel = '%s.%s.json' % ( math, sequence )
        pathToPaymodel = '%s/%s/%s' % ( self.game_dict.get( "pathToPaymodels" ), math, paymodel )
        pathToPlaydata = '%s/baseline-playdata/baseline-playdata.%s' % ( self.game_dict.get( "pathToTarget" ), paymodel )

        if os.path.isfile( pathToPaymodel ):
            self.game_dict["paymodels"].append( paymodel )
        if os.path.isfile( pathToPlaydata ):
            self.game_dict["playdata"].append( 'baseline-playdata.%s' % ( paymodel ) )

    def loadJSON ( self, pathToFile, fileName ):

        with open( '%s/%s.json' % ( pathToFile, fileName ), 'r' ) as file:
            values = json.loads( file.read() )

        return values

    def processC101Paymodel ( self, math, sequence ):
        self.copyPaymodel ( math, sequence )

        c101Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( "pathToPaymodels" ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c101Values['components']
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID )

    def processC102Paymodel ( self, math, sequence ):
        self.copyPaymodel ( math, sequence )

        c102Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( "pathToPaymodels" ), math ), '%s.%s' % ( math, sequence ) )

        components = c102Values['components']
        for component in components:
            self.processPaymodel ( component["paymodel"] )

    def processC103Paymodel ( self, math, sequence ):
        self.copyPaymodel ( math, sequence )

        c103Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( "pathToPaymodels" ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c103Values['components']
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID )


    def processPaymodel ( self, paymodel ):
        math, sequence = paymodel.split( "." )

        complexPaymodels = [ "C101", "C102", "C103" ]
        simplePaymodels = [ "CTR", "D101", "D105", "D106", "D107", "DNT", "ITD", "P101", "S101" ]

        if math in complexPaymodels:
            getattr( self, 'process%sPaymodel' % ( math ) )( math, sequence )
        elif math in simplePaymodels:
            self.copyPaymodel ( math, sequence )
        else:
            print ( "Undefined math type \'%s\'" % ( math ) )

    def getConfigPaymodels ( self, gameID ):
        configValues = self.loadJSON ( self.game_dict.get( gameID ).get( "pathToConfigs" ), 'game%d' % ( gameID ) )

        idValue = configValues['validMathIds']

        for paymodelIDfromConfig in idValue:
            self.processPaymodel ( paymodelIDfromConfig )


    def processGame ( self, gameID ):


        project = 'ts'
#        pathToConfigs = '%s/%s/configurations/games/%d/configs-game' % ( self.sourcePathRoot, project, gameID )
#        self.game_dict.getpathToPaymodels" ) = '%s/%d' % ( self.targetPathRoot, gameID )
#        pathToPaymodels = '%s/%s/math-paymodels' % ( self.sourcePathRoot, project )
        self.addGameToGameDictionary ( gameID )
        self.game_dict[ gameID ][ 'pathToConfigs' ] = '%s/%s/configurations/games/%d/configs-game' % ( self.sourcePathRoot, project, gameID )
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = '%s/%s/math-paymodels' % ( self.sourcePathRoot, project )
        self.game_dict[ gameID ][ 'pathToTarget' ] = '%s/%d' % ( self.targetPathRoot, gameID )

        self.getConfigPaymodels( gameID )


if __name__ == '__main__':
    sourcePathRoot = "C:/build-master/.data/.staging"
    targetPathRoot = "G:/controlled/games"

    foo = mathReader( sourcePathRoot, targetPathRoot )

    foo.processGame ( 202 )


#     processGame ( self, sourcePathRoot, targetPathRoot, 1002 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1102 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1202 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1402 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1802 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 1902 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2102 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2112 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2302 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2502 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2602 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2702 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 2902 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 3102 )
#     processGame ( self, sourcePathRoot, targetPathRoot, 3202 )
