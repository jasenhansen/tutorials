'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from library import setChapter
from library import setLesson
from library import printChapter
from library import printLesson
from library import incrementLevel
from library import decrementLevel
from library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter("Introduction to Classes", 11)
printChapter()

setLesson("Introduction to Classes", 1)
printLesson()

# Set Scope level
incrementLevel()

useFunction = False

# 1 
printOutput("Why Use Classes?")
incrementLevel()
    
class Fruit(object):
    """A class that makes various tasty fruits."""
    def __init__(self, name, color, flavor, poisonous):
        self.name = name
        self.color = color
        self.flavor = flavor
        self.poisonous = poisonous

    def description(self):
        printOutput("I'm a %s %s and I taste %s." % (self.color, self.name, self.flavor))

    def is_edible(self):
        if not self.poisonous:
            printOutput("Yep! I'm edible.")
        else:
            printOutput("Don't eat me! I am super poisonous.")

lemon = Fruit("lemon", "yellow", "sour", False)
lemon.description()
lemon.is_edible()

decrementLevel()

# 2 Class Syntax
printOutput("Class Syntax")
incrementLevel()

class Animal_1 (object):
    """A class that defines an animal."""
    pass

decrementLevel()

# 3 Classier Classes
printOutput("Classier Classes")
incrementLevel()

class Animal_2 (object):
    """A class that defines an animal."""
    def __init__(self):
        pass

decrementLevel()

# 4 Let's Not Get Too Selfish
printOutput("Let's Not Get Too Selfish")
incrementLevel()
class Animal_3 (object):
    """A class that defines an animal."""
    def __init__(self, name):
        self.name = name
   
decrementLevel()

# 5 Instantiating Your First Object
printOutput("Instantiating Your First Object")
incrementLevel()

class Animal_4 (object):
    """A class that defines an animal."""
    def __init__(self, name):
        self.name = name

zebra = Animal_4("Jeffrey")

printOutput("My animal's name is '%s'" % (zebra.name))

decrementLevel()

# 6 More on __init__() and self
printOutput("More on __init__() and self")
incrementLevel()
# Class definition
class Animal_5(object):
    """Makes cute animals."""
    # For initializing our instance objects
    def __init__(self, name, age, is_hungry):
        self.name = name
        self.age = age
        self.is_hungry = is_hungry

zebra = Animal_5("Jeffrey", 2, True)
giraffe = Animal_5("Bruce", 1, False)
panda = Animal_5("Chad", 7, True)

printOutput("My zebra's name is '%s', it is '%s' and it is hungry '%s'" % (zebra.name, zebra.age, zebra.is_hungry))
printOutput("My giraffe's name is '%s', it is '%s' and it is hungry '%s'" % (giraffe.name, giraffe.age, giraffe.is_hungry))
printOutput("My panda's name is '%s', it is '%s' and it is hungry '%s'" % (panda.name, panda.age, panda.is_hungry))

def print_variable(name, scope):
    print "%s = %r" % (name, scope[name])

print_variable(panda, local_scope)

decrementLevel()

# 7 Class Scope
printOutput("Class Scope")
incrementLevel()

decrementLevel()

# 
printOutput("")
incrementLevel()

decrementLevel()

# 
printOutput("")
incrementLevel()

decrementLevel()

# 
printOutput("")
incrementLevel()

decrementLevel()

# Decrement Scope level
decrementLevel()
