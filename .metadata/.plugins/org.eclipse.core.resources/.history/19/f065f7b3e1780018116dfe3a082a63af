'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from collections import OrderedDict
import json
import os.path

def printEntry ( tab, key, value ):
    tabs = '\t' * tab
    if ( type( value ) == str ):
        print ( "%s \"%s\" : \"%s\"" % ( tabs, key, value ) )
    elif ( type( value ) == int ):
        print ( "%s \"%s\" : %d" % ( tabs, key, value ) )
    elif ( type( value ) == bool ):
        print ( "%s \"%s\" : %r" % ( tabs, key, value ) )
    elif ( type( value ) == list ):
        print ( "%s \"%s\" : %s" % ( tabs, key, value ) )
    elif ( type( value ) is OrderedDict ):
        print ( "%s \"%s\" : {" % ( tabs, key ) )
        for key, value in value.items():
            printEntry ( tab + 1, key, value )
        print ( "%s }" % ( tabs + '\t' ) )
    else:
        print ( type( value ), "  Undefined" )

def printJSON ( values ):
    for key, value in values.items():
        printEntry ( 0, key, value )

def copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    paymodel = '%s.%s.json' % ( math, sequence )
    pathToPaymodel = '%s/%s/%s' % ( pathToPaymodels, math, paymodel )
    pathToPlaydata = '%s/baseline-playdata/baseline-playdata.%s' % ( pathToConfigs, paymodel )

    print ( '\tProcess %s' % ( paymodel ) )
    print ( '\tpathToPaymodel %s' % ( pathToPaymodel ) )
    if os.path.isfile( pathToPaymodel ):
        print ( '\t\tpaymodel \'%s\' found' % ( pathToPaymodel ) )
    if os.path.isfile( pathToPlaydata ):
        print ( '\t\tplaydata \'%s\' found' % ( pathToPlaydata ) )


def processC101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

    json_data = open( pathToPaymodels + '/%s/%s' % ( math, '%s.%s.json' % ( math, sequence ) ), 'r' )
    values = json.load( json_data, object_pairs_hook=OrderedDict )
    json_data.close()
    childPaymodels = values['components']
    for paymodelID in childPaymodels:
        processPaymodel ( pathToConfigs, pathToPaymodels, paymodelID )

def processC102Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

    json_data = open( pathToPaymodels + '/%s/%s' % ( math, '%s.%s.json' % ( math, sequence ) ), 'r' )
    values = json.load( json_data, object_pairs_hook=OrderedDict )
    json_data.close()
    components = values['components']
    for component in components:
        processPaymodel ( pathToConfigs, pathToPaymodels, component["paymodel"] )

def processC103Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

    json_data = open( pathToPaymodels + '/%s/%s' % ( math, '%s.%s.json' % ( math, sequence ) ), 'r' )
    values = json.load( json_data, object_pairs_hook=OrderedDict )
    json_data.close()
    childPaymodels = values['components']
    for paymodelID in childPaymodels:
        processPaymodel ( pathToConfigs, pathToPaymodels, paymodelID )

def processCTRPaymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processD101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processD105Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processD106Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processD107Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processDDPaymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processDNTPaymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processITDPaymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processP101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processS101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence ):
    copyPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )

def processPaymodel ( pathToConfigs, pathToPaymodels, paymodel ):
    math, sequence = paymodel.split( "." )

    if math == "C101":
        processC101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "C102":
        processC102Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "C103":
        processC103Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "CTR":
        processCTRPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "D101":
        processD101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "D105":
        processD105Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "D106":
        processD106Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "D107":
        processD107Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "DNT":
        processDNTPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "ITD":
        processITDPaymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "P101":
        processP101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    elif math == "S101":
        processS101Paymodel ( pathToConfigs, pathToPaymodels, math, sequence )
    else:
        print ( "Undefined math type \'%s\'" % ( math ) )

def getConfigPaymodels ( pathToConfigs, pathToPaymodels, gameID, values ):
    idValue = values['validMathIds']
    print ( "paymodels for gameID %d : %s" % ( gameID, idValue ) )
    for paymodelIDfromConfig in idValue:
        processPaymodel ( pathToConfigs, pathToPaymodels, paymodelIDfromConfig )


def processGame ( sourcePathRoot, targetPathRoot, gameID ):
    project = '/ts'

    pathToConfigs = sourcePathRoot + '%s/configurations/games/%d/configs-game' % ( project, gameID )
    pathToPaymodels = sourcePathRoot + '%s/math-paymodels' % ( project )

    json_data = open( '%s/game%d.json' % ( pathToConfigs, gameID ), 'r' )
    values = json.load( json_data, object_pairs_hook=OrderedDict )
    json_data.close()

    getConfigPaymodels( pathToConfigs, pathToPaymodels, gameID, values )


if __name__ == '__main__':
    sourcePathRoot = "C:/build-master/.data/.staging"
    targetPathRoot = "G:/controlled"

    gameID = 202
    processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1002
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1102
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1202
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1402
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1802
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 1902
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2102
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2112
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2302
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2502
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2602
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2702
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 2902
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 3102
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
#     gameID = 3202
#     processGame ( sourcePathRoot, targetPathRoot, gameID )
