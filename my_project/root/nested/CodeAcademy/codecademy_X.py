'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from Codecamy_Python_Library import setChapter
from Codecamy_Python_Library import setLesson
from Codecamy_Python_Library import printChapter
from Codecamy_Python_Library import printLesson
from Codecamy_Python_Library import incrementLevel
from Codecamy_Python_Library import decrementLevel
from Codecamy_Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

setChapter( "Chapter", 0 )
printChapter()

setLesson( "Lesson", 0 )
printLesson()

useFunction = False

# Set Scope level
incrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

#
printOutput( "" )
incrementLevel()

decrementLevel()

# Decrement Scope level
decrementLevel()
