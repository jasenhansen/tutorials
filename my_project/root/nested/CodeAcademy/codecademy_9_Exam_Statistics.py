'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Exam Statistics", 9 )

useFunction = False

# 1 Let's look at those grades!
printExercise( "Let's look at those grades!" )

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]
printOutput( "Grades: '%s'" % ( grades ) )

# 2 Print those grades
printExercise( "Print those grades" )

def print_grades ( grades_input ):
    for index, grade in enumerate( grades_input ):
        printOutput( 'Grade %s: %s' % ( index + 1, grade ) )

print_grades( grades )

# 3 Review
printExercise( "Review" )

printOutput( "Let's compute some stats!" )

# 4 The sum of scores
printExercise( "The sum of scores" )

def grades_sum ( grades_input ):
    SUM = 0
    for grade in grades_input:
        SUM += grade
    return SUM

printOutput( "The sum of the grade list '%s' is '%s'" % ( grades, grades_sum ( grades ) ) )

# 5 Computing the Average
printExercise( "Computing the Average" )

def grades_average ( grades_input ):
    SUM = grades_sum ( grades_input )
    return SUM / float( len( grades_input ) )

printOutput( "The average of the grade list '%s' is '%s'" % ( grades, grades_average ( grades ) ) )

LIST2 = [0, 8, 4, 2]
printOutput( "The average of the list '%s' is '%s'" % ( grades, grades_average ( LIST2 ) ) )

# 6 Review
printExercise( "Review" )

printOutput( "Time to conquer the variance!" )

# 7 The Variance
printExercise( "The Variance" )

def grades_variance ( scores ):
    AVERAGE = grades_average ( scores )
    VARIANCE = 0
    for score in scores:
        VARIANCE += ( AVERAGE - score ) ** 2
    return VARIANCE / float( len( scores ) )

printOutput( "The variance of the grade list '%s' is '%s'" % ( grades, grades_variance ( grades ) ) )

# 8 Standard Deviation
printExercise( "Standard Deviation" )

def grades_std_deviation ( variance ):
    return ( variance ) ** 0.5

printOutput( "The standard deviation of the grade list '%s' is '%s'" % ( grades, grades_std_deviation( grades_variance ( grades ) ) ) )

# 9 Review
printExercise( "Review" )

def grades_statistics ( grades_input ):
    print_grades ( grades_input )
    printOutput( "Sum: %s" % ( grades_sum ( grades_input ) ) )
    printOutput( "Average: %s" % ( grades_average ( grades_input ) ) )
    variance = grades_variance ( grades_input )
    printOutput( "Variance: %s" % ( variance ) )
    printOutput( "Standard Deviation: %s" % ( grades_std_deviation( variance ) ) )

grades_statistics ( grades )
