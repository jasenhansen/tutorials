'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Strings and Console Output", 2 )

printLesson( "Date and Time", 2 )

# 1 The datetime Library
printExercise( "The datetime Library" )

from datetime import datetime

# 2 Getting the Current Date and Time
printExercise( "Getting the Current Date and Time" )

now = datetime.now()

printOutput( now, LABEL="now: ", DESCRIPTIVE="" )

# 3 Extracting Information
printExercise( "Extracting Information" )

now = datetime.now()

printOutput( "The current date is '%s'" % ( now ) )
printOutput( "The current year is '%d'" % ( now.year ) )
printOutput( "The current month is '%d'" % ( now.month ) )
printOutput( "The current day is '%d'" % ( now.day ) )
printOutput( "The current hour is '%d'" % ( now.hour ) )
printOutput( "The current minute is '%d'" % ( now.minute ) )
printOutput( "The current second is '%d'" % ( now.second ) )

# 4 Hot Date
printExercise( "Hot Date" )

now = datetime.now()

printOutput( "The formated date is '%d/%d/%d'" % ( now.year, now.month, now.day ) )

# 5 Pretty Time
printExercise( "Pretty Time" )

now = datetime.now()

printOutput( "The formated time is '%d:%d:%d'" % ( now.hour, now.minute, now.second ) )

# 6 Grand Finale
printExercise( "Grand Finale" )

now = datetime.now()

printOutput( "The formated date and time '%d/%d/%d %d:%d:%d'" % ( now.year, now.month, now.day, now.hour, now.minute, now.second ) )
