'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Loops", 8 )

printLesson( "Practice Makes Perfect", 2 )

# 1 Practice! Practice Practice!
printExercise( "Practice! Practice Practice!" )

# 2 is_even
printExercise( "is_even" )

def is_even( VALUE ):
    if ( ( VALUE % 2 ) == 0 ):
        return True
    else :
        return False

for i in range( 4 ):
    if ( is_even( i ) ) :
        printOutput( "Your count is '%d' is even" % ( i ) )
    else :
        printOutput( "Your count is '%d' is odd" % ( i ) )

# 3 is_int
printExercise( "is_int" )

def is_int( num ):
    if ( num - round( num, 0 ) == 0 ) :
        return True
    else:
        return False

numbers = [7.0, 7.5, -1]
for i in numbers:
    if ( is_int( i ) ) :
        printOutput( "Your value is '%s' is an Int" % ( i ) )
    else :
        printOutput( "Your value is '%s' is not an Int" % ( i ) )

# 4 digit_sum
printExercise( "digit_sum" )

def digit_sum( num ):
    value = str( num )
    total = 0
    for char in value:
        total += int( char )
    return total

digits = 1234
printOutput( "The sum of the digits of '%d' is '%d'" % ( digits, digit_sum( digits ) ) )

# 5 factorial
printExercise( "factorial" )

def factorial( num ):
    total = 1
    while num > 0:
        total *= num
        num -= 1
    return total

number = 4
printOutput( "The '%d' factorial is '%d'" % ( number, factorial( number ) ) )

# 6 is_prime
printExercise( "is_prime" )

import math

def is_prime ( NUMBER ):

    if( NUMBER < 2 ) :
        return False
    if( ( NUMBER > 2 and ( NUMBER % 2 ) == 0 ) ) :
        return False

    for i in range( 3, int( math.sqrt( NUMBER ) + 1 ) ):
        if ( ( NUMBER % ( i ) ) == 0 ):
            return False
    return True

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for i in numbers:
    if ( is_prime( i ) ) :
        printOutput( "Your value is '%s' is Prime" % ( i ) )
    else :
        printOutput( "Your value is '%s' is not Prime" % ( i ) )

# 7 reverse
printExercise( "reverse" )

def reverse ( TEST_STRING ):
    LENGTH = len( TEST_STRING )
    VALUE = ""
    for i in range( LENGTH ):
        VALUE += TEST_STRING[LENGTH - 1 - i]
    return VALUE

STRING = "Cat"

printOutput( "The string '%s' reversed is '%s'" % ( STRING, reverse ( STRING ) ) )

# 8 anti_vowel
printExercise( "anti_vowel" )

def anti_vowel ( TEST_STRING ):
    TEST_LIST = ["A", "E", "I", "O", "U"]

    VALUE = ""
    for char in TEST_STRING:
        TEST_ADD = True
        for ENTRY in TEST_LIST:
            if char.upper() == ENTRY:
                TEST_ADD = False
                break
        if ( TEST_ADD ) :
            VALUE += char
    return VALUE

STRING = "Hey You!"

printOutput( "The string '%s' with vowels removed is '%s'" % ( STRING, anti_vowel ( STRING ) ) )

# 9 scrabble_score
printExercise( "scrabble_score" )

def scrabble_score ( TEST_STRING ):
    SCORE = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2, "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3, "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1, "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4, "x": 8, "z": 10}
    VALUE = 0
    for char in TEST_STRING:
        VALUE += SCORE[char.lower()]
    return VALUE

STRING = "Helix"

printOutput( "The scrabble_score of string '%s' is '%d'" % ( STRING, scrabble_score ( STRING ) ) )

# 10 censor
printExercise( "censor" )

def censor ( TEST_STRING, TEST_CENSOR_WORD ):
    CENSOR_WORD_REPLACE = ""
    CENSOR_WORD_REPLACE_CHARACTER = "*"
    for i in range( len( TEST_CENSOR_WORD ) ):
        CENSOR_WORD_REPLACE += CENSOR_WORD_REPLACE_CHARACTER
    return TEST_STRING.replace( TEST_CENSOR_WORD, CENSOR_WORD_REPLACE )

STRING = "this hack is wack hack"
CENSOR_WORD = "hack"

printOutput( "The censor of string '%s' is '%s'" % ( STRING, censor ( STRING, CENSOR_WORD ) ) )

# 11 count
printExercise( "count" )

def count ( TEST_LIST, TEST_ITEM ):
    COUNT = 0
    for ITEM in TEST_LIST:
        if ITEM == TEST_ITEM :
            COUNT += 1
    return COUNT

ITEM = 1
ITEM_LIST = [1, 2, 1, 1]
printOutput( "The count of '%s' in the list '%s' is '%s'" % ( ITEM, ITEM_LIST, count ( ITEM_LIST, ITEM ) ) )

# 12 purify
printExercise( "purify" )

def purify ( TEST_LIST ):
    RETURN_LIST = []

    for ITEM in TEST_LIST:
        if ( ( ITEM % 2 ) == 0 ) :
            RETURN_LIST.append( ITEM )
    return RETURN_LIST

ITEM_LIST = [1, 2, 3]
printOutput( "The purify of the list '%s' is '%s'" % ( ITEM_LIST, purify ( ITEM_LIST ) ) )

# 13 product
printExercise( "product" )

def product ( TEST_LIST ):
    PRODUCT = 1

    for ITEM in TEST_LIST:
        PRODUCT *= ITEM
    return PRODUCT

ITEM_LIST = [4, 5, 5]
printOutput( "The product of the list '%s' is '%d'" % ( ITEM_LIST, product ( ITEM_LIST ) ) )

# 14 remove_duplicates
printExercise( "remove_duplicates" )

def remove_duplicates ( TEST_LIST ):
    RETURN_LIST = []

    for ITEM in TEST_LIST:
        INSERT = True
        for KEEP_ITEM in RETURN_LIST:
            if ( KEEP_ITEM == ITEM ) :
                INSERT = False
        if ( INSERT ) :
            RETURN_LIST.append( ITEM )
    return RETURN_LIST

ITEM_LIST = [1, 1, 2, 2]
printOutput( "The remove_duplicates of the list '%s' is '%s'" % ( ITEM_LIST, remove_duplicates ( ITEM_LIST ) ) )

# 15 median
printExercise( "median" )

def median ( TEST_LIST ):
    RETURN_LIST = sorted( TEST_LIST )
    TEST_LENGTH = len( RETURN_LIST )
    RETURN_VALUE = 0
    if ( ( TEST_LENGTH % 2 ) == 0 ):
        FIRST = RETURN_LIST[int( TEST_LENGTH / 2 )]
        SECOND = RETURN_LIST[int ( TEST_LENGTH / 2 - 1 )]
        RETURN_VALUE = ( FIRST + SECOND ) / 2
        if ( ( RETURN_VALUE * 2 ) != ( FIRST + SECOND ) ) :
            RETURN_VALUE = float( ( FIRST + SECOND ) ) / 2
    else :
        RETURN_VALUE = RETURN_LIST[int( TEST_LENGTH / 2 )]
    return RETURN_VALUE

ITEM_LIST = [7, 12, 3, 1, 6]
printOutput( "The median of the list '%s' is '%s'" % ( ITEM_LIST, median ( ITEM_LIST ) ) )
ITEM_LIST = [7, 12, 4, 1]
printOutput( "The median of the list '%s' is '%s'" % ( ITEM_LIST, median ( ITEM_LIST ) ) )
