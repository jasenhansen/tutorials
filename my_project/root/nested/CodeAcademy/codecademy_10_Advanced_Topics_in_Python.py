'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Advanced Topics in Python", 10 )

printLesson( "Advanced Topics in Python", 1 )

# 1 Iterators for Dictionaries
printExercise( "Iterators for Dictionaries" )

my_dict = {
  'name_first': 'Jasen',
  'title': 'Programmer',
  'employer': 'Gamblit',
  'state': 'California',
}

printOutput( "My Dictionary: '%s'" % ( my_dict.items() ) )

# 2 keys() and values()
printExercise( "keys() and values()" )

printOutput( "My Dictionary Keys: '%s'" % ( my_dict.keys() ) )
printOutput( "My Dictionary Values: '%s'" % ( my_dict.values() ) )

# 3 The 'in' Operator
printExercise( "The 'in' Operator" )

for key in my_dict:
    printOutput( "My Dictionary Key '%s' has Value '%s'" % ( key, my_dict[key] ) )

# 4 Building Lists
printExercise( "Building Lists" )

evens_to_50 = [i for i in range( 51 ) if i % 2 == 0]
printOutput( "My even list has values '%s'" % ( evens_to_50 ) )

# 5 List Comprehension Syntax
printExercise( "List Comprehension Syntax" )

doubles_by_3 = [x * 2 for x in range( 1, 6 ) if x % 3 == 0]
printOutput( "My doubles_by_3 list has values '%s'" % ( doubles_by_3 ) )

even_squares = [x ** 2 for x in range( 1, 11 ) if x % 2 == 0]
printOutput( "My even_squares list has values '%s'" % ( even_squares ) )

# 6 Now You Try!
printExercise( "Now You Try!" )

cubes_by_four = [x ** 3 for x in range( 1, 11 ) if ( x ** 3 ) % 4 == 0]
printOutput( "My cubes_by_four list has values '%s'" % ( cubes_by_four ) )

# 7 List Slicing Syntax
printExercise( "List Slicing Syntax" )

l = [i ** 2 for i in range( 1, 11 )]
printOutput( "My sliced list has values '%s'" % ( l[2:9:2] ) )

# 8 Omitting Indices
printExercise( "Omitting Indices" )

my_list = range( 1, 11 )
printOutput( "My sliced list has values '%s'" % ( my_list[::2] ) )

# 9 Reversing a List
printExercise( "Reversing a List" )

my_list = range( 1, 11 )
printOutput( "My reversed list has values '%s'" % ( my_list[::-1] ) )

# 10 Stride Length
printExercise( "Stride Length" )

to_one_hundred = range( 101 )
backwards_by_tens = to_one_hundred[::-10]
printOutput( "My backwards_by_tens list has values '%s'" % ( backwards_by_tens ) )

# 11 Practice Makes Perfect
printExercise( "Practice Makes Perfect" )

to_21 = range( 1, 22 )
printOutput( "My to_21 list has values '%s'" % ( to_21 ) )

odds1 = to_21[::2]
odds2 = [x for x in range( 1, 22 ) if ( x ) % 2 != 0]
printOutput( "My odds1 list has values '%s'" % ( odds1 ) )
printOutput( "My odds2 list has values '%s'" % ( odds2 ) )

middle_third = to_21[7:14:]
printOutput( "My middle_third list has values '%s'" % ( middle_third ) )

# 12 Anonymous Functions
printExercise( "Anonymous Functions" )

my_list = range( 16 )
printOutput( "My lambda used on list '%s' returns values '%s'" % ( my_list, filter( lambda x: x % 3 == 0, my_list ) ) )

# 13 Lambda Syntax
printExercise( "Lambda Syntax" )

languages = ["HTML", "JavaScript", "Python", "Ruby"]
printOutput( "My lambda used on list '%s' returns values '%s'" % ( my_list, filter( lambda x: x == "Python", languages ) ) )

# 14 Try It!
printExercise( "Try It!" )

squares = [x ** 2 for x in range( 1, 11 )]
printOutput( "My lambda used on list '%s' returns values '%s'" % ( my_list, filter( lambda x: ( 30 <= x ) and ( x <= 70 ), squares ) ) )

# 15 Iterating Over Dictionaries
printExercise( "Iterating Over Dictionaries" )

movies = {
  "Monty Python and the Holy Grail": "Great",
  "Monty Python's Life of Brian": "Good",
  "Monty Python's Meaning of Life": "Okay"
}

printOutput( "My Dictionary: '%s'" % ( movies.items() ) )

# 16 Comprehending Comprehensions
printExercise( "Comprehending Comprehensions" )

threes_and_fives = [x for x in range( 1, 16 )  if ( ( ( x % 3 ) == 0 ) or ( ( x % 5 ) == 0 ) )]
printOutput( "My threes_and_fives list has values '%s'" % ( threes_and_fives ) )

# 17 List Slicing
printExercise( "List Slicing" )

garbled = "!XeXgXaXsXsXeXmX XtXeXrXcXeXsX XeXhXtX XmXaX XI"
inverted = garbled[::-1]
message = inverted[::2]

printOutput( "My garbled list has values '%s'" % ( garbled ) )
printOutput( "My inverted list has values '%s'" % ( inverted ) )
printOutput( "My message list has values '%s'" % ( message ) )

# 18 Lambda Expressions
printExercise( "Lambda Expressions" )

garbled = "IXXX aXXmX aXXXnXoXXXXXtXhXeXXXXrX sXXXXeXcXXXrXeXt mXXeXsXXXsXaXXXXXXgXeX!XX"
message = filter( lambda x: ( x != "X" ), garbled )

#          filter(lambda x: (30 <= x) and (x <= 70), squares)))
# evens_to_50 = [i for i in range(51) if i % 2 == 0]

printOutput( "My garbled list has values '%s'" % ( garbled ) )
printOutput( "My message list has values '%s'" % ( message ) )
