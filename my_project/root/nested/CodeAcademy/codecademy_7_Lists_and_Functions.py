'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import determine_type

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Lists and Functions", 7 )

printLesson( "Lists and Functions", 1 )

# 1 List accessing
printExercise( "List accessing" )

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( numbers[1], LABEL="2nd element: ", DESCRIPTIVE="" )

# 2 List element modification
printExercise( "List element modification" )

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbers[1] = numbers[1] * 5

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

# 3 Appending to a list
printExercise( "Appending to a list" )

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbers.append( 4 )

printOutput( numbers, LABEL="numbers.append: ", DESCRIPTIVE="" )

# 4 Removing elements from lists
printExercise( "Removing elements from lists" )

numbers = [1, 3, 5]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( numbers.pop( 0 ), LABEL="numbers.pop", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( numbers.remove( 5 ), LABEL="numbers.remove(5)", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

del( numbers[0] )

printOutput( numbers, LABEL="del()", DESCRIPTIVE="" )

# 5 Changing the functionality of a function
printExercise( "Changing the functionality of a function" )

def my_function( x ):
    return x * 3

number = 5

printOutput( number, LABEL="number: ", DESCRIPTIVE="" )
printOutput( my_function( number ), LABEL="my_function: ", DESCRIPTIVE="" )

# 6 More than one argument
printExercise( "More than one argument" )

def add_function ( x, y ):
    return x + y

number1 = 5
number2 = 13

printOutput( number1, LABEL="number1: ", DESCRIPTIVE="" )
printOutput( number2, LABEL="number2: ", DESCRIPTIVE="" )
printOutput( add_function( number1, number2 ), LABEL="add_function: ", DESCRIPTIVE="" )

# 7 Strings in functions
printExercise( "Strings in functions" )

def string_function( value ):
    return determine_type( value ) + "world"

printOutput( "hello", LABEL="string: ", DESCRIPTIVE="" )
printOutput( string_function( "hello" ), LABEL="string_function", DESCRIPTIVE="" )
printOutput( string_function( 5 ), LABEL="string_function", DESCRIPTIVE="" )

# 8 Passing a list to a function
printExercise( "Passing a list to a function" )

def list_function( x ):
    return x

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( list_function( numbers ), LABEL="list_function: ", DESCRIPTIVE="" )

# 9 Using an element from a list in a function
printExercise( "Using an element from a list in a function" )

def list_function_1( x ):
    return x[1]

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( list_function_1( numbers ), LABEL="list_function_1: ", DESCRIPTIVE="" )

# 10 Using an element from a list in a function
printExercise( "Using an element from a list in a function" )

def list_function_2( x ):
    x[1] = x[1] + 3
    return x

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbersNew = list_function_2( numbers )

printOutput( numbersNew, LABEL="numbersNew: ", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

# 11 List manipulation in functions
printExercise( "List manipulation in functions" )

def list_extender( lst ):
    lst.append( 9 )
    return lst

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

numbersNew = list_extender( numbers )

printOutput( numbersNew, LABEL="numbersNew: ", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

# 12 Printing out a list item by item in a function
printExercise( "Printing out a list item by item in a function" )

def print_list( x ):

    for i in range( 0, len( x ) ):
        printOutput( x[i], LABEL="x[i]:", DESCRIPTIVE="" )

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
print_list( numbers )

# 13 Modifying each element in a list in a function
printExercise( "Modifying each element in a list in a function" )

def double_list( x ):
    for i in range( 0, len( x ) ):
        x[i] = x[i] * 2
    return x

numbers = [3, 5, 7]

printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )
numbersNew = double_list( numbers )

printOutput( numbersNew, LABEL="numbersNew: ", DESCRIPTIVE="" )
printOutput( numbers, LABEL="numbers: ", DESCRIPTIVE="" )

# 14 Passing a range into a function
printExercise( "Passing a range into a function" )

def my_function_1( x ):
    mylist = []
    for i in range( 0, len( x ) ):
        mylist.append( x[i] * 2 )
    return mylist

printOutput( range( 3 ) , LABEL="range: ", DESCRIPTIVE="" )
printOutput( my_function_1( range( 3 ) ) , LABEL="printLessonCounterValue: ", DESCRIPTIVE="" )

# 15 Iterating over a list in a function
printExercise( "Iterating over a list in a function" )

def total ( lst ) :
    result = 0
    for item in lst :
        result += item
    return result

numbers = [3, 5, 7]

printOutput( numbers , LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( total( numbers ) , LABEL="total: ", DESCRIPTIVE="" )

# 16 Using strings in lists in functions
printExercise( "Using strings in lists in functions" )

def join_strings ( words ) :
    result = ""
    for item in words :
        result += item
    return result

strings = ["Michael", "Lieberman"]

printOutput( strings , LABEL="strings: ", DESCRIPTIVE="" )
printOutput( join_strings( strings ) , LABEL="join_strings: ", DESCRIPTIVE="" )

# 17 Using two lists as two arguments in a function
printExercise( "Using two lists as two arguments in a function" )

def join_lists( list_1, list_2 ):
    return list_1 + list_2

numbers1 = [1, 2, 3]
numbers2 = [4, 5, 6]

printOutput( numbers1 , LABEL="numbers1: ", DESCRIPTIVE="" )
printOutput( numbers2 , LABEL="numbers2: ", DESCRIPTIVE="" )
printOutput( join_lists( numbers1, numbers2 ) , LABEL="join_lists: ", DESCRIPTIVE="" )

# 18 Using two lists as two arguments in a function
printExercise( "Using two lists as two arguments in a function" )

def flatten ( list_outer ):
    result = []
    for item in list_outer:
        result += item
    return result

numbers = [[1, 2, 3], [4, 5, 6, 7, 8, 9]]

printOutput( numbers , LABEL="numbers: ", DESCRIPTIVE="" )
printOutput( flatten( numbers ) , LABEL="flatten: ", DESCRIPTIVE="" )
