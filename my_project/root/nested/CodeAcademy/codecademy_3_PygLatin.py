'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import functionCall
from Python_Library import functionCleanup

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Conditionals and Control Flow", 3 )

printLesson( "PygLatin", 2 )

useFunction = False

# 1 Break It Down
printExercise( "Break It Down" )

# 2 Ahoy! (or Should I Say Ahoyay!)
printExercise( "Ahoy! (or Should I Say Ahoyay!)" )

printOutput( "Pig Latin" )

# 3 Input!
printExercise( "Input!" )

printOutput( 'Welcome to the Pig Latin Translator!' )

if useFunction:
    original = input( "\t\tEnter a word:  " )
else:
    original = "Functions Disabled"

printOutput( "The Original String is '%s'" % ( original ) )

# 4 Check Yourself!
printExercise( "Check Yourself!" )

def stringTest_1( original ):
    functionCall( INCREMENT="" )
    if len( original ) > 0 :
        printOutput( "The String for stringTest_1 is '%s'" % ( original ) )
    else :
        printOutput( "The String for stringTest_1 is empty" )
    functionCleanup()

printOutput( 'Welcome to the Pig Latin Translator!' )

if useFunction:
    original = input( "\t\tEnter a word:  " )
else:
    original = "Functions Disabled"

if len( original ) > 0 :
    printOutput( "The Original String is '%s'" % ( original ) )
else :
    printOutput( original, LABEL="empty: " )

stringTest_1( original )
stringTest_1( "" )
stringTest_1( "cat" )

# 5 Check Yourself... Some More
printExercise( "Check Yourself... Some More" )

printOutput( 'Welcome to the Pig Latin Translator!' )

def stringTest_2( original ):
    functionCall( INCREMENT="" )
    if len( original ) > 0 and original.isalpha():
        printOutput( "The String for stringTest_2 is '%s'" % ( original ) )
    else :
        if len( original ) > 0:
            printOutput( "The String '%s' for stringTest_2 is not isalpha" % ( original ) )
        else :
            printOutput( "The String for stringTest_2 is empty" )
    functionCleanup()

if useFunction:
    original = input( "\t\tEnter a word:  " )
else:
    original = "Functions Disabled"

stringTest_2( original )
stringTest_2( "" )
stringTest_2( "cat" )
stringTest_2( "c@t" )

# 6 Pop Quiz!
printExercise( "Pop Quiz!" )

# 7 Ay B C
printExercise( "Ay B C" )

pyg = 'ay'
printOutput( "The Pig Latin Suffix is '%s'" % ( pyg ) )

# 8 Word Up
printExercise( "Word Up" )

def stringTest_3( original ):
    functionCall( INCREMENT="" )
    pyg = 'ay'
    printOutput( "The stringTest_3 Pig Latin Suffix is '%s'" % ( pyg ) )
    if len( original ) > 0 and original.isalpha():
        printOutput( "The String for stringTest_3 is '%s'" % ( original ) )
        word = original.lower()
        printOutput( "The String for stringTest_3 set to lower is '%s'" % ( word ) )
        first = word[0]
        printOutput( "The first letter of the String for stringTest_3 set to lower is '%s'" % ( first ) )
    else :
        if len( original ) > 0:
            printOutput( "The String '%s' for stringTest_3 is not isalpha" % ( original ) )
        else :
            printOutput( "The String for stringTest_3 is empty" )
    functionCleanup()

if useFunction:
    original = input( "\t\tEnter a word:  " )
else:
    original = "Functions Disabled"
printOutput( "The Original String is '%s'" % ( original ) )
stringTest_3( original )
stringTest_3( "" )
stringTest_3( "Cat" )
stringTest_3( "c@t" )

# 9 Move it on Back
printExercise( "Move it on Back" )

def stringTest_4( original ):
    functionCall( INCREMENT="" )
    pyg = 'ay'
    printOutput( "The stringTest_4 Pig Latin Suffix is '%s'" % ( pyg ) )
    if len( original ) > 0 and original.isalpha():
        printOutput( "The String for stringTest_4 is '%s'" % ( original ) )
        word = original.lower()
        printOutput( "The String for stringTest_4 set to lower is '%s'" % ( word ) )
        first = word[0]
        printOutput( "The first letter of the String for stringTest_4 set to lower is '%s'" % ( first ) )
        new_word = word + first + pyg
        printOutput( "The new word for stringTest_4 is '%s'" % ( new_word ) )
    else :
        if len( original ) > 0:
            printOutput( "The String '%s' for stringTest_4 is not isalpha" % ( original ) )
        else :
            printOutput( "The String for stringTest_4 is empty" )
    functionCleanup()

if useFunction:
    original = input( "\t\tEnter a word:  " )
else:
    original = "Functions Disabled"

printOutput( "The Original String is '%s'" % ( original ) )
stringTest_4( original )
stringTest_4( "" )
stringTest_4( "Cat" )
stringTest_4( "c@t" )

# 10 Ending Up
printExercise( "Ending Up" )

def stringTest_5( original ):
    functionCall( INCREMENT="" )
    pyg = 'ay'
    printOutput( "The stringTest_5 Pig Latin Suffix is '%s'" % ( pyg ) )
    if len( original ) > 0 and original.isalpha():
        printOutput( "The String for stringTest_5 is '%s'" % ( original ) )
        word = original.lower()
        printOutput( "The String for stringTest_5 set to lower is '%s'" % ( word ) )
        first = word[0]
        printOutput( "The first letter of the String for stringTest_5 set to lower is '%s'" % ( first ) )
        new_word = word[1:] + first + pyg
        printOutput( "The new word for stringTest_5 is '%s'" % ( new_word ) )
    else :
        if len( original ) > 0:
            printOutput( "The String '%s' for stringTest_5 is not isalpha" % ( original ) )
        else :
            printOutput( "The String for stringTest_5 is empty" )
    functionCleanup()

if useFunction:
    original = input( "\t\tEnter a word:  " )
else:
    original = "Functions Disabled"
printOutput( "The Original String is '%s'" % ( original ) )
stringTest_5( original )
stringTest_5( "" )
stringTest_5( "Cat" )
stringTest_5( "c@t" )

# 11 Testing, Testing, is This Thing On?
printExercise( "Testing, Testing, is This Thing On?" )
