'''
Created on May 30,2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import functionCall
from Python_Library import functionCleanup
from Python_Library import inputInt

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Lists and Functions", 7 )

printLesson( "Battleship!", 2 )

# 1 Welcome to Battleship!
printExercise( "Welcome to Battleship!" )

# 2 Getting Our Feet Wet
printExercise( "Getting Our Feet Wet" )

BOARD = []

printOutput( BOARD , LABEL="BOARD: ", DESCRIPTIVE="" )

# 3 Make a List
printExercise( "Make a List" )

def generate_board( ROW_COUNT, COLUMN_COUNT ):
    BOARD = []
    for _ in range( ROW_COUNT ):
        ROW = []
        for _ in range( COLUMN_COUNT ):
            ROW.append( "O" )
        BOARD.append( ROW )
    return BOARD

BOARD = generate_board( 5, 5 )

printOutput( BOARD , LABEL="BOARD: ", DESCRIPTIVE="" )

# 4 Check it Twice
printExercise( "Check it Twice" )

BOARD = generate_board( 5, 5 )

printOutput( BOARD, LABEL="BOARD: ", DESCRIPTIVE="" )

# 5 Custom Print
printExercise( "Custom Print" )

def print_board ( BOARD ):
    for ROW in BOARD:
        printOutput( ROW, LABEL="row: ", DESCRIPTIVE="" )

BOARD = generate_board( 5, 5 )
print_board( BOARD )

# 6 Printing Pretty
printExercise( "Printing Pretty" )

def pretty_print_board ( BOARD ):
    functionCall()
    for ROW in BOARD:
        printOutput( " ".join( ROW ), LABEL="row: ", DESCRIPTIVE="" )
    functionCleanup()

BOARD = generate_board( 5, 5 )
pretty_print_board( BOARD )

# 7 Hide...
printExercise( "Hide..." )

from random import randint

def random_row ( BOARD ):
    return randint( 0, len( BOARD ) - 1 )

def random_col ( BOARD ):
    return randint( 0, len( BOARD[1] ) - 1 )

BOARD = generate_board( 5, 5 )

printOutput( random_row( BOARD ), LABEL="random_row: ", DESCRIPTIVE="" )
printOutput( random_col( BOARD ), LABEL="random_col: ", DESCRIPTIVE="" )

# 8 ...and Seek!
printExercise( "...and Seek!" )

def getGuess ():
    functionCall()
    GUESS_LIST = []
    GUESS_LIST.append( inputInt( "\t\tGuess Row: " ) )
    GUESS_LIST.append( inputInt( "\t\tGuess Column: " ) )

    printOutput( GUESS_LIST[0], LABEL="Row: ", DESCRIPTIVE="" )
    printOutput( GUESS_LIST[1], LABEL="Column: ", DESCRIPTIVE="" )

    functionCleanup()
    return GUESS_LIST

getGuess ()

# 9 It's Not Cheating-It's Debugging!
printExercise( "It's Not Cheating-It's Debugging!" )

def getShip ( BOARD, DEBUG=None ):
    functionCall()
    SHIP_LIST = []
    SHIP_LIST.append( random_row( BOARD ) )
    SHIP_LIST.append( random_col( BOARD ) )

    if DEBUG is not None:
        printOutput( SHIP_LIST[0], LABEL="Ship Row: ", DESCRIPTIVE="" )
        printOutput( SHIP_LIST[1], LABEL="Ship Column: ", DESCRIPTIVE="" )

    functionCleanup()
    return SHIP_LIST

BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD, DEBUG="" )
GUESS = getGuess ()

# 10 You win!
printExercise( "You win!" )

BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD, DEBUG="" )
GUESS = getGuess ()

if GUESS[0] == SHIP[0] and GUESS[1] == SHIP[1]:
    printOutput( "Congratulations! You sank my battleship!", LABEL="result: ", DESCRIPTIVE="" )
else:
    printOutput( "You missed my battleship!", LABEL="result: ", DESCRIPTIVE="" )

# 11 Danger, Will Robinson!!
printExercise( "Danger, Will Robinson!!" )

BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD, DEBUG="" )
GUESS = getGuess ()

if GUESS[0] == SHIP[0] and GUESS[1] == SHIP[1]:
    printOutput( "Congratulations! You sank my battleship!", LABEL="result: ", DESCRIPTIVE="" )
else:
    printOutput( "You missed my battleship!", LABEL="result: ", DESCRIPTIVE="" )
    BOARD[GUESS[0]][GUESS[1]] = "X"
    pretty_print_board( BOARD )

# 12 Bad Aim
printExercise( "Bad Aim" )

BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD, DEBUG="" )
GUESS = getGuess ()

if GUESS[0] == SHIP[0] and GUESS[1] == SHIP[1]:
    printOutput( "Congratulations! You sank my battleship!", LABEL="result: ", DESCRIPTIVE="" )
else:
    if GUESS[0] not in range( len( BOARD ) ) or GUESS[1] not in range( len( BOARD[1] ) ):
        printOutput( "Oops,that's not even in the ocean.", LABEL="result: ", DESCRIPTIVE="" )
    else:
        printOutput( "You missed my battleship!", LABEL="result: ", DESCRIPTIVE="" )
        BOARD[GUESS[0]][GUESS[1]] = "X"
        pretty_print_board( BOARD )

# 13 Not Again!
printExercise( "Not Again!" )

BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD, DEBUG="" )
GUESS = getGuess ()

if GUESS[0] == SHIP[0] and GUESS[1] == SHIP[1]:
    printOutput( "Congratulations! You sank my battleship!", LABEL="result: ", DESCRIPTIVE="" )
else:
    if GUESS[0] not in range( len( BOARD ) ) or GUESS[1] not in range( len( BOARD[1] ) ):
        printOutput( "Oops,that's not even in the ocean.", LABEL="result: ", DESCRIPTIVE="" )
    elif BOARD[GUESS[0]][GUESS[1]] == "X":
        printOutput( "You guessed that one already.", LABEL="result: ", DESCRIPTIVE="" )
    else:
        printOutput( "You missed my battleship!", LABEL="result: ", DESCRIPTIVE="" )
        BOARD[GUESS[0]][GUESS[1]] = "X"
        pretty_print_board( BOARD )

# 14 Test Run
printExercise( "Test Run" )

def battleship_round( SHIP, BOARD ):
    GUESS = getGuess ()
    if GUESS[0] == SHIP[0] and GUESS[1] == SHIP[1]:
        printOutput( "Congratulations! You sank my battleship!", LABEL="result: ", DESCRIPTIVE="" )
        BOARD[GUESS[0]][GUESS[1]] = "*"
        OUTPUT_CODE = 0
    else:
        if GUESS[0] not in range( len( BOARD ) ) or GUESS[1] not in range( len( BOARD[1] ) ):
            printOutput( "Oops,that's not even in the ocean.", LABEL="result: ", DESCRIPTIVE="" )
            OUTPUT_CODE = -1
        elif BOARD[GUESS[0]][GUESS[1]] == "X":
            printOutput( "You guessed that one already.", LABEL="result: ", DESCRIPTIVE="" )
            OUTPUT_CODE = -1
        else:
            printOutput( "You missed my battleship!", LABEL="result: ", DESCRIPTIVE="" )
            BOARD[GUESS[0]][GUESS[1]] = "X"
            OUTPUT_CODE = 1
        pretty_print_board( BOARD )
    return OUTPUT_CODE

battleship_round( SHIP, BOARD )

# 15 Play It, Sam
printExercise( "Play It, Sam" )

ROUNDS = 3
ROUND = 0
GAME_PUTPUT = "Lose"
BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD, DEBUG="" )

printOutput( "Start", LABEL="Start: ", DESCRIPTIVE="" )

for ROUND in range( ROUNDS ):
    printOutput( ROUND + 1, LABEL="Round: ", DESCRIPTIVE="" )

    battleship_round( SHIP, BOARD )

printOutput( "You " + GAME_PUTPUT, LABEL="Result: ", DESCRIPTIVE="" )

# 16 Game Over
printExercise( "Game Over" )

ROUNDS = 3
GAME_PUTPUT = "Lose"
BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD, DEBUG="" )
pretty_print_board( BOARD )

printOutput( "Start", LABEL="Start: ", DESCRIPTIVE="" )

for ROUND in range( ROUNDS ):
    printOutput( ROUND + 1, LABEL="Round: ", DESCRIPTIVE="" )

    value = battleship_round( SHIP, BOARD )

    if value == 0:
        GAME_PUTPUT = "Win!"

printOutput( "You " + GAME_PUTPUT, LABEL="Result: ", DESCRIPTIVE="" )

# 17 A Real Win
printExercise( "A Real Win" )

ROUNDS = 3
GAME_PUTPUT = "Lose"
BOARD = generate_board( 5, 5 )
SHIP = getShip ( BOARD )
pretty_print_board( BOARD )

printOutput( "Start", LABEL="Start: ", DESCRIPTIVE="" )

for ROUND in range( ROUNDS ):
    printOutput( ROUND + 1, LABEL="Round: ", DESCRIPTIVE="" )

    value = battleship_round( SHIP, BOARD )

    if value == 0:
        GAME_PUTPUT = "Win!"
        break

printOutput( "You " + GAME_PUTPUT, LABEL="Result: ", DESCRIPTIVE="" )

# 18 To Your Battle Stations!
printExercise( "To Your Battle Stations!" )

# 19 Extra Credit
printExercise( "Extra Credit" )

# Make multiple battleships
printOutput( "Make multiple battleships" )

def getShips ( BOARD, DEBUG=None ):
    functionCall()
    SHIP_LIST = []
    SHIP_LIST.append( random_row( BOARD ) )
    SHIP_LIST.append( random_col( BOARD ) )

    if DEBUG is not None:
        printOutput( SHIP_LIST[0], LABEL="Ship Row: ", DESCRIPTIVE="" )
        printOutput( SHIP_LIST[1], LABEL="Ship Column: ", DESCRIPTIVE="" )

    functionCleanup()
    return SHIP_LIST

# Make battleships of different sizes
printOutput( "Battleships of different sizes" )

# Make your game a two-player game
printOutput( "Two-player game" )

# Use functions to allow your game to have more features like rematches, statistics and more!
printOutput( "Rematches" )

printOutput( "Statistics" )