'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Introduction to Classes", 11 )

printLesson( "Introduction to Classes", 1 )

# 1 Why Use Classes?
printExercise( "Why Use Classes?" )

class Fruit( object ):
    """A class that makes various tasty fruits."""
    def __init__( self, name, color, flavor, poisonous ):
        self.name = name
        self.color = color
        self.flavor = flavor
        self.poisonous = poisonous

    def description( self ):
        printOutput( "I'm a %s %s and I taste %s." % ( self.color, self.name, self.flavor ) )

    def is_edible( self ):
        if not self.poisonous:
            printOutput( "Yep! I'm edible." )
        else:
            printOutput( "Don't eat me! I am super poisonous." )

lemon = Fruit( "lemon", "yellow", "sour", False )
lemon.description()
lemon.is_edible()

# 2 Class Syntax
printExercise( "Class Syntax" )


class Animal_1 ( object ):
    """A class that defines an animal."""
    pass

# 3 Classier Classes
printExercise( "Classier Classes" )

class Animal_2 ( object ):
    """A class that defines an animal."""
    def __init__( self ):
        pass

# 4 Let's Not Get Too Selfish
printExercise( "Let's Not Get Too Selfish" )

class Animal_3 ( object ):
    """A class that defines an animal."""
    def __init__( self, name ):
        self.name = name

# 5 Instantiating Your First Object
printExercise( "Instantiating Your First Object" )

class Square( object ):
    def __init__( self ):
        self.sides = 4

my_shape = Square()
printOutput( "My shapehas '%d' sides" % ( my_shape.sides ) )

class Animal_4 ( object ):
    """A class that defines an animal."""
    def __init__( self, name ):
        self.name = name

zebra = Animal_4( "Jeffrey" )

printOutput( "My animal's name is '%s'" % ( zebra.name ) )

# 6 More on __init__() and self
printExercise( "More on __init__() and self" )

class Animal_5( object ):
    """Makes cute animals."""
    # For initializing our instance objects
    def __init__( self, name, age, is_hungry ):
        self.name = name
        self.age = age
        self.is_hungry = is_hungry

zebra = Animal_5( "Jeffrey", 2, True )
giraffe = Animal_5( "Bruce", 1, False )
panda = Animal_5( "Chad", 7, True )

printOutput( "My zebra's name is '%s', it is '%s' and it is hungry '%s'" % ( zebra.name, zebra.age, zebra.is_hungry ) )
printOutput( "My giraffe's name is '%s', it is '%s' and it is hungry '%s'" % ( giraffe.name, giraffe.age, giraffe.is_hungry ) )
printOutput( "My panda's name is '%s', it is '%s' and it is hungry '%s'" % ( panda.name, panda.age, panda.is_hungry ) )

# 7 Class Scope
printExercise( "Class Scope" )

class Animal_6( object ):
    """Makes cute animals."""
    is_alive = True
    def __init__( self, name, age ):
        self.name = name
        self.age = age

zebra = Animal_6( "Jeffrey", 2 )
giraffe = Animal_6( "Bruce", 1 )
panda = Animal_6( "Chad", 7 )

printOutput( "My zebra's name is '%s', it is '%s' and it is alive '%s'" % ( zebra.name, zebra.age, zebra.is_alive ) )
printOutput( "My giraffe's name is '%s', it is '%s' and it is alive '%s'" % ( giraffe.name, giraffe.age, giraffe.is_alive ) )
printOutput( "My panda's name is '%s', it is '%s' and it is alive '%s'" % ( panda.name, panda.age, panda.is_alive ) )

# 8 A Methodical Approach
printExercise( "A Methodical Approach" )

hippo = Animal_6( "Jake", 12 )
cat = Animal_6( "Boots", 3 )

printOutput( "My hippo's is alive '%s'" % ( hippo.is_alive ) )
hippo.is_alive = False
printOutput( "My hippo's is alive '%s'" % ( hippo.is_alive ) )

printOutput( "My cat's is alive '%s'" % ( cat.is_alive ) )

class Animal_7( object ):
    """Makes cute animals."""
    is_alive = True
    def __init__( self, name, age ):
        self.name = name
        self.age = age
    def description ( self ):
        printOutput( "My animals's name is '%s' and it is '%s'" % ( self.name, self.age ) )

hippo = Animal_7( "Hungry", 5 )
hippo.description()

# 9 They're Multiplying!
printExercise( "They're Multiplying!" )


class Animal_8( object ):
    """Makes cute animals."""
    is_alive = True
    health = "good"
    def __init__( self, name, age ):
        self.name = name
        self.age = age
    def description ( self ):
        printOutput( "My animals's name is '%s' and it is '%s'" % ( self.name, self.age ) )

hippo = Animal_8( "Hungry", 5 )
sloth = Animal_8( "Bill", 12 )
ocelot = Animal_8( "HanniBill", 12 )
printOutput( "My hippo's health is '%s'" % ( hippo.health ) )
printOutput( "My sloth's health is '%s'" % ( sloth.health ) )
printOutput( "My ocelot's health is '%s'" % ( ocelot.health ) )

# 10 It's Not All Animals and Fruits
printExercise( "It's Not All Animals and Fruits" )

class ShoppingCart( object ):
    """Creates shopping cart objects for users of our fine website."""
    items_in_cart = {}
    def __init__( self, customer_name ):
        self.customer_name = customer_name
    def add_item( self, product, price ):
        """Add product to the cart."""
        if not product in self.items_in_cart:
            self.items_in_cart[product] = price
            printOutput( "'%s' added to Shopping Cart." % ( product ) )
        else:
            printOutput( "'%s' is already in the Shopping Cart." % ( product ) )

    def remove_item( self, product ):
        """Remove product from the cart."""
        if product in self.items_in_cart:
            del self.items_in_cart[product]
            printOutput( "'%s' removed from the Shopping Cart." % ( product ) )
        else:
            printOutput( "'%s' is not in the Shopping Cart." % ( product ) )

my_cart = ShoppingCart ( "Jasen" )
printOutput( "'%s''s Shopping Cart." % ( my_cart.customer_name ) )
my_cart.add_item( "Milk", 3 )
my_cart.add_item( "Bread", 2 )
my_cart.add_item( "Meat", 1 )
my_cart.add_item( "Meat", 1 )

# 11 Warning: Here Be Dragons
printExercise( "Warning: Here Be Dragons" )

class Customer( object ):
    """Produces objects that represent customers."""
    def __init__( self, customer_id ):
        self.customer_id = customer_id
    def display_cart( self ):
        printOutput( "I'm a string that stands in for the contents of your shopping cart!" )

class ReturningCustomer( Customer ):
    """For customers of the repeat variety."""
    def display_order_history( self ):
        printOutput ( "I'm a string that stands in for your order history!" )

monty_python = ReturningCustomer( "ID: 12345" )
printOutput( "Customer ID: '%s'." % ( monty_python.customer_id ) )
monty_python.display_cart()
monty_python.display_order_history()

# 12 Inheritance Syntax
printExercise( "Inheritance Syntax" )

class Shape( object ):
    """Makes shapes!"""
    def __init__( self, number_of_sides ):
        self.number_of_sides = number_of_sides

class Triangle_1( Shape ):
    def __init__( self, side1, side2, side3 ):
        self.side1 = side1
        self.side2 = side2
        self.side3 = side3

# 13 Override!
printExercise( "Override!" )

class Employee_1( object ):
    def __init__( self, name ):
        self.name = name
    def greet( self, other ):
        printOutput( "Hello, %s." % ( other.name ) )

class CEO( Employee_1 ):
    def greet( self, other ):
        printOutput( "Get back to work, %s!" % ( other.name ) )

ceo = CEO( "Emily" )
emp = Employee_1( "Steve" )
emp.greet( ceo )
ceo.greet( emp )

class Employee_2( object ):
    """Models real-life employees!"""
    def __init__( self, employee_name ):
        self.employee_name = employee_name
    def calculate_wage( self, hours ):
        self.hours = hours
        return hours * 20.00

# Add your code below!
class PartTimeEmployee_1( Employee_2 ):
    def calculate_wage( self, hours ):
        self.hours = hours
        return hours * 12.00

# 14 This Looks Like a Job For...
printExercise( "This Looks Like a Job For..." )

class PartTimeEmployee_2( Employee_2 ):
    def calculate_wage( self, hours ):
        self.hours = hours
        return hours * 12.00
    def full_time_wage( self, hours ):
        return super( PartTimeEmployee_2, self ).calculate_wage( hours )

milton = PartTimeEmployee_2( "Milton" )
hours = 10

printOutput( "Employee '%s' worked '%d' hours earning '$%s'" % ( milton.employee_name, hours, milton.full_time_wage( hours ) ) )

# 15 Class Basics
printExercise( "Class Basics" )

class Triangle_2( object ):
    def __init__( self, angle1, angle2, angle3 ):
        self.angle1 = angle1
        self.angle2 = angle2
        self.angle3 = angle3

# 16 Class It Up
printExercise( "Class It Up" )

class Triangle_3( object ):
    number_of_sides = 3
    def __init__( self, angle1, angle2, angle3 ):
        self.angle1 = angle1
        self.angle2 = angle2
        self.angle3 = angle3
    def check_angles( self ):
        if ( self.angle1 + self.angle2 + self.angle3 ) == 180:
            return True
        else:
            return False

sample_angle_1 = 90
sample_angle_2 = 60
sample_angle_3 = 45
sample_angle_4 = 30
sample1 = Triangle_3( sample_angle_1, sample_angle_3, sample_angle_3 )
sample2 = Triangle_3( sample_angle_1, sample_angle_2, sample_angle_4 )
sample3 = Triangle_3( sample_angle_1, sample_angle_1, sample_angle_1 )

printOutput( "Triangle with angles: '%d', '%d', '%d' is valid '%s'" % ( sample2.angle1, sample2.angle2, sample2.angle3, sample2.check_angles() ) )
printOutput( "Triangle with angles: '%d', '%d', '%d' is valid '%s'" % ( sample3.angle1, sample3.angle2, sample3.angle3, sample3.check_angles() ) )

# 17 Instantiate an Object
printExercise( "Instantiate an Object" )

printOutput( "Triangles have '%d' sides'" % ( sample1.number_of_sides ) )
printOutput( "Triangle with angles: '%d', '%d', '%d' is valid '%s'" % ( sample1.angle1, sample1.angle2, sample1.angle3, sample1.check_angles() ) )

# 18 Inheritance
printExercise( "Inheritance" )

class Equilateral( Triangle_3 ):
    angle = 60
    def __init__( self ):
        self.angle1 = self.angle
        self.angle2 = self.angle
        self.angle3 = self.angle

sample4 = Equilateral()

printOutput( "Triangle with angles: '%d', '%d', '%d' is valid '%s'" % ( sample4.angle1, sample4.angle2, sample4.angle3, sample4.check_angles() ) )
