'''
Created on May 30,2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import functionCall
from Python_Library import functionCleanup
from Python_Library import inputInt

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python
printChapter( "Lists and Functions", 7 )

printLesson( "Extra Credit", 3 )

# 19 Extra Credit
printExercise( "Extra Credit" )

from random import randint

BOARD_DIMENSION = 8
GAME_ROUNDS = 30
SHIP_TYPES = ["Aircraft Carrier", "Battleship", "Destroyer", "Submarine", "Patrol Boat"]
SHIP_SIZES = [5, 4, 3, 3, 2]
EMPTY = "O"
HIT = "X"
MISS = "*"
LOSE = "Lose"
WIN = "Win!!!"

def generateBoard( ROW_COUNT, COLUMN_COUNT ):
    BOARD = []
    for _ in range( ROW_COUNT ):
        ROW = []
        for _ in range( COLUMN_COUNT ):
            ROW.append( EMPTY )
        BOARD.append( ROW )
    return BOARD

def getRow ( BOARD, OFFEST ):
    return randint( 0, len( BOARD ) - OFFEST - 1 )

def getCoulmn ( BOARD, OFFEST ):
    return randint( 0, len( BOARD[1] ) - OFFEST - 1 )

def prettyPrintBoard ( BOARD ):
    functionCall()
    for BOARD_ROW in BOARD:
        printOutput( " ".join( BOARD_ROW ) )
    functionCleanup()

def getShipDetails ():
    functionCall()
#    SHIP_COUNT = inputInt("\t\tHow many ships: ")
    SHIP_DETAILS = []
    for SHIP in SHIP_SIZES:
        SHIP_DETAIL = []
        # Index 0 = Size
        SHIP_DETAIL.append( SHIP )
        # Index 1 = Orientation
        SHIP_DETAIL.append( randint( 0, 1 ) )
        SHIP_DETAILS.append( SHIP_DETAIL )
    functionCleanup()
    return SHIP_DETAILS

def shipVertical( BOARD_TARGET, SHIP_ID, SHIP_DATA ):
    functionCall()
    VALUE = None
    while not VALUE:
        VALUE = 1
        SHIP_shipVertical = []
        SHIP_shipVertical.append( getRow( BOARD_TARGET, SHIP_DATA[0] ) )
        SHIP_shipVertical.append( getCoulmn( BOARD_TARGET, 0 ) )
        for INDEX in range( SHIP_DATA[0] ):
            # Prevent collisions
            if BOARD_TARGET[SHIP_shipVertical[0] + INDEX ][SHIP_shipVertical[1]] != EMPTY:
                VALUE = None
                break
        if VALUE:
            for INDEX in range( SHIP_DATA[0] ):
                BOARD_TARGET[SHIP_shipVertical[0] + INDEX ][SHIP_shipVertical[1]] = str( SHIP_ID )
    functionCleanup()
    return SHIP_DATA + SHIP_shipVertical

def shipHorizontal( BOARD_TARGET, SHIP_ID, SHIP_DATA ):
    functionCall()
    VALUE = None
    while not VALUE:
        VALUE = 1
        SHIP_shipHorizontal = []
        SHIP_shipHorizontal.append( getRow( BOARD_TARGET, 0 ) )
        SHIP_shipHorizontal.append( getCoulmn( BOARD_TARGET, SHIP_DATA[0] ) )
        for INDEX in range( SHIP_DATA[0] ):
            # Prevent collisions
            if BOARD_TARGET[SHIP_shipHorizontal[0]][SHIP_shipHorizontal[1] + INDEX ] != EMPTY:
                VALUE = None
                break

        if VALUE:
            for INDEX in range( SHIP_DATA[0] ):
                BOARD_TARGET[SHIP_shipHorizontal[0]][SHIP_shipHorizontal[1] + INDEX ] = str( SHIP_ID )
    functionCleanup()
    return SHIP_DATA + SHIP_shipHorizontal

# Sunk Row Column Orientation Size
shipOptions = {
    0 : shipVertical,
    1 : shipHorizontal
    }

def getShips ( SHIP_DETAILS, BOARD_TARGET, DEBUG=None ):
    functionCall()
    SHIP_LIST = []
    for SHIP_ID in range( len( SHIP_DETAILS ) ):
        SHIP_DETAIL = shipOptions[SHIP_DETAILS[SHIP_ID][1]]( BOARD_TARGET, SHIP_ID, SHIP_DETAILS[SHIP_ID] )
        SHIP_LIST.append( SHIP_DETAIL )
        if DEBUG is not None:
            printOutput( SHIP_ID, LABEL="Ship ID: " )
            printOutput( SHIP_DETAIL, LABEL="Ship Detail: " )

            printOutput( SHIP_DETAIL[0], LABEL="Ship Row: " )
            printOutput( SHIP_DETAIL[1], LABEL="Ship Column: " )

    functionCleanup()
    return SHIP_LIST

def getGuess ():
    functionCall()
    GUESS_LIST = []
    GUESS_LIST.append( inputInt( "\t\tGuess Row: " ) - 1 )
    GUESS_LIST.append( inputInt( "\t\tGuess Column: " ) - 1 )
    functionCleanup()
    return GUESS_LIST

def getNextPlayer ( PLAYER, PLAYERS ):
    return ( PLAYER + 1 ) % len( PLAYERS )

# battleship_round(PLAYERS[PLAYER][3], PLAYERS[PLAYER][0], PLAYERS[PLAYER][1])
def battleship_round( PLAYER, PLAYERS ):

    NEXT_PLAYER = getNextPlayer( PLAYER, PLAYERS )
    SHIP_LIST = PLAYERS[NEXT_PLAYER][3]
    BOARD_DISPLAY = PLAYERS[NEXT_PLAYER][0]
    BOARD_TARGET = PLAYERS[NEXT_PLAYER][1]
    OUTPUT_CODE = None
    while not OUTPUT_CODE:
        GUESS = getGuess ()
        OUTPUT_CODE = 1
        if GUESS[0] not in range( len( BOARD_TARGET ) ) or GUESS[1] not in range( len( BOARD_TARGET[1] ) ):
            printOutput( "Oops,that's not even in the ocean." )
            OUTPUT_CODE = None
        elif BOARD_TARGET[GUESS[0]][GUESS[1]] == HIT or BOARD_TARGET[GUESS[0]][GUESS[1]] == MISS:
            printOutput( "You guessed that one already." )
            OUTPUT_CODE = None
        elif BOARD_TARGET[GUESS[0]][GUESS[1]] != EMPTY:
            printOutput( "You Hit!" )
            SHIP_ID = int( BOARD_TARGET[GUESS[0]][GUESS[1]] )
            BOARD_TARGET[GUESS[0]][GUESS[1]] = HIT
            BOARD_DISPLAY[GUESS[0]][GUESS[1]] = HIT
            SHIP_LIST[SHIP_ID][0] -= 1
            if SHIP_LIST[SHIP_ID][0] == 0:
                printOutput( "You Sunk a Ship!!!" )
        else:
            printOutput( "You missed." )
            BOARD_TARGET[GUESS[0]][GUESS[1]] = MISS
            BOARD_DISPLAY[GUESS[0]][GUESS[1]] = MISS
        prettyPrintBoard( BOARD_DISPLAY )

    return OUTPUT_CODE

def detectWin( PLAYER, PLAYERS ):
    functionCall()
    WIN_CODE = 1
    NEXT_PLAYER = getNextPlayer( PLAYER, PLAYERS )
    SHIP_LIST = PLAYERS[NEXT_PLAYER][3]
    for SHIP_ID in range( len( SHIP_LIST ) ):
        if SHIP_LIST[SHIP_ID][0] != 0:
            WIN_CODE = None
            break
    functionCleanup()
    return WIN_CODE

def statistics( SHIP_LIST ):
    functionCall()
    for SHIP_ID in range( len( SHIP_LIST ) ):
        if SHIP_LIST[SHIP_ID][0] == 0:
            printOutput( "Ship %s: Destroyed" % ( SHIP_TYPES[SHIP_ID] ) )
        else:
            printOutput( "Ship %s: had %d of %d hits remaining" % ( SHIP_TYPES[SHIP_ID], SHIP_LIST[SHIP_ID][0], SHIP_SIZES[SHIP_ID] ) )
    functionCleanup()

def createPlayers():
    PLAYER_COUNT = inputInt( "\t\tHow many players? " )
    PLAYERS = []
    for _ in range( PLAYER_COUNT ):
        PLAYER_DATA = []
        # Index 0 = BOARD_DISPLAY
        PLAYER_DATA.append( generateBoard( BOARD_DIMENSION, BOARD_DIMENSION ) )
        # Index 1 = BOARD_TARGET
        PLAYER_DATA.append( generateBoard( BOARD_DIMENSION, BOARD_DIMENSION ) )
        # Index 2 = SHIP_DETAILS
        PLAYER_DATA.append( getShipDetails () )
        # Index 3 = SHIP_LIST
        PLAYER_DATA.append( getShips( PLAYER_DATA[2], PLAYER_DATA[1] ) )
        PLAYERS.append( PLAYER_DATA )
    return PLAYERS

def playGame():
    END_GAME = None
    printOutput( "Test" )
    PLAYERS = createPlayers()
    printOutput( "Begin Game" )
    printOutput( "Test" )
    for ROUND in range( GAME_ROUNDS ):
        for PLAYER in range( len( PLAYERS ) ):
            printOutput( "Player %d Round: %d" % ( PLAYER + 1, ROUND + 1 ) )
            printOutput( ROUND + 1, LABEL="Round: " )
            prettyPrintBoard( PLAYERS[PLAYER][0] )
            battleship_round( PLAYER, PLAYERS )
            if detectWin( PLAYER, PLAYERS ):
                END_GAME = WIN
                break
        if END_GAME:
            break

    for PLAYER in range( len( PLAYERS ) ):
        printOutput( "Player %d Statistics" % ( PLAYER + 1 ) )

        printOutput( "Game Board" )
        prettyPrintBoard( PLAYERS[PLAYER][1] )
        printOutput( "Ships" )
        statistics( PLAYERS[PLAYER][3] )

# Make multiple battleships
printOutput( "Begin Game" )
playGame()
printOutput( "Game Over" )

"""
Items to do,
3.  Detect Player win
    If all ships are not sunk, determine who did better.
      a. Who sunk more ships
      b. Who had more hits remaining on their ships
4.  Allow forfit
5.  Allow re-match
"""