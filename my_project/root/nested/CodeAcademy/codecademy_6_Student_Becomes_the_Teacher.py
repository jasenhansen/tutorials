'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Student Becomes the Teacher", 6 )

# 1 Lesson Number One
printExercise( "Lesson Number One" )

lloyd = {
    "name": "Lloyd",
    "homework": [],
    "quizzes": [],
    "tests": []
    }

alice = {
    "name": "Alice",
    "homework": [],
    "quizzes": [],
    "tests": []
    }

tyler = {
    "name": "Tyler",
    "homework": [],
    "quizzes": [],
    "tests": []
    }

printOutput( lloyd, LABEL="lloyd: ", DESCRIPTIVE="" )

for item in lloyd:
    printOutput( "%s: %s" % ( item, lloyd[item] ), LABEL="lloyd: ", DESCRIPTIVE="" )

printOutput( alice, LABEL="alice: ", DESCRIPTIVE="" )

for item in alice:
    printOutput( "%s: %s" % ( item, alice[item] ), LABEL="alice: ", DESCRIPTIVE="" )

printOutput( tyler, LABEL="tyler: ", DESCRIPTIVE="" )

for item in tyler:
    printOutput( "%s: %s" % ( item, tyler[item] ), LABEL="tyler: ", DESCRIPTIVE="" )

# 2 What's the Score?
printExercise( "What's the Score?" )

lloyd = {
    "name": "Lloyd",
    "homework": [90.0, 97.0, 75.0, 92.0],
    "quizzes": [88.0, 40.0, 94.0],
    "tests": [75.0, 90.0]
    }

alice = {
    "name": "Alice",
    "homework": [100.0, 92.0, 98.0, 100.0],
    "quizzes": [82.0, 83.0, 91.0],
    "tests": [89.0, 97.0]
    }

tyler = {
    "name": "Tyler",
    "homework": [0.0, 87.0, 75.0, 22.0],
    "quizzes": [0.0, 75.0, 78.0],
    "tests": [100.0, 100.0]
    }

printOutput( lloyd, LABEL="lloyd: ", DESCRIPTIVE="" )

for item in lloyd:
    printOutput( "%s: %s" % ( item, lloyd[item] ), LABEL="lloyd: ", DESCRIPTIVE="" )

printOutput( alice, LABEL="alice: ", DESCRIPTIVE="" )

for item in alice:
    printOutput( "%s: %s" % ( item, alice[item] ), LABEL="alice: ", DESCRIPTIVE="" )

printOutput( tyler, LABEL="tyler: ", DESCRIPTIVE="" )

for item in tyler:
    printOutput( "%s: %s" % ( item, tyler[item] ), LABEL="tyler: ", DESCRIPTIVE="" )

# 3 Put It Together
printExercise( "Put It Together" )

students = [lloyd, alice, tyler]

printOutput( students, LABEL="students: ", DESCRIPTIVE="" )

for student in students:
    printOutput( student, LABEL="student: ", DESCRIPTIVE="" )
    for item in student:

        printOutput( "%s: %s" % ( item, student[item] ), LABEL="%s: " % ( student["name"] ), DESCRIPTIVE="" )

# 4 For the Record
printExercise( "For the Record" )

printOutput( students, LABEL="students: ", DESCRIPTIVE="" )

for student in students:
    printOutput( student, LABEL="student: ", DESCRIPTIVE="" )

    printOutput( "%s" % ( student["name"] ), LABEL="%s: " % ( student["name"] ), DESCRIPTIVE="" )
    printOutput( "%s" % ( student["homework"] ), LABEL="%s: " % ( student["name"] ), DESCRIPTIVE="" )
    printOutput( "%s" % ( student["quizzes"] ), LABEL="%s: " % ( student["name"] ), DESCRIPTIVE="" )
    printOutput( "%s" % ( student["tests"] ), LABEL="%s: " % ( student["name"] ), DESCRIPTIVE="" )

# 5 It's Okay to be Average
printExercise( "It's Okay to be Average" )

def average( numbers ):
    total = sum( numbers )
    total = float( total )
    return total / len( numbers )

numbers = [1, 2, 3, 4, 5]

printOutput( "%.2f" % average( numbers ), LABEL="average: ", DESCRIPTIVE="" )

# 6 Just Weight and See
printExercise( "Just Weight and See" )

def get_average( student ):
    average_homework = average( student["homework"] )
    average_quizzes = average( student["quizzes"] )
    average_tests = average( student["tests"] )
    return ( 0.1 * average_homework ) + ( 0.3 * average_quizzes ) + ( 0.6 * average_tests )

printOutput( students, LABEL="students: ", DESCRIPTIVE="" )

for student in students:
    printOutput( student, LABEL="student: ", DESCRIPTIVE="" )

    printOutput( "%.2f" % get_average( student ), LABEL="%s: " % ( student["name"] ), DESCRIPTIVE="" )

# 7 Sending a Letter
printExercise( "Sending a Letter" )

def get_letter_grade( number ):
    if number >= 90 :
        return "A"
    elif number >= 80 :
        return "B"
    elif number >= 70 :
        return "C"
    elif number >= 60 :
        return "D"
    else:
        return "F"

printOutput( students, LABEL="students: ", DESCRIPTIVE="" )

for student in students:
    printOutput( student, LABEL="student: ", DESCRIPTIVE="" )

    average_value = get_average( student )
    printOutput( get_letter_grade( average_value ), LABEL="%s: score %.2f: grade:  " % ( student["name"], average_value ), DESCRIPTIVE="" )

# 8 Part of the Whole
printExercise( "Part of the Whole" )

def get_class_average ( students ):
    results = []
    for student in students:
        average_value = get_average( student )
#        average_value = round(average_value, 2)
        results.append( average_value )
    return average( results )

printOutput( get_class_average ( students ), LABEL="class average: ", DESCRIPTIVE="" )

# 9 How is Everybody Doing?
printExercise( "How is Everybody Doing?" )

class_average = get_class_average ( students )

printOutput( "%.2f" % class_average, LABEL="class average:", DESCRIPTIVE="" )
printOutput( get_letter_grade( class_average ), LABEL="class grade:", DESCRIPTIVE="" )
