'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import is_numeric

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Functions", 4 )

printLesson( "Taking a Vacation", 2 )

# 1 Before We Begin
printExercise( "Before We Begin" )

def answer():
    return 42

printOutput( "The answer is '%d'" % ( answer() ) )

# 2 Planning Your Trip
printExercise( "Planning Your Trip" )

def hotel_cost( nights ):
    if is_numeric( nights ):
        if nights >= 0:
            return 140 * nights
        else:
            return "You have to actually stay to have a cost ..."
    else:
        return "Not a valid number of nights"

value_1 = -1
value_2 = 8
value_3 = "Cat"
printOutput( "The cost of a hotel for '%s' nights is '%s'" % ( value_1, hotel_cost( value_1 ) ) )
printOutput( "The cost of a hotel for '%s' nights is '%s'" % ( value_2, hotel_cost( value_2 ) ) )
printOutput( "The cost of a hotel for '%s' nights is '%s'" % ( value_3, hotel_cost( value_3 ) ) )

# 3 Getting There
printExercise( "Getting There" )

def plane_ride_cost( destination ):
    if destination == "Charlotte":
        return 183
    elif destination == "Tampa":
        return 220
    elif destination == "Pittsburgh":
        return 222
    elif destination == "Los Angeles":
        return 475
    else:
        return "No flights avialable"

value_1 = "Charlotte"
value_2 = "Tampa"
value_3 = "Pittsburgh"
value_4 = "Los Angeles"
value_5 = "New York"
printOutput( "The cost of a plane ride to '%s' is '%s'" % ( value_1, plane_ride_cost( value_1 ) ) )
printOutput( "The cost of a plane ride to '%s' is '%s'" % ( value_2, plane_ride_cost( value_2 ) ) )
printOutput( "The cost of a plane ride to '%s' is '%s'" % ( value_3, plane_ride_cost( value_3 ) ) )
printOutput( "The cost of a plane ride to '%s' is '%s'" % ( value_4, plane_ride_cost( value_4 ) ) )
printOutput( "The cost of a plane ride to '%s' is '%s'" % ( value_5, plane_ride_cost( value_5 ) ) )

# 4 Transportation
printExercise( "Transportation" )

def rental_car_cost( days ):
    cost = 0
    if is_numeric( days ):
        if days >= 0:
            cost = 40 * days
        else:
            return "You have to actually drive to have a cost ..."
    else:
        return "Not a valid number of days"
    if days >= 7:
        cost -= 50
    elif days >= 3:
        cost -= 20
    return cost

value_1 = 0
value_2 = 1
value_3 = 3
value_4 = 8
value_5 = -2
value_6 = "Cat"
printOutput( "The cost of a rental car for '%s' days is '%s'" % ( value_1, rental_car_cost( value_1 ) ) )
printOutput( "The cost of a rental car for '%s' days is '%s'" % ( value_2, rental_car_cost( value_2 ) ) )
printOutput( "The cost of a rental car for '%s' days is '%s'" % ( value_3, rental_car_cost( value_3 ) ) )
printOutput( "The cost of a rental car for '%s' days is '%s'" % ( value_4, rental_car_cost( value_4 ) ) )
printOutput( "The cost of a rental car for '%s' days is '%s'" % ( value_5, rental_car_cost( value_5 ) ) )
printOutput( "The cost of a rental car for '%s' days is '%s'" % ( value_6, rental_car_cost( value_6 ) ) )

# 5 Pull it Together
printExercise( "Pull it Together" )

def trip_cost( city, days ):
    total = 0
    if is_numeric( days ):
        if days >= 0:
            total += hotel_cost( days )
            total += rental_car_cost( days )
        else:
            return "You have to actually Travel to have a cost ..."
    flight = plane_ride_cost( city )
    if is_numeric( flight ):
        return total + flight
    else:
        return flight

value_1 = "Charlotte"
value_2 = "Tampa"
value_3 = "Pittsburgh"
value_4 = "Los Angeles"
value_5 = "New York"
value_6 = 0
value_7 = 5
value_8 = 12
value_9 = -1
printOutput( "The cost of a trip to '%s' for '%s' days is '%s'" % ( value_1, value_6, trip_cost( value_1, value_6 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days is '%s'" % ( value_2, value_7, trip_cost( value_2, value_7 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days is '%s'" % ( value_4, value_8, trip_cost( value_4, value_8 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days is '%s'" % ( value_3, value_9, trip_cost( value_3, value_9 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days is '%s'" % ( value_5, value_8, trip_cost( value_5, value_8 ) ) )

# 6 Hey, You Never Know!
printExercise( "Hey, You Never Know!" )

def trip_cost_1( city, days, spending_money ):
    total = 0
    if is_numeric( days ):
        if days >= 0:
            total += hotel_cost( days )
            total += rental_car_cost( days )
        else:
            return "You have to actually Travel to have a cost ..."
    flight = plane_ride_cost( city )
    if is_numeric( flight ):
        total += flight
    else:
        return flight
    if is_numeric( spending_money ):
        if spending_money >= 0:
            return total + spending_money
        else:
            return "You need spending money"
    else:
        return "You can not afford this trip with no spending money"

value_1 = "Charlotte"
value_2 = "Tampa"
value_3 = "Pittsburgh"
value_4 = "Los Angeles"
value_5 = "New York"
value_6 = 0
value_7 = 5
value_8 = 12
value_9 = -1
value_10 = 71
value_11 = 82
value_12 = 93
value_13 = 100
value_14 = -100
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_1, value_6, value_10, trip_cost_1( value_1, value_6, value_10 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_2, value_7, value_11, trip_cost_1( value_2, value_7, value_11 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_4, value_8, value_12, trip_cost_1( value_4, value_8, value_12 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_4, value_8, value_6, trip_cost_1( value_4, value_8, value_6 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_3, value_9, value_13, trip_cost_1( value_3, value_9, value_13 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_5, value_8, value_13, trip_cost_1( value_5, value_8, value_13 ) ) )
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_4, value_8, value_14, trip_cost_1( value_4, value_8, value_14 ) ) )

# 7 Plan Your Trip!
printExercise( "Plan Your Trip!" )

value_1 = "Los Angeles"
value_2 = 5
value_3 = 600
printOutput( "The cost of a trip to '%s' for '%s' days with '%s' spending money is '%s'" % ( value_1, value_2, value_3, trip_cost_1( value_1, value_2, value_3 ) ) )
