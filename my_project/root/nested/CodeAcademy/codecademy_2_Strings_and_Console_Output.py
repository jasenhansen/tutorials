'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Strings and Console Output", 2 )

printLesson( "Strings & Console Output", 1 )

# 1 The Strings
printExercise( "Strings" )

brian = "Hello life!"

printOutput( "Brian says '%s'" % ( brian ) )

# 2 Practice
printExercise( "Practice" )

caesar = "Graham"
praline = "John"
viking = "Teresa"

printOutput( "Caesar likes '%s'" % ( caesar ) )
printOutput( "Praline likes '%s'" % ( praline ) )
printOutput( "Viking likes '%s'" % ( viking ) )

# 3 Escaping characters
printExercise( "Escaping characters" )

escaped = "This isn\'t flying, this is falling with style!"

printOutput( "My escaped string is '%s'" % ( escaped ) )

# 4 Access by Index
printExercise( "Access by Index" )

monty = "MONTY"
fifth_letter = monty[4]

printOutput( "The fifth_letter of '%s' is '%s'" % ( monty, fifth_letter ) )

# 5 String methods
printExercise( "String methods" )

parrot = "Norwegian Blue"
printOutput( "The length of '%s' is '%d'" % ( parrot, len( parrot ) ) )

# 6 lower()
printExercise( "lower()" )

printOutput( "'%s' set to lower is '%s' " % ( parrot, parrot.lower() ) )

# 7 upper()
printExercise( "upper()" )

printOutput( "'%s' set to upper is '%s' " % ( parrot, parrot.upper() ) )

# 8 str()
printExercise( "str()" )

pi = 3.14

printOutput( "The String version of Float '%.2f' is '%s' " % ( pi, str( pi ) ) )

# 9 Dot Notation
printExercise( "Dot Notation" )

ministry = "The Ministry of Silly Walks"

printOutput( "The length of '%s' is '%d'" % ( ministry, len( ministry ) ) )
printOutput( "'%s' set to upper is '%s'" % ( ministry, ministry.upper() ) )

# 10 Printing Strings
printExercise( "Printing Strings" )

value_1 = "Monty Python"

printOutput( "The my String is '%s'" % ( value_1 ) )

# 11 Printing Variables
printExercise( "Printing Variables" )

the_machine_goes = "Ping!"

printOutput( "The machine goes '%s'" % ( the_machine_goes ) )

# 12 String Concatenation
printExercise( "String Concatenation" )

value_1 = "Spam "
value_2 = "and "
value_3 = "eggs"

value_4 = value_1 + value_2 + value_3

printOutput( "The Strings '%s' + '%s' + '%s' is '%s'" % ( value_1, value_2, value_3, value_4 ) )

# 13 Explicit String Conversion
printExercise( "Explicit String Conversion" )

printOutput( "The value of pi is around '%s'" % ( str( 3.14 ) ) )

# 14 String Formatting with %, Part 1
printExercise( "String Formatting with %, Part 1" )

string_1 = "Camelot"
string_2 = "place"
string_3 = "Let's not go to %s. 'Tis a silly %s." % ( string_1, string_2 )

printOutput( "The adding the Strings '%s' and '%s' to the base template is '%s'" % ( string_1, string_2, string_3 ) )

# 15 String Formatting with %, Part 2
printExercise( "String Formatting with %, Part 2" )

name = "Sir Lancelot of Camelot"
quest = "To seek the Holy Grail"
color = "Blue"

value = "What ... is your name? " + name
printOutput( value )

value = "What ... is your quest? " + quest
printOutput( value )

value = "What ... is your favorite color? " + color
printOutput( value )

printOutput( "Ah, so your name is %s, your quest is %s, and your favorite color is %s." % ( name, quest, color ) )

# 16 And Now, For Something Completely Familiar
printExercise( "And Now, For Something Completely Familiar" )

my_string = "My name is Sir Lancelot of Camelot"

printOutput( "The length of '%s' is '%d'" % ( my_string, len( my_string ) ) )
printOutput( "'%s' set to upper is '%s' " % ( my_string, my_string.upper() ) )
