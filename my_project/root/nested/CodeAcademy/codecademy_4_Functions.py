'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import functionCall
from Python_Library import functionCleanup

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Functions", 4 )

printLesson( "Functions", 1 )

# 1 What Good are Functions?
printExercise( "What Good are Functions?" )

def tax( bill ):
    """Adds 8% tax to a restaurant bill."""
    functionCall( INCREMENT="" )
    tax = 0.08
    total = bill + bill * tax
    printOutput( "The bill of '${0:.2f}' with {1:.0%} tax is '${2:.2f}'".format( bill, tax, total ) )
    functionCleanup()
    return total

def tip( bill ):
    """Adds 15% tip to a restaurant bill."""
    functionCall( INCREMENT="" )
    tip = 0.15
    total = bill + bill * tip
    printOutput( "$%.2f" % bill, LABEL="With tip: " )
    printOutput( "The bill of '${0:.2f}' with {1:.0%} tip is '${2:.2f}'".format( bill, tip, total ) )
    functionCleanup()
    return total

meal_cost = 100

printOutput( "The initial meal costs '${0:.2f}'".format( meal_cost ) )
meal_with_tax = tax( meal_cost )
tip( meal_with_tax )

# 2 Function Junction
printExercise( "Function Junction" )

def spam():
    """Test Function"""
    functionCall( INCREMENT="" )
    printOutput( "Function 'spam()' outputs 'Eggs!'" )
    functionCleanup()

spam()

# 3 Function Junction
printExercise( "Call and Response" )

def square( number ):
    """Returns the square of a number."""
    functionCall( INCREMENT="" )
    squared = number ** 2
    printOutput( "'%d' squared is '%d'." % ( number, squared ) )
    functionCleanup()
    return squared

square( 10 )

# 4 Parameters and Arguments
printExercise( "Parameters and Arguments" )

def power( base, exponent ):
    functionCall( INCREMENT="" )
    result = base ** exponent
    printOutput( "'%d' to the power of '%d' is '%d'." % ( base, exponent, result ) )
    functionCleanup()

power( 37, 4 )

# 5 Functions Calling Functions
printExercise( "Functions Calling Functions" )

def one_good_turn( n ):
    return n + 1

def deserves_another( n ):
    return one_good_turn( n ) + 2

base = 10

printOutput( "Function 'one_good_turn()' with input '%d' outputs '%d'" % ( base, one_good_turn( base ) ) )
printOutput( "Function 'deserves_another()' with input '%d' outputs '%d'" % ( base, deserves_another( base ) ) )

# 6 Practice Makes Perfect
printExercise( "Practice Makes Perfect" )

def cube( number ):
    return number ** 3

def by_three( number ):
    if ( number % 3 ) == 0:
        return cube( number )
    else:
        return False

base = 10
printOutput( "'%d' cubed is '%s'" % ( base, cube( base ) ) )
printOutput( "'%d' by_three is '%s'" % ( base, by_three( base ) ) )

base = 9
printOutput( "'%d' cubed is '%s'" % ( base, cube( base ) ) )
printOutput( "'%d' by_three is '%s'" % ( base, by_three( base ) ) )

# 7 I Know Kung Fu
printExercise( "I Know Kung Fu" )

import math
base = 25
printOutput( "math.sqrt of '%d' is '%.2f'" % ( base, math.sqrt( base ) ) )

# 8 Generic Imports
printExercise( "Generic Imports" )

base = 25
printOutput( "math.sqrt of '%d' is '%.2f'" % ( base, math.sqrt( base ) ) )

# 9 Function Imports
printExercise( "Function Imports" )

from math import sqrt
base = 25
printOutput( "sqrt of '%d' is '%.2f'" % ( base, sqrt( base ) ) )

# 10 Universal Imports
printExercise( "Universal Imports" )

# from math import *
base = 25
printOutput( "sqrt of '%d' is '%.2f'" % ( base, sqrt( base ) ) )

# 11 Here Be Dragons
printExercise( "Here Be Dragons" )

everything = dir( math )

STEP = 0
for item in everything:
    functionCall( INCREMENT="" )
    printOutput( item, LABEL="Math Method: " )
    functionCleanup()

# 12 On Beyond Strings
printExercise( "On Beyond Strings" )

def biggest_number( *args ):
    return max( args )

def smallest_number( *args ):
    return min( args )

def distance_from_zero( arg ):
    return abs( arg )

num_1 = -10
num_2 = -5
num_3 = 5
num_4 = 10
printOutput( "Biggest number in: %d, %d, %d, %d is '%d'" % ( num_1, num_2, num_3, num_4, biggest_number( num_1, num_2, num_3, num_4 ) ) )
printOutput( "Smallest number in: %d, %d, %d, %d is '%d'" % ( num_1, num_2, num_3, num_4, smallest_number( num_1, num_2, num_3, num_4 ) ) )
printOutput( "Distance of '%d' from '0' is '%d'" % ( num_1, distance_from_zero( num_1 ) ) )
printOutput( "Distance of '%d' from '0' is '%d'" % ( num_2, distance_from_zero( num_2 ) ) )
printOutput( "Distance of '%d' from '0' is '%d'" % ( num_3, distance_from_zero( num_3 ) ) )
printOutput( "Distance of '%d' from '0' is '%d'" % ( num_4, distance_from_zero( num_4 ) ) )

# 13 max()
printExercise( "max()" )

num_1 = 5
num_2 = -2
num_3 = 100
num_4 = 8
printOutput( "Maximum in: %d, %d, %d, %d is '%d'" % ( num_1, num_2, num_3, num_4, max( num_1, num_2, num_3, num_4 ) ) )

# 14 min()
printExercise( "min()" )

num_1 = 5
num_2 = -2
num_3 = 100
num_4 = 8
printOutput( "Minimum in: %d, %d, %d, %d is '%d'" % ( num_1, num_2, num_3, num_4, min( num_1, num_2, num_3, num_4 ) ) )

# 15 abs()
printExercise( "abs()" )

num_1 = -12
printOutput( "Absolute value of '%d' is '%d'" % ( num_1, distance_from_zero( num_1 ) ) )

num_2 = 2
printOutput( "Absolute value of '%d' is '%d'" % ( num_2, distance_from_zero( num_2 ) ) )

# 16 type()
printExercise( "type()" )

value_1 = 42
value_2 = 4.2
value_3 = "42"

printOutput( "The type of '%s' is '%s'" % ( value_1, type( value_1 ) ) )
printOutput( "The type of '%s' is '%s'" % ( value_2, type( value_2 ) ) )
printOutput( "The type of '%s' is '%s'" % ( value_3, type( value_3 ) ) )

# 17 Review: Functions
printExercise( "Review: Functions" )

def shut_down( s ):
    if s.lower() == "yes" or s.lower() == "y":
        return "Shutting down"
    elif s.lower() == "no" or s.lower() == "n":
        return "Shutdown aborted"
    else:
        return "Sorry"

value_1 = "Yes"
value_2 = "y"
value_3 = "No"
value_4 = "n"
value_5 = "I am undecided"
printOutput( "Shut Down with input '%s'. '%s'" % ( value_1, shut_down( value_1 ) ) )
printOutput( "Shut Down with input '%s'. '%s'" % ( value_2, shut_down( value_2 ) ) )
printOutput( "Shut Down with input '%s'. '%s'" % ( value_3, shut_down( value_3 ) ) )
printOutput( "Shut Down with input '%s'. '%s'" % ( value_4, shut_down( value_4 ) ) )
printOutput( "Shut Down with input '%s'. '%s'" % ( value_5, shut_down( value_5 ) ) )

# 18 Review: Modules
printExercise( "Review: Modules" )

# from math import sqrt

base = 13689
printOutput( "sqrt of '%d' is '%.2f'" % ( base, sqrt( base ) ) )

# 19 Review: Built-In Functions
printExercise( "Review: Built-In Functions" )

def is_numeric( num ):
    return type( num ) == int or type( num ) == float

def distance_from_zero_1( value ):
    if is_numeric( value ):
        return abs( value )
    else:
        return "Nope"

num_1 = -1
num_2 = 8
num_3 = "Cat"
printOutput( "Absolute value of '%s' is '%s'" % ( num_1, distance_from_zero_1( num_1 ) ) )
printOutput( "Absolute value of '%s' is '%s'" % ( num_2, distance_from_zero_1( num_2 ) ) )
printOutput( "Absolute value of '%s' is '%s'" % ( num_3, distance_from_zero_1( num_3 ) ) )
