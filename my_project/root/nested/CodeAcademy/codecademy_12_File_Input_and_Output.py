'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "File Input and Output", 12 )

useFunction = False

# 1 See It to Believe It
printExercise( "See It to Believe It" )

my_list = [i ** 2 for i in range( 1, 11 )]
# Generates a list of squares of the numbers 1 - 10

my_file = open( "Files/12.1.txt", "w" )

for item in my_list:
    my_file.write( str( item ) + "\n" )

my_file.close()

# 2 The open() Function
printExercise( "The open() Function" )

my_file = open( "Files/12.1.txt", "r+" )
my_file.close()

# 3 Writing
printExercise( "Writing" )

my_list = [i ** 2 for i in range( 1, 11 )]

my_file = open( "Files/12.3.txt", "w" )

my_file.write( "Data to be written\n" )

for value in my_list:
    my_file.write( "Your value is '%d'\n" % ( value ) )

my_file.close()

# 4 Reading
printExercise( "Reading" )

my_file = open( "Files/12.3.txt", "r" )
# printOutput( my_file.read())
for line in my_file :
    printOutput( line.rstrip( '\n' ) )

my_file.close()

# 5 Reading Between the Lines
printExercise( "Reading Between the Lines" )

my_file = open( "Files/12.5.txt", "w" )
my_file.write( "I'm the first line of the file!\n" )
my_file.write( "I'm the second line.\n" )
my_file.write( "Third line here, boss.\n" )
my_file.close()

my_file = open( "Files/12.5.txt", "r" )
printOutput( my_file.readline().rstrip( '\n' ) )
printOutput( my_file.readline().rstrip( '\n' ) )
printOutput( my_file.readline().rstrip( '\n' ) )
my_file.close()

# 6 PSA: Buffering Data
printExercise( "PSA: Buffering Data" )

write_file = open( "Files/12.6.txt", "w" )
read_file = open( "Files/12.6.txt", "r" )

write_file.write( "Not closing files is VERY BAD." )
write_file.close()

printOutput( read_file.read() )
read_file.close()

# 7 The 'with' and 'as' Keywords
printExercise( "The 'with' and 'as' Keywords" )

with open( "Files/12.7.txt", "w" ) as textfile:
    textfile.write( "Success!" )

# 8 Try It Yourself
printExercise( "Try It Yourself" )

my_list = [i ** 3 for i in range( 1, 11 )]

with open( "Files/12.8.txt", "w" ) as my_file:
    for value in my_list:
        my_file.write( "Your value is '%d'\n" % ( value ) )

# 9 Case Closed?
printExercise( "Case Closed?" )

f = open( "Files/12.8.txt" )
printOutput( "Your file is closed '%s'" % ( f.closed ) )
f.close()
printOutput( "Your file is closed '%s'" % ( f.closed ) )

my_file = open( "Files/12.8.txt" )
if  ( not my_file.closed ):
    my_file.close()

printOutput( "Your file is closed '%s'" % ( my_file.closed ) )
