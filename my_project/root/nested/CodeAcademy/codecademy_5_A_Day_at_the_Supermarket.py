'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import functionCall
from Python_Library import functionCleanup

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Lists & Dictionaries", 5 )

printLesson( "A Day at the Supermarket", 2 )

# 1 BeFOR We Begin
printExercise( "BeFOR We Begin" )

names = ["Adam", "Alex", "Mariah", "Martine", "Columbus"]

for item in names:
    printOutput( item, LABEL="name: ", DESCRIPTIVE="" )

# 2 This is KEY!
printExercise( "This is KEY!" )

webster = {
    "Aardvark" : "A star of a popular children's cartoon show.",
    "Baa" : "The sound a goat makes.",
    "Carpet": "Goes on the floor.",
    "Dab": "A small amount."
}

for item in webster:
    printOutput( webster[item], LABEL="value: ", DESCRIPTIVE="" )

# 3 Control Flow and Looping
printExercise( "This is KEY!" )

a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

for item in a:
    if item % 2 == 0:
        printOutput( item, LABEL="even: ", DESCRIPTIVE="" )

# 4 Lists + Functions
printExercise( "Lists + Functions" )

def fizz_count( x ):
    count = 0
    for item in x:
        if item == "fizz":
            count += 1
    return count

printOutput( fizz_count( ["fizz", "cat", "fizz"] ), LABEL="fizz_count: ", DESCRIPTIVE="" )

# 5 String Looping
printExercise( "String Looping" )

VALUE = "Codecademy"
printOutput( VALUE, LABEL="letter: ", DESCRIPTIVE="" )

for letter in "Codecademy":
    printOutput( letter, LABEL="letter: ", DESCRIPTIVE="" )

word = "Programming is fun!"
printOutput( word, LABEL="letter: ", DESCRIPTIVE="" )

for letter in word:
    # Only print out the letter i
    if letter == "i":
        printOutput( letter, LABEL="letter: ", DESCRIPTIVE="" )

# 6 Your Own Store!
printExercise( "Your Own Store!" )

prices = {
    "banana": 4,
    "apple": 2,
    "orange": 1.5,
    "pear": 3
}

printOutput( prices, LABEL="prices: ", DESCRIPTIVE="" )

for item in prices:
    functionCall()
    printOutput( item, LABEL="item: ", DESCRIPTIVE="" )
    printOutput( prices[item], LABEL="price: ", DESCRIPTIVE="" )
    functionCleanup()

# 7 Investing in Stock
printExercise( "Investing in Stock" )

prices = {
    "banana": 4,
    "apple": 2,
    "orange": 1.5,
    "pear": 3
}

stock = {
    "banana": 6,
    "apple": 0,
    "orange": 32,
    "pear": 15
}

printOutput( prices, LABEL="prices: ", DESCRIPTIVE="" )

for item in prices:
    functionCall()
    printOutput( item, LABEL="item: ", DESCRIPTIVE="" )
    printOutput( "price: %s" % prices[item], LABEL="price: ", DESCRIPTIVE="" )
    functionCleanup()

printOutput( stock, LABEL="stock: ", DESCRIPTIVE="" )

for item in stock:
    functionCall()
    printOutput( item, LABEL="item: ", DESCRIPTIVE="" )
    printOutput( "stock: %s" % stock[item], LABEL="stock: ", DESCRIPTIVE="" )
    functionCleanup()

# 8 Keeping Track of the Produce
printExercise( "Keeping Track of the Produce" )

prices = {
    "banana": 4,
    "apple": 2,
    "orange": 1.5,
    "pear": 3
}

stock = {
    "banana": 6,
    "apple": 0,
    "orange": 32,
    "pear": 15
}

printOutput( prices, LABEL="prices: ", DESCRIPTIVE="" )

for item in prices:
    functionCall()
    printOutput( item, LABEL="item: ", DESCRIPTIVE="" )
    printOutput( "price: %s" % prices[item], LABEL="price: ", DESCRIPTIVE="" )
    printOutput( "stock: %s" % stock[item], LABEL="stock: ", DESCRIPTIVE="" )
    functionCleanup()

# 9 Something of Value
printExercise( "Something of Value" )

prices = {
    "banana" : 4,
    "apple"  : 2,
    "orange" : 1.5,
    "pear"   : 3,
}
stock = {
    "banana" : 6,
    "apple"  : 0,
    "orange" : 32,
    "pear"   : 15,
}
total = 0

for key in prices:
    total += prices[key] * stock[key]

printOutput( "stock: %s" % total, LABEL="total: ", DESCRIPTIVE="" )

# 10 Shopping at the Market
printExercise( "Shopping at the Market" )

groceries = ["banana", "orange", "apple"]

printOutput( groceries, LABEL="groceries: ", DESCRIPTIVE="" )

for item in groceries:
    printOutput( item, LABEL="item: ", DESCRIPTIVE="" )

# 11 Making a Purchase
printExercise( "Making a Purchase" )


prices = {
    "banana" : 4,
    "apple"  : 2,
    "orange" : 1.5,
    "pear"   : 3,
}
stock = {
    "banana" : 6,
    "apple"  : 0,
    "orange" : 32,
    "pear"   : 15,
}

def compute_bill( food ):
    total = 0
    for item in food:
        total += prices[item]
    return total

shopping_list = ["banana", "orange", "apple"]
printOutput( "%s" % compute_bill( shopping_list ), LABEL="shopping_list total: ", DESCRIPTIVE="" )

# 12 Stocking Out
printExercise( "Stocking Out" )

prices = {
    "banana" : 4,
    "apple"  : 2,
    "orange" : 1.5,
    "pear"   : 3,
}

stock = {
    "banana" : 6,
    "apple"  : 0,
    "orange" : 32,
    "pear"   : 15,
}

def compute_bill_1( food ):
    total = 0
    for item in food:
        if stock[item] >= 1:
            stock[item] -= 1
            total += prices[item]
    return total

printOutput( "Inventory", LABEL="Inventory: ", DESCRIPTIVE="" )

for item in prices:
    functionCall()
    printOutput( item, LABEL="item: ", DESCRIPTIVE="" )
    printOutput( "price: %s" % prices[item], LABEL="price: ", DESCRIPTIVE="" )
    printOutput( "stock: %s" % stock[item], LABEL="stock: ", DESCRIPTIVE="" )
    functionCleanup()

shopping_list = ["banana", "pear", "apple"]
printOutput( shopping_list, LABEL="shopping_list: ", DESCRIPTIVE="" )

printOutput( "%s" % compute_bill_1( shopping_list ), LABEL="shopping_list total: ", DESCRIPTIVE="" )

printOutput( "Inventory", LABEL="Inventory: ", DESCRIPTIVE="" )

for item in prices:
    functionCall()
    printOutput( item, LABEL="item: ", DESCRIPTIVE="" )
    printOutput( "price: %s" % prices[item], LABEL="price: ", DESCRIPTIVE="" )
    printOutput( "stock: %s" % stock[item], LABEL="stock: ", DESCRIPTIVE="" )
    functionCleanup()

# 13 Let's Check Out!
printExercise( "Let's Check Out!" )
