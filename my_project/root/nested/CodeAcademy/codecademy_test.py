'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import helpMe

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python


printChapter( "Chapter", 0 )

printLesson( "Lesson", 0 )

bool_one = False and False
bool_two = -( -( -( -2 ) ) ) == -2 and 4 >= 16 ** 0.5
bool_three = 19 % 4 != 300 / 10 / 10 and False
bool_four = -( 1 ** 2 ) < 2 ** 0 and 10 % 10 <= 20 - 10 * 2
bool_five = True and True

printOutput ( "'{0}' '{1}' '{2}' '{3}' '{4}'".format( bool_one, bool_two, bool_three, bool_four, bool_four, bool_five ), DESCRIPTIVE="" )
printOutput ( "'{0}' '{1}' '{2}' '{3}' '{4}'".format( bool_one, bool_two, bool_three, bool_four, bool_four, bool_five ) , DESCRIPTIVE="" )
printOutput ( "'{0}' '{0}' '{0}' '{0}' '{0}'".format( bool_one ) , DESCRIPTIVE="" )
helpMe()

meal = 44.50

printOutput( format( meal, '.2f' ), LABEL="meal: " , DESCRIPTIVE="" )
printOutput( "format testing '%.2f'" % ( meal ), DESCRIPTIVE="" )
printOutput( "format testing '%.2f'" % ( meal ), LABEL="meal: ", DESCRIPTIVE="" )
printOutput( "%.2f" % ( meal ), LABEL="meal: ", DESCRIPTIVE="" )
printOutput( format( meal, '.2f' ), LABEL="test value: ", DESCRIPTIVE="" )

tip = .15
printOutput( "{0:.0%}".format( .15 ), DESCRIPTIVE="" )
