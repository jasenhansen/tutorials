'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput
from Python_Library import functionCall
from Python_Library import functionCleanup
from Python_Library import is_String

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Conditionals and Control Flow", 3 )

printLesson( "Conditionals & Control Flow", 1 )

useFunction = False

# 1 The Big If
printExercise( "Go With the Flow" )

def clinic_1( answer ):
    printOutput( "You've just entered the clinic!", LABEL="Enter: " )
    functionCall()
    printOutput( "Do you take the door on the left or the right?", LABEL="Choice: " )
    printOutput( "I choose: " + answer, LABEL="Selected: " )
    if answer.lower() == "left" or answer.lower() == "l":
        printOutput( "This is the Verbal Abuse Room, you heap of parrot droppings!", LABEL="Result: " )
    elif answer.lower() == "right" or answer.lower() == "r":
        printOutput( "Of course this is the Argument Room, I've told you that already!", LABEL="Result: " )
    else:
        printOutput( "You didn't pick left or right! Get out!", LABEL="Result: " )
    functionCleanup()

def clinic_2():
    printOutput( "Enter Clinic" )
    functionCall()
    printOutput( "You've just entered the clinic!", LABEL="Enter: " )
    printOutput( "Do you take the door on the left or the right?", LABEL="Choice: " )
    answer = input( "\t\tType left or right and hit 'Enter':  " ).lower()
    printOutput( "I choose: " + answer, LABEL="Selected: " )
    if answer.lower() == "left" or answer.lower() == "l":
        printOutput( "This is the Verbal Abuse Room, you heap of parrot droppings!", LABEL="Result: " )
    elif answer.lower() == "right" or answer.lower() == "r":
        printOutput( "Of course this is the Argument Room, I've told you that already!", LABEL="Result: " )
    else:
        printOutput( "You didn't pick left or right! Pick again!", LABEL="Result: " )
        clinic_2()
    functionCleanup()
clinic_1( "left" )
clinic_1( "right" )
clinic_1( "cat" )

if useFunction :
    clinic_2()
    clinic_2()
    clinic_2()

# 2 Compare Closely!
printExercise( "Compare Closely!" )

bool_one = 17 < 328
bool_two = 100 == ( 2 * 50 )
bool_three = 19 <= 19
bool_four = -22 >= -18
bool_five = 99 != ( 98 + 1 )

printOutput( "'17 < 328' is '%s'" % ( bool_one ) )
printOutput( "'100 == (2 * 50)' is '%s'" % ( bool_two ) )
printOutput( "'19 <= 19' is '%s'" % ( bool_three ) )
printOutput( "'-22 >= -18' is '%s'" % ( bool_four ) )
printOutput( "'99 != (98 + 1)' is '%s'" % ( bool_five ) )

# 3 Compare... Closelier!
printExercise( "Compare... Closelier!" )

bool_one = ( 20 - 10 ) > 15
bool_two = ( 10 + 17 ) == 3 ** 16
bool_three = 1 ** 2 <= -1
bool_four = 40 * 4 >= -4
bool_five = 100 != 10 ** 2

printOutput( "'(20 - 10) > 15' is '%s'" % ( bool_one ) )
printOutput( "'(10 + 17) == 3 ** 16' is '%s'" % ( bool_two ) )
printOutput( "'1 ** 2 <= -1' is '%s'" % ( bool_three ) )
printOutput( "'40 * 4 >= -4' is '%s'" % ( bool_four ) )
printOutput( "'100 != 10 ** 2' is '%s'" % ( bool_five ) )

# 4 How the Tables Have Turned
printExercise( "How the Tables Have Turned" )

bool_one = 3 < 5
bool_two = 3 == 5
bool_three = 3 != 5
bool_four = 3 > 5
bool_five = 3 <= 5

printOutput( "'3 < 5' is '%s'" % ( bool_one ) )
printOutput( "'3 == 5' is '%s'" % ( bool_two ) )
printOutput( "'3 != 5' is '%s'" % ( bool_three ) )
printOutput( "'3 > 5' is '%s'" % ( bool_four ) )
printOutput( "'3 <= 5' is '%s'" % ( bool_five ) )

# 5 To Be and/or Not to Be
printExercise( "To Be and/or Not to Be" )

# 6 And
printExercise( "And" )

bool_one = False and False
bool_two = -( -( -( -2 ) ) ) == -2 and 4 >= 16 ** 0.5
bool_three = 19 % 4 != 300 / 10 / 10 and False
bool_four = -( 1 ** 2 ) < 2 ** 0 and 10 % 10 <= 20 - 10 * 2
bool_five = True and True

printOutput( "'False and False' is '%s'" % ( bool_one ) )
printOutput( "'-(-(-(-2))) == -2 and 4 >= 16 ** 0.5' is '%s'" % ( bool_two ) )
printOutput( "'19 %% 4 != 300 / 10 / 10 and False' is '%s'" % ( bool_three ) )
printOutput( "'-(1 ** 2) < 2 ** 0 and 10 %% 10 <= 20 - 10 * 2' is '%s'" % ( bool_four ) )
printOutput( "'True and True' is '%s'" % ( bool_five ) )

# 7 Or
printExercise( "Or" )

bool_one = 2 ** 3 == 108 % 100 or 'Cleese' == 'King Arthur'
bool_two = True or False
bool_three = 100 ** 0.5 >= 50 or False
bool_four = True or True
bool_five = 1 ** 100 == 100 ** 1 or 3 * 2 * 1 != 3 + 2 + 1

printOutput( "'2 ** 3 == 108 %% 100 or 'Cleese' == 'King Arthur'' is '%s'" % ( bool_one ) )
printOutput( "'True or False' is '%s'" % ( bool_two ) )
printOutput( "'100 ** 0.5 >= 50 or False' is '%s'" % ( bool_three ) )
printOutput( "'True or True' is '%s'" % ( bool_four ) )
printOutput( "'1 ** 100 == 100 ** 1 or 3 * 2 * 1 != 3 + 2 + 1' is '%s'" % ( bool_five ) )

# 8 Not
printExercise( "Not" )

bool_one = not True
bool_two = not 3 ** 4 < 4 ** 3
bool_three = not 10 % 3 <= 10 % 2
bool_four = not 3 ** 2 + 4 ** 2 != 5 ** 2
bool_five = not not False

printOutput( "'not True' is '%s'" % ( bool_one ) )
printOutput( "'not 3 ** 4 < 4 ** 3' is '%s'" % ( bool_two ) )
printOutput( "'not 10 %% 3 <= 10 %% 2' is '%s'" % ( bool_three ) )
printOutput( "'not 3 ** 2 + 4 ** 2 != 5 ** 2' is '%s'" % ( bool_four ) )
printOutput( "'not not False' is '%s'" % ( bool_five ) )

# 9 This and That (or This, But Not That!)
printExercise( "This and That (or This, But Not That!)" )

bool_one = False or not True and True
bool_two = False and not True or True
bool_three = True and not ( False or False )
bool_four = not not True or False and not True
bool_five = False or not ( True and True )

printOutput( "'False or not True and True' is '%s'" % ( bool_one ) )
printOutput( "'False and not True or True' is '%s'" % ( bool_two ) )
printOutput( "'True and not (False or False)' is '%s'" % ( bool_three ) )
printOutput( "'not not True or False and not True' is '%s'" % ( bool_four ) )
printOutput( "'False or not (True and True)' is '%s'" % ( bool_five ) )

# 10 Mix 'n' Match
printExercise( "Mix 'n' Match" )

bool_one = ( 2 <= 2 ) and "Alpha" == "Bravo"
bool_two = ( 2 <= 2 ) or "Alpha" == "Bravo"
bool_three = not ( 2 <= 2 ) or "Alpha" == "Bravo"
bool_four = ( 2 <= 2 ) and not ( "Alpha" == "Bravo" )
bool_five = not not ( 2 <= 2 ) and not ( "Alpha" == "Bravo" )

printOutput( "'(2 <= 2) and \"Alpha\" == \"Bravo\"' is '%s'" % ( bool_one ) )
printOutput( "'(2 <= 2) or \"Alpha\" == \"Bravo\"' is '%s'" % ( bool_two ) )
printOutput( "'not (2 <= 2) or \"Alpha\" == \"Bravo\"' is '%s'" % ( bool_three ) )
printOutput( "'(2 <= 2) and not (\"Alpha\" == \"Bravo\")' is '%s'" % ( bool_four ) )
printOutput( "'not not (2 <= 2) and not (\"Alpha\" == \"Bravo\")' is '%s'" % ( bool_five ) )

# 11 Conditional Statement Syntax
printExercise( "Conditional Statement Syntax" )

def print_Test_1( answer ):
    functionCall( INCREMENT="" )
    TEST = "Cat"
    printOutput( "Testing '%s' against '%s'" % ( answer, TEST ), LABEL="" )
    if answer == TEST:
        printOutput( "'%s' is the same as '%s'" % ( answer, TEST ), LABEL="" )
    else:
        printOutput( "'%s' is not the same as '%s'" % ( answer, TEST ), LABEL="" )
    functionCleanup()

def print_Test_2():
    functionCall( INCREMENT="" )
    TEST = "Cat"
    answer = input( "\t\tType a value to test and hit 'Enter':  " )
    printOutput( "Testing '%s' against '%s'" % ( answer, TEST ), LABEL="" )
    if answer == TEST:
        printOutput( "'%s' is the same as '%s'" % ( answer, TEST ), LABEL="" )
    else:
        printOutput( "'%s' is not the same as '%s'" % ( answer, TEST ), LABEL="" )
    functionCleanup()

print_Test_1( "left" )
print_Test_1( "right" )
print_Test_1( "Cat" )

if useFunction :
    print_Test_2()
    print_Test_2()

# 12 If You're Having...
printExercise( "If You're Having..." )

def using_control_once():
    if True:
        return "Success #1"

def using_control_again():
    if 1 >= 0:
        return "Success #2"

printOutput( "Calling 'using_control_once()' with no input causes output '%s'" % ( using_control_once() ) )
printOutput( "Calling 'using_control_again()' with no input causes output '%s'" % ( using_control_again() ) )

# 13 Else Problems, I Feel Bad for You, Son...
printExercise( "Else Problems, I Feel Bad for You, Son..." )

answer = "'Tis but a scratch!"

def black_knight_1():
    if answer == "'Tis but a scratch!":
        return True
    else:
        return False  # Make sure this returns False

def black_knight_2( value ):
    if value == "'Tis but a scratch!":
        return True
    else:
        return False

def french_soldier_1():
    if answer == "Go away, or I shall taunt you a second time!":
        return True
    else:
        return False  # Make sure this returns False

def french_soldier_2( value ):
    if value == "Go away, or I shall taunt you a second time!":
        return True
    else:
        return False

value_1 = "'Tis but a scratch!"
value_2 = "That Hurt!"
value_3 = "Go away, or I shall taunt you a second time!"
value_4 = "I laugh at you!"
printOutput( "Calling 'black_knight_1()' with no input causes output '%s'" % ( black_knight_1() ) )
printOutput( "Calling 'french_soldier_1()' with no input causes output '%s'" % ( french_soldier_1() ) )
printOutput( "Calling 'black_knight_2()' with input '%s' causes output '%s'" % ( value_1, black_knight_2( value_1 ) ) )
printOutput( "Calling 'black_knight_2()' with input '%s' causes output '%s'" % ( value_2, black_knight_2( value_2 ) ) )
printOutput( "Calling 'french_soldier_2()' with input '%s' causes output '%s'" % ( value_3, french_soldier_2( value_3 ) ) )
printOutput( "Calling 'french_soldier_2()' with input '%s' causes output '%s'" % ( value_4, french_soldier_2( value_4 ) ) )

# 14 I Got 99 Problems, But a Switch Ain't One
printExercise( "I Got 99 Problems, But a Switch Ain't One" )

def greater_less_equal_5( answer ):
    if answer > 5:
        return 1
    elif answer < 5:
        return -1
    else:
        return 0

value_1 = 4
value_2 = 5
value_3 = 6
printOutput( "Calling 'greater_less_equal_5()' with input '%s' causes output '%s'" % ( value_1, greater_less_equal_5( value_1 ) ) )
printOutput( "Calling 'greater_less_equal_5()' with input '%s' causes output '%s'" % ( value_2, greater_less_equal_5( value_2 ) ) )
printOutput( "Calling 'greater_less_equal_5()' with input '%s' causes output '%s'" % ( value_3, greater_less_equal_5( value_3 ) ) )

# 15 The Big If
printExercise( "The Big If" )

def the_flying_circus_1():
    if True == False:
        return False
    elif True or False:
        return True
    else:
        return False

def the_flying_circus_2( answer ):
    if is_String( answer ):
        if answer.lower() == "pass":
            return True
    return False

value_1 = "Fail"
value_2 = "Pass"
value_3 = 6
printOutput( "Calling 'the_flying_circus_1()' with no input causes output '%s'" % ( the_flying_circus_1() ) )
printOutput( "Calling 'the_flying_circus_2()' with input '%s' causes output '%s'" % ( value_1, the_flying_circus_2( value_1 ) ) )
printOutput( "Calling 'the_flying_circus_2()' with input '%s' causes output '%s'" % ( value_2, the_flying_circus_2( value_2 ) ) )
printOutput( "Calling 'the_flying_circus_2()' with input '%s' causes output '%s'" % ( value_3, the_flying_circus_2( value_3 ) ) )
