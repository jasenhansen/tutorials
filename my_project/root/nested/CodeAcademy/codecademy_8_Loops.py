'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Loops", 8 )

printLesson( "Loops", 1 )

useFunction = False

# 1 While you're here
printExercise( "While you're here" )

count = 0

if count < 10:
    printOutput( "Hello, I am an if statement and count is %d" % count )

while count < 10:
    printOutput( "Hello, I am a while and count is %d" % count )
    count += 1

# 2 Condition
printExercise( "Condition" )

loop_condition = True

while loop_condition:
    printOutput( "I am a loop" )
    loop_condition = False

# 3 While you're at it
printExercise( "While you're at it" )

count = 1

while count <= 10:
    printOutput( '%d squared is %d' % ( count, count ** 2 ) )
    count += 1

# 4 Simple errors
printExercise( "Simple errors" )

def enjoying():
    choiceEnjoying = input( '\t\tEnjoying the course? (y/n): ' )
    printOutput( "Initial choice %s" % choiceEnjoying )
    # Fill in the condition (before the colon)y
    while ( ( choiceEnjoying.upper() != "Y" ) and ( choiceEnjoying.upper() != "N" ) and ( choiceEnjoying.upper() != "YES" ) and ( choiceEnjoying.upper() != "NO" ) ):
        choiceEnjoying = input( "\t\tSorry, I didn't catch that. Enter again: " )
        printOutput( "Loop choice %s" % choiceEnjoying )

    printOutput( "Your successful choice was %s" % choiceEnjoying )

if ( useFunction ) :
    enjoying()
else :
    printOutput( "Skipping output so no user input is not needed." )

# 5 Infinite loops
printExercise( "Infinite loops" )

count = 0

while count < 10:
    printOutput( "Your count is %s" % count )
    count += 1

# 6 Break
printExercise( "Break" )

count = 0

while True:
    printOutput( "Your count is %s" % count )
    count += 1
    if count >= 10:
        break

# 7 While / else
printExercise( "While / else" )

import random

printOutput( "Lucky Numbers! 3 numbers will be generated." )
printOutput( "If one of them is a '5', you lose!" )

count = 0
while count < 3:
    num = random.randint( 1, 6 )
    printOutput( "Your number is %s" % num )
    if num == 5:
        printOutput( "Sorry, you lose!" )
        break
    count += 1
else:
    printOutput( "You win!" )

# 8 Your own while / else
printExercise( "Your own while / else" )

from random import randint

def guessing():
    # Generates a number from 1 through 10 inclusive
    random_number = randint( 1, 10 )

    guesses_left = 3
    # Start your game!
    while guesses_left > 0:
        guess = int( input( "\t\tYour guess: " ) )
        if random_number == guess:
            printOutput( "You win!" )
            break
        guesses_left -= 1
    else:
        printOutput( "Sorry, you lose!" )

if ( useFunction ) :
    guessing()
else :
    printOutput( "Skipping output so no user input is not needed." )

# 9 For your health
printExercise( "For your health" )

printOutput( "Counting..." )

for i in range( 20 ):
    printOutput( "Your count is %s" % i )

# 10 For your hobbies
printExercise( "For your hobbies" )

def hobbies():
    hobbiesList = []

    for i in range( 3 ):
        hobby = str( input( "\t\tTell me one of your favorite hobbies:  " ) )
        printOutput( "Your hobby is %s" % hobby )
        hobbiesList.append( hobby )

    printOutput( hobbiesList )

if ( useFunction ) :
    hobbies()
else :
    printOutput( "Skipping output so no user input is not needed." )

# 11 For your strings
printExercise( "For your strings" )

thing = "spam!"
printOutput( "Your word is %s" % thing )

for char in thing:
    printOutput( "Your character is %s" % char )

word = "eggs!"
printOutput( "Your word is %s" % word )

for char in word:
    printOutput( "Your character is %s" % char )

# 12 For your A
printExercise( "For your A" )

phrase = "A bird in the hand..."
printOutput( "Your phrase is %s" % phrase )

newString = ""
for char in phrase:
    if char.upper() == "A":
        # print ( "X", )
        newString += "X"
    else :
        # print ( char, )
        newString += char
printOutput( "Your new string is %s" % newString )

# 13 For your lists
printExercise( "For your lists" )

numbers = [7, 9, 12, 54, 99]

print ( "This list contains: " )
printOutput( numbers )

for num in numbers:
    printOutput( "Your number is %d" % num )

for num in numbers:
    printOutput( "Your number squared is  is %d" % num ** 2 )

# 14 Looping over a dictionary
printExercise( "Looping over a dictionary" )

fruitDictionary = {'a': 'apple', 'b': 'berry', 'c': 'cherry'}

printOutput( "Standard Dictionary" )
printOutput( fruitDictionary )
for key in fruitDictionary:
    printOutput( 'Key: %s Value: %s' % ( key, fruitDictionary[key] ) )

import collections

printOutput( "Ordered Dictionary" )

fruitTupple = [( "a", "apple" ),
        ( "b", "berry" ),
        ( "c", "cherry" )]

newFruitDictionary = collections.OrderedDict( fruitTupple )
printOutput( newFruitDictionary )

for key in newFruitDictionary:
    printOutput( 'Key: %s Value: %s' % ( key, newFruitDictionary[key] ) )

# 15 Counting as you go
printExercise( "Counting as you go" )

choices = ['pizza', 'pasta', 'salad', 'nachos']

printOutput( "Your choices are:" )
printOutput( choices )
for index, item in enumerate( choices ):
    printOutput( 'Key: %s Value: %s' % ( index + 1, item ) )

# 16 Multiple lists
printExercise( "Multiple lists" )

list_a = [3, 9, 17, 15, 19]
list_b = [2, 4, 8, 10, 30, 40, 50, 60, 70, 80, 90]

for a, b in zip( list_a, list_b ):
    if a >= b :
        printOutput( "The element in list_a '%d' is greater or equal the element in list_b. '%d'" % ( a, b ) )
    else :
        printOutput( "The element in list_a '%d' is less than the element in list_b. '%d'" % ( a, b ) )

# 17 For / else
printExercise( "For / else" )

def fruitTest( FRUIT_LIST ):
    printOutput( "Your fruits are:" )
    printOutput( FRUIT_LIST )

    printOutput( "You have..." )
    for f in FRUIT_LIST:
        if f == 'tomato':
            printOutput( "A tomato is not a fruit!" )  # (It actually is.)
            break
        printOutput( "A %s" % f )

    else:
        printOutput( "A fine selection of fruits!" )

fruits = ['banana', 'apple', 'orange', 'tomato', 'pear', 'grape']
fruitTest( fruits )

# 18 Change it up
printExercise( "Change it up" )

fruits2 = ['banana', 'apple', 'orange', 'pear', 'grape']
fruitTest( fruits2 )

# 19 Create your own
printExercise( "Create your own" )

def divdeTest( COUNT, DIVIDE_BY ):
    printOutput( "Test to see if %d numbers are divisible by '%d'" % ( COUNT, DIVIDE_BY ) )
    for i in range( COUNT ):
        number = int( input( "\t\tEnter a number:  " ) )
        if ( ( number % 3 ) == 0 ):
            printOutput( "Your number '%d' is divisible by '%d'" % ( number, DIVIDE_BY ) )
            break
        else :
            printOutput( "Your number '%d' is not divisible by '%d'" % ( number, DIVIDE_BY ) )
    else:
        printOutput( "None of your numbers are divisible by '%d'" % ( DIVIDE_BY ) )

if ( useFunction ) :
    divdeTest( 3, 3 )
else :
    printOutput( "Skipping output so no user input is not needed." )
