'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Introduction to Classes", 11 )

printLesson( "Classes", 2 )

# 1 Class basics
printExercise( "Class basics" )


class Car( object ):
    pass

# 2 Create an instance of a class
printExercise( "Create an instance of a class" )

my_car = Car()

# 3 Class member variables
printExercise( "Class member variables" )

class Car_1( Car ):
    condition = "New"

# 4 Calling class member variables
printExercise( "Calling class member variables" )

my_car = Car_1()
printOutput( "The condition of this car is '%s'" % ( my_car.condition ) )

# 5 Initializing a class
printExercise( "Initializing a class" )

class Car_2( Car_1 ):
    def __init__( self, model, color, mpg ):
        self.model = model
        self.color = color
        self.mpg = mpg

my_car = Car_2( "DeLorean", "Silver", 88 )

# 6 Referring to member variables
printExercise( "Referring to member variables" )

printOutput( "The condition of this car is '%s'" % ( my_car.condition ) )
printOutput( "The model of this car is '%s'" % ( my_car.model ) )
printOutput( "The color of this car is '%s'" % ( my_car.color ) )
printOutput( "The mpg of this car is '%d'" % ( my_car.mpg ) )

# 7 Creating class methods
printExercise( "Creating class methods" )

class Square( object ):
    def __init__( self, side ):
        self.side = side

    def perimeter( self ):
        return self.side * 4

square = Square( 4 )
printOutput( "My Square has a perimeter of '%d'" % ( square.perimeter() ) )

class Car_3( Car_2 ):
    def display_car( self ):
        printOutput( "This is a %s %s with %d MPG." % ( self.color, self.model, self.mpg ) )

my_car = Car_3( "DeLorean", "Silver", 88 )
my_car.display_car()

# 8 Modifying member variables
printExercise( "Modifying member variables" )

class Car_4( Car_3 ):
    def drive_car( self ):
        self.condition = "Used"

my_car = Car_4( "DeLorean", "Silver", 88 )
printOutput( "The condition of this car is '%s'" % ( my_car.condition ) )
my_car.drive_car()
printOutput( "The condition of this car is '%s'" % ( my_car.condition ) )

# 9 Inheritance
printExercise( "Inheritance" )

class ElectricCar_1 ( Car_4 ):
    def __init__( self, model, color, mpg, battery_type ):
        self.model = model
        self.color = color
        self.mpg = mpg
        self.battery_type = battery_type
    def display_car( self ):
        printOutput( "This is a %s %s with %d MPG and a %s battery type." % ( self.color, self.model, self.mpg, self.battery_type ) )

my_car = ElectricCar_1( "DeLorean", "Silver", 88, "Molten Salt" )
my_car.display_car()

# 10 Overriding methods
printExercise( "Overriding methods" )

class ElectricCar_2 ( ElectricCar_1 ):
    def drive_car( self ):
        self.condition = "Like New"

my_car = ElectricCar_2( "DeLorean", "Silver", 88, "Molten Salt" )
printOutput( "The condition of this car is '%s'" % ( my_car.condition ) )
my_car.drive_car()
printOutput( "The condition of this car is '%s'" % ( my_car.condition ) )

# 11 Building useful classes
printExercise( "Building useful classes" )

class Point3D ( object ):
    def __init__( self, x, y, z ):
        self.x = x
        self.y = y
        self.z = z
    def __repr__( self ):
        return "(%d, %d, %d)" % ( self.x, self.y, self.z )

my_point = Point3D( 90, 60, 30 )
printOutput( my_point )
