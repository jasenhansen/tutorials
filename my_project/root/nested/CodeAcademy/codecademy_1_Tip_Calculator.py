'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Python Syntax", 1 )

printLesson( "Tip Calculator", 2 )

# 1 The Meal
printOutput( "The Meal" )

meal = 44.50
printExercise( "The meal costs $%.2f" % ( meal ) )

# 2 The Tax
printExercise( "The Tax" )

meal = 44.50
tax = .0675
tax_total = meal * tax

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )

# 3 The Tip
printExercise( "The Tip" )

meal = 44.50
tax = .0675
tax_total = meal * tax
tip = .15

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )
printOutput( "The tip is {0:.0%}".format( tip ) )

# 4 Reassign in a Single Line
printExercise( "Reassign in a Single Line" )

meal = 44.50
tax = .0675
tax_total = meal * tax
sub_total = meal + tax_total
tip = .15
tip_total = sub_total * tip

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )
printOutput( "The sub_total is $%.2f" % ( sub_total ) )
printOutput( "The tip is {0:.0%}".format( tip ) )
printOutput( "The tip costs $%.2f" % ( tip_total ) )

# 5 The Total
printExercise( "The Total" )

meal = 44.50
tax = .0675
tax_total = meal * tax
sub_total = meal + tax_total
tip = .15
tip_total = sub_total * tip
total = sub_total + tip_total

printOutput( "The meal costs $%.2f" % ( meal ) )
printOutput( "The tax is {0:.0%}".format( tax ) )
printOutput( "The tax costs $%.2f" % ( tax_total ) )
printOutput( "The sub total is $%.2f" % ( sub_total ) )
printOutput( "The tip is {0:.0%}".format( tip ) )
printOutput( "The tip costs $%.2f" % ( tip_total ) )
printOutput( "The total is $%.2f" % ( total ) )
