'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/String_Formatting

printChapter( "String Formatting", 5 )

printLesson( "String Formatting" )
printOutput( "Python uses C-style string formatting to create new, formatted strings." )

printExercise( "Let's say you have a variable called \"name\" with your user name in it, and you would then like to print(out a greeting to that user.)" )

# This prints out "Hello, John!"
name = "John"
printOutput( "Hello, %s!" % name, DESCRIPTIVE="" )

printExercise( "To use two or more argument specifiers, use a tuple (parentheses)" )

# This prints out "John is 23 years old."
name = "John"
age = 23
printOutput( "%s is %d years old." % ( name, age ), DESCRIPTIVE="" )

printExercise( "Any object which is not a string can be formatted using the %s operator as well." )

# This prints out: A list: [1, 2, 3]
mylist = [1, 2, 3]
printOutput( "A list: %s" % mylist, DESCRIPTIVE="" )

printLesson( "Exercise" )

printExercise( "You will need to write a format string which prints out the data using the following syntax: Hello John Doe. Your current balance is $53.44." )

data = ( "John", "Doe", 53.44 )
format_string = "Hello %s %s. Your current balance is $%s."

printOutput( format_string % ( data[0], data[1], data[2] ), DESCRIPTIVE="" )
