'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys

sys.path.insert( 0, '../Framework' )
sys.path.insert( 0, 'subModules' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Modules_and_Packages

printChapter( "Modules and Packages", 1 )

printOutput( "In programming, a module is a piece of software that has a specific functionality. For example, when building a ping pong game, one module would be responsible for the game logic, and another module would be responsible for drawing the game on the screen. Each module is a different file, which can be edited separately." )

printLesson( "Writing modules" )

printExercise( "Modules in Python are simply Python files with a .py extension. The name of the module will be the name of the file. A Python module can have a set of functions, classes or variables defined and implemented. In the example above, we will have two files, we will have" )

printOutput( "mygame/", DESCRIPTIVE="" )
printOutput( "mygame/game.py", DESCRIPTIVE="" )
printOutput( "mygame/draw.py", DESCRIPTIVE="" )
printOutput( "mygame/draw.py", DESCRIPTIVE="" )

printOutput( "The Python script 'game.py will' implement the game. It will use the function draw_game from the file 'draw.py', or in other words, the draw module, that implements the logic for drawing the game on the screen." )

printExercise( "Modules are imported from other modules using the import command. In this example, the game.py script may look something like this" )

import game_1
game_1.play_game()

printExercise( "The draw module may look something like this" )

import draw_1
draw_1.draw_game( "" )
draw_1.clear_screen( "" )

printLesson( "Importing module objects to the current namespace" )

printExercise( "We may also import the function draw_game directly into the main script's namespace, by using the from command." )

import game_1
game_1.main()

printExercise( "We may also import the function draw_game directly into the main script's namespace, by using the from command." )

import game_2
game_2.main()

printLesson( "Importing all objects from a module" )

printExercise( "We may also use the import * command to import all objects from a specific module, like this:" )

import game_3
game_3.main()

printLesson( "Custom import name" )

printOutput( "We may also load modules under any name we want. This is useful when we want to import a module conditionally to use the same name in the rest of the code." )

printOutput( "For example, if you have two draw modules with slighty different names - you may do the following:" )

printExercise( "Sample with default." )

import game_4
game = game_4.game_4()
game.main()

printExercise( "Sample with visual_mode = False." )

visual_mode = False
import game_4
game = game_4.game_4()
game.setImports( visual_mode )
game.main()

printExercise( "Sample with visual_mode = True." )

visual_mode = True
import game_4
game = game_4.game_4()
game.setImports( visual_mode )
game.main()

printLesson( "Module initialization" )

printExercise( "The first time a module is loaded into a running Python script, it is initialized by executing the code in the module once. If another module in your code imports the same module again, it will not be loaded twice but once only - so local variables inside the module act as a 'singleton' - they are initialized only once." )

import draw_2
draw_2.Screen()

printLesson( "Extending module load path" )

printOutput( "There are a couple of ways we could tell the Python interpreter where to look for modules, aside from the default, which is the local directory and the built-in modules. You could either use the environment variable PYTHONPATH to specify additional directories to look for modules in, like this" )

printExercise( "environment variable PYTHONPATH" )

printOutput( "Sample (Not working yet): PYTHONPATH=/subModules python game.py" )

printExercise( "sys.path.append(\"\")" )

printOutput( "Adds to the end of the path" )

printExercise( "sys.path.insert(\"\")" )

printOutput( "Adds to the beginning of the path so I think that this is faster if you want your modules to load first" )

printLesson( "Exploring built-in modules" )

printOutput( "Two very important functions come in handy when exploring modules in Python - the dir and help functions." )

printExercise( "If we want to import the module urllib, which enables us to create read data from URLs, we simply import the module:" )

printOutput( "These are not outputting as expected ..." )
# import the library
import urllib

# use it
# urllib.urlopen( "" )

printExercise( "dir( urllib )" )

import urllib
print ( dir( urllib ) )

printOutput( "dir( \"Python_Library\" )" )
print ( dir( "Python_Library" ) )

printOutput( "dir( \"subModules\" )" )
print ( dir( "subModules" ) )

printOutput( "help(urllib)" )

help( urllib )

printExercise( "When we find the function in the module we want to use, we can read about it more using the help function, inside the Python interpreter:" )
printOutput( "help(urllib.parse)" )

help( urllib.parse )

printLesson( "Writing packages" )

printOutput( "Packages are namespaces which contain multiple packages and modules themselves. They are simply directories, but with a twist." )
printOutput( "Each package in Python is a directory which MUST contain a special file called __init__.py. This file can be empty, and it indicates that the directory it contains is a Python package, so it can be imported the same way a module can be imported." )
printOutput( "If we create a directory called foo, which marks the package name, we can then create a module inside that package called bar. We also must not forget to add the __init__.py file inside the foo directory." )
printOutput( "To use the module bar, we can import it in two ways:" )
printOutput( "To use the module bar, we can import it in two ways:" )

printExercise( "import foo.bar" )

printExercise( "from foo import bar" )

printExercise( "The __init__.py file can also decide which modules the package exports as the API, while keeping other modules internal, by overriding the __all__ variable, like so:" )

printOutput( "Not sure how this is supposed to work..." )

printLesson( "Exercise" )

printExercise( "In this exercise, you will need to print an alphabetically sorted list of all functions in the re module, which contain the word find." )

import re

# Your code goes here
printOutput( "dir ( \"re\" )" )
printOutput ( dir ( "re" ), DESCRIPTIVE="" )

printOutput( "for member in dir( re )" )
for member in dir( re ):
    printOutput ( member, DESCRIPTIVE="" )

# find_members
printOutput( "if \"find\" in member:" )
for member in dir( re ):
    if "find" in member:
        printOutput ( member, DESCRIPTIVE="" )

printOutput( "Populate find_members" )
find_members = []
for member in dir( re ):
    if "find" in member:
        find_members.append( member )

printOutput( find_members, DESCRIPTIVE="" )

printOutput( "sorted find_members" )
printOutput( sorted( find_members ), DESCRIPTIVE="" )

