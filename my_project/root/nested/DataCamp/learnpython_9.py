'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Functions

printChapter( "Functions", 9 )

printLesson( "What are Functions?" )

printOutput( "Functions are a convenient way to divide your code into useful blocks, allowing us to order our code, make it more readable, reuse it and save some time. Also functions are a key way to define interfaces so programmers can share their code." )

printLesson( "How do you write functions in Python?" )

printExercise( "As we have seen on previous tutorials, Python makes use of blocks." )

printOutput( "block_head:", DESCRIPTIVE="" )
printOutput( "    1st block line", DESCRIPTIVE="" )
printOutput( "    2nd block line", DESCRIPTIVE="" )

printExercise( "Functions in python are defined using the block keyword \"def\", followed with the function's name as the block's name" )

def my_function():
    printOutput( "Hello From My Function!", DESCRIPTIVE="" )

my_function()

printExercise( "Functions may also receive arguments (variables passed from the caller to the function)" )

def my_function_with_args( username, greeting ):
    printOutput( "Hello, %s, From My Function!, I wish you %s" % ( username, greeting ), DESCRIPTIVE="" )

username = "Jasen Hansen"
greeting = "prosperitity"

my_function_with_args( username, greeting )

printExercise( "Functions may return a value to the caller, using the keyword- 'return'." )

def sum_two_numbers( a, b ):
    return a + b

a = 2
b = 7
c = sum_two_numbers( a, b )

printOutput( a, DESCRIPTIVE="" )
printOutput( b, DESCRIPTIVE="" )
printOutput( c, DESCRIPTIVE="" )

printLesson( "How do you call functions in Python?" )

printExercise( "Simply write the function's name followed by (), placing any required arguments within the brackets. For example, lets call the functions written above (in the previous example)" )

# Define our 3 functions
def my_function_1():
    printOutput( "Hello From My Function!", DESCRIPTIVE="" )

def my_function_with_args_1( username, greeting ):
    printOutput( "Hello, %s , From My Function!, I wish you %s" % ( username, greeting ) )

def sum_two_numbers_1( a, b ):
    return a + b

# print(a simple greeting)
my_function_1()

# prints - "Hello, John Doe, From My Function!, I wish you a great year!"
my_function_with_args_1( "John Doe", "a great year!" )

a = 1
b = 2

# after this line x will hold the value 3!
x = sum_two_numbers_1( 1, 2 )

printOutput( a, DESCRIPTIVE="" )
printOutput( b, DESCRIPTIVE="" )
printOutput( x, DESCRIPTIVE="" )

printLesson( "Exercise" )

printOutput( "In this exercise you'll use an existing function, and while adding your own to create a fully functional program." )
printOutput( "1. Add a function named list_benefits() that returns the following list of strings: 'More organized code', 'More readable code', 'Easier code reuse', 'Allowing programmers to share and connect code together'" )
printOutput( "2. Add a function named build_sentence(info) which receives a single argument containing a string and returns a sentence starting with the given string and ending with the string 'is a benefit of functions!'" )
printOutput( "3. Run and see all the functions work together!" )

printExercise( "Execute logic." )

# Modify this function to return a list of strings as defined above
def list_benefits():
    return ["More organized code", "More readable code", "Easier code reuse", "Allowing programmers to share and connect code together"]

# Modify this function to concatenate to each benefit - " is a benefit of functions!"
def build_sentence( benefit ):
    return benefit + " is a benefit of functions!"

def name_the_benefits_of_functions():
    list_of_benefits = list_benefits()
    for benefit in list_of_benefits:
        printOutput( build_sentence( benefit ), DESCRIPTIVE="" )

name_the_benefits_of_functions()


