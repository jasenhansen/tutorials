'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Conditions

printChapter( "Conditions", 7 )

printLesson( "Conditions" )

printOutput( "Python is a very simple language, and has a very straightforward syntax" )

printExercise( "Python uses boolean variables to evaluate conditions. The boolean values True and False are returned when an expression is compared or evaluated. For example" )
x = 2

printOutput( x == 2, DESCRIPTIVE="" )  # prints out True
printOutput( x == 3, DESCRIPTIVE="" )  # prints out False
printOutput( x < 3, DESCRIPTIVE="" )  # prints out True
printOutput( "Notice that variable assignment is done using a single equals operator \"=\", whereas comparison between two variables is done using the double equals operator \"==\". The \"not equals\" operator is marked as \"!=\"" )  # prints out True

printLesson( "Boolean operators" )

printExercise( "The \"and\" and \"or\" boolean operators allow building complex boolean expressions, for example" )

name = "John"
age = 23

if name == "John" and age == 23:
    printOutput( "Your name is John, and you are also 23 years old.", DESCRIPTIVE="" )

if name == "John" or name == "Rick":
    printOutput( "Your name is either John or Rick.", DESCRIPTIVE="" )

printExercise( "The \"in\" operator" )

printOutput( "The \"in\" operator could be used to check if a specified object exists within an iterable object container, such as a list" )

name = "John"
if name in ["John", "Rick"]:
    printOutput( "Your name is either John or Rick.", DESCRIPTIVE="" )

printExercise( "Indentation " )

printOutput( "Python uses indentation to define code blocks, instead of brackets. The standard Python indentation is 4 spaces, although tabs and any other space size will work, as long as it is consistent. Notice that code blocks do not need any termination." )

def indentationTest( INPUT, DESCRIPTIVE=None ):
    if type( INPUT ) == int :
        printOutput( "do something", DESCRIPTIVE="" )
    elif type( INPUT ) == str :
        printOutput( "do something else", DESCRIPTIVE="" )
    else:
        printOutput( "do else", DESCRIPTIVE="" )

INPUT = 1
indentationTest( INPUT )
INPUT = "John"
indentationTest( INPUT )
INPUT = []
indentationTest( INPUT )

printExercise( "Indentation Sample" )

x = 2
if x == 2:
    printOutput( "x equals two!", DESCRIPTIVE="" )
else:
    printOutput( "x does not equal to two.", DESCRIPTIVE="" )

printOutput( "A statement is evaulated as true if one of the following is correct: 1. The \"True\" boolean variable is given, or calculated using an expression, such as an arithmetic comparison. 2. An object which is not considered \"empty\" is passed." )
printOutput( "Here are some examples for objects which are considered as empty: 1. An empty string: "" 2. An empty list: [] 3. The number zero: 0 4. The false boolean variable: False" )

printLesson( "The 'is' operator" )

printExercise( "Unlike the double equals operator \"==\", the \"is\" operator does not match the values of the variables, but the instances themselves" )

x = [1, 2, 3]
y = [1, 2, 3]

printOutput( x == y, DESCRIPTIVE="" )  # Prints out True
printOutput( x is y, DESCRIPTIVE="" )  # Prints out False

printLesson( "The 'not' operator" )

printExercise( "Using \"not\" before a boolean expression inverts it" )

printOutput( not False, DESCRIPTIVE="" )  # Prints out True
printOutput( ( not False ) == ( False ), DESCRIPTIVE="" )  # Prints out False

printLesson( "Exercise" )

printExercise( "Change the variables in the first section, so that each if statement resolves as True." )

# change this code
number = 16
second_number = 0
first_array = [1, 4, 5]
second_array = [2, 3]

if number > 15:
    printOutput( "1", DESCRIPTIVE="" )

if first_array:
    printOutput( "2", DESCRIPTIVE="" )

if len( second_array ) == 2:
    printOutput( "3", DESCRIPTIVE="" )

if len( first_array ) + len( second_array ) == 5:
    printOutput( "4", DESCRIPTIVE="" )

if first_array and first_array[0] == 1:
    printOutput( "5", DESCRIPTIVE="" )

if not second_number:
    printOutput( "6", DESCRIPTIVE="" )



