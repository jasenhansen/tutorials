'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../../Framework' )

from Python_Library import printOutput

class game_4:

    def __init__( self ):
        self.importType = None
    # Source of setup
    # http://www.pydev.org/manual.html

    # https://www.learnpython.org/en/Modules_and_Packages

    # game.py
    # define import selector
    def setImports( self, imports ):
        self.importType = imports
        self.draw = imports

#    def my_imports(modulename,shortname = None):
#        if shortname is None:
#            shortname = modulename globals()[shortname] = __import__(modulename)

    # import the draw module
    def importsInternal( self ):
        if self.importType:
            # in visual mode, we draw using graphics
            printOutput( "import: draw_visual" )
            globals()['draw'] = __import__( 'draw_visual' )
        else:
            # in textual mode, we print out text
            printOutput( "import: draw_textual" )
            globals()['draw'] = __import__( 'draw_textual' )

    def play_game( self ):
        printOutput( "game_4: play_game" )
        return ""

    def main( self ):
        if self.importType is None:
            self.importType = True

        self.importsInternal()

        result = self.play_game()
        draw.draw_game( result )
        draw.clear_screen( result )
