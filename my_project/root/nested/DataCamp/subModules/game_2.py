'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../../Framework' )

from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Modules_and_Packages

# game.py
# import all modules
from draw_1 import draw_game
from draw_1 import clear_screen

def play_game():
    printOutput( "game_2: play_game" )
    return ""

def main():
    result = play_game()
    draw_game( result )
    clear_screen( result )

# this means that if this script is executed, then
# main() will be executed
if __name__ == '__main__':
    main()
