'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../../Framework' )

from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Modules_and_Packages

# draw.py

def draw_game( game ):
    printOutput( "draw_1: draw_game", IGNORE_HEADDING="" )

def clear_screen( screen ):
    printOutput( "draw_1: clear_screen", IGNORE_HEADDING="" )
