'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../../Framework' )

from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Modules_and_Packages

# game.py
# import the draw module
import draw_1

def play_game():
    printOutput( "game_1: play_game" )
    return ""

def main():
    result = play_game()
    draw_1.draw_game( result )
    draw_1.draw_game( result )

# this means that if this script is executed, then
# main() will be executed
if __name__ == '__main__':
    main()
