'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Lists

printChapter( "Lists", 3 )

printLesson( "Lists" )

printExercise( "Lists are very similar to arrays." )

mylist = []
mylist.append( 1 )
mylist.append( 2 )
mylist.append( 3 )
printOutput( mylist[0], DESCRIPTIVE="" )
printOutput( mylist[1], DESCRIPTIVE="" )
printOutput( mylist[2], DESCRIPTIVE="" )

# prints out 1,2,3
for x in mylist:
    printOutput( x, DESCRIPTIVE="" )

printExercise( "Accessing an index which does not exist generates an exception (an error)." )

mylist = [1]
printOutput( "This will not work: 'mylist = [1,2,3]' accessing 'mylist[10]'" )

printLesson( "Exercise" )

printExercise( "In this exercise, you will need to add numbers and strings to the correct lists using the 'append' list method." )

numbers = []
strings = []
names = ["John", "Eric", "Jessica"]

# write your code here
second_name = names[1]

numbers.append( 1 )
numbers.append( 2 )
numbers.append( 3 )
strings.append( "hello" )
strings.append( "world" )

printOutput( numbers, DESCRIPTIVE="" )
printOutput( strings, DESCRIPTIVE="" )
printOutput( names, DESCRIPTIVE="" )
printOutput( "The second name on the names list is %s" % second_name, DESCRIPTIVE="" )
