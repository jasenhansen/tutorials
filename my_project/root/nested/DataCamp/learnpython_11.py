'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Dictionaries

printChapter( "Dictionaries", 11 )

printLesson( "Dictionaries" )

printOutput( "A dictionary is a data type similar to arrays, but works with keys and values instead of indexes. Each value stored in a dictionary can be accessed using a key, which is any type of object (a string, a number, a list, etc.) instead of using its index to address it." )

printExercise( "Database of phone numbers could be stored using a dictionary like this." )

phonebook = {}
phonebook["John"] = 938477566
phonebook["Jack"] = 938377264
phonebook["Jill"] = 947662781

printOutput( phonebook, DESCRIPTIVE="" )

printExercise( "Alternatively, a dictionary can be initialized with the same values in the following notation" )

phonebook = {
    "John" : 938477566,
    "Jack" : 938377264,
    "Jill" : 947662781
}

printOutput( phonebook, DESCRIPTIVE="" )

printLesson( "Iterating over dictionaries" )

printExercise( "Dictionaries can be iterated over, just like a list. However, a dictionary, unlike a list, does not keep the order of the values stored in it. To iterate over key value pairs, use the following syntax:" )

phonebook = {"John" : 938477566, "Jack" : 938377264, "Jill" : 947662781}

for name, number in phonebook.items():
    printOutput( "Phone number of %s is %d" % ( name, number ), DESCRIPTIVE="" )

printLesson( "Removing a value" )

printOutput( "To remove a specified index, use either one of the following notations:" )

printExercise( "del using key" )

phonebook = {
   "John" : 938477566,
   "Jack" : 938377264,
   "Jill" : 947662781
}

del phonebook["John"]

printOutput( phonebook, DESCRIPTIVE="" )

printExercise( "pop using key" )

phonebook = {
   "John" : 938477566,
   "Jack" : 938377264,
   "Jill" : 947662781
}

phonebook.pop( "John" )

printOutput( phonebook, DESCRIPTIVE="" )

printLesson( "Exercise" )

printExercise( "Add 'Jake' to the phonebook with the phone number '938273443', and remove 'Jill' from the phonebook." )

phonebook = {
    "John" : 938477566,
    "Jack" : 938377264,
    "Jill" : 947662781
}

printOutput( phonebook, DESCRIPTIVE="" )

# write your code here
phonebook["Jake"] = 938273443

printOutput( phonebook, DESCRIPTIVE="" )

phonebook.pop( "Jill" )

printOutput( phonebook, DESCRIPTIVE="" )

# testing code
if "Jake" in phonebook:
    printOutput( "Jake is listed in the phonebook.", DESCRIPTIVE="" )
if "Jill" not in phonebook:
    printOutput( "Jill is not listed in the phonebook.", DESCRIPTIVE="" )

