'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Variables_and_Types

printChapter( "Variables and Types", 2 )
printOutput( "Python is completely object oriented, and not 'statically typed'. You do not need to declare variables before using them, or declare their type. Every variable in Python is an object." )

printLesson( "Numbers" )
printOutput( "Python supports two types of numbers - integers and floating point numbers." )

printExercise( "To define an integer, use the following syntax" )

myint = 7
printOutput( myint, DESCRIPTIVE="" )

printExercise( "To define a floating point number, you may use one of the following notations" )

myfloat = 7.0
printOutput( myfloat, DESCRIPTIVE="" )
myfloat = float( 7 )
printOutput( myfloat, DESCRIPTIVE="" )

printLesson( "Strings" )

printExercise( "Strings are defined either with a 'single quote' or a \"double quotes\"" )

mystring = 'hello'
printOutput( mystring, DESCRIPTIVE="" )
mystring = "hello"
printOutput( mystring, DESCRIPTIVE="" )

printExercise( "The difference between the two is that using double quotes makes it easy to include apostrophes" )

mystring = "Don't worry about apostrophes"
printOutput( mystring, DESCRIPTIVE="" )

printExercise( "Simple operators can be executed on numbers" )

one = 1
two = 2
three = one + two
printOutput( three, DESCRIPTIVE="" )

printExercise( "Simple operators can be executed on strings" )

hello = "hello"
world = "world"
helloworld = hello + " " + world
printOutput( helloworld, DESCRIPTIVE="" )

printExercise( "(Not working for some reason, throws \"TypeError: 'int' object is not iterable\") Assignments can be done on more than one variable \"simultaneously\" on the same line like this" )

# a, b = 3
a = 3
b = 3
printOutput( a, DESCRIPTIVE="" )
printOutput( b, DESCRIPTIVE="" )

printExercise( "Mixing operators between numbers and strings is not supported" )

# This will not work!
one = 1
two = 2
hello = "hello"

printOutput( one, DESCRIPTIVE="" )
printOutput( two, DESCRIPTIVE="" )
printOutput( hello, DESCRIPTIVE="" )

printOutput( "This will not work: '1' + '2' + 'hello'", DESCRIPTIVE="" )

printLesson( "Exercise" )

printExercise( "The target of this exercise is to create a string, an integer, and a floating point number" )

# change this code
mystring = "hello"
myfloat = 10.0
myint = 20

# testing code
if mystring == "hello":
    printOutput( "String: %s" % mystring, DESCRIPTIVE="" )
if isinstance( myfloat, float ) and myfloat == 10.0:
    printOutput( "Float: %f" % myfloat, DESCRIPTIVE="" )
if isinstance( myint, int ) and myint == 20:
    printOutput( "Integer: %d" % myint, DESCRIPTIVE="" )
