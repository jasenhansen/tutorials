'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Basic_String_Operations

printChapter( "Basic String Operations", 6 )

printLesson( "Basic String Operations" )

printExercise( "'Single' and \"Double\" quotes" )

astring = "Hello world!"
astring2 = 'Hello world!'

printOutput( "Strings are bits of text. They can be defined as anything between quotes" )
printOutput( astring, DESCRIPTIVE="" )
printOutput( astring2, DESCRIPTIVE="" )
printOutput( "As you can see, the first thing you learned was printing a simple sentence. This sentence was stored by Python as a string. However, instead of immediately printing strings out, we will explore the various things you can do to them. You can also use single quotes to assign a string. However, you will face problems if the value to be assigned itself contains single quotes." )

printExercise( "Printing 'Single' quotes" )

astring = "Hello world!"

printOutput( "For example to assign the string in these bracket(single quotes are ' ') you need to use double quotes only like this" )
printOutput( "single quotes are ' '", DESCRIPTIVE="" )

printExercise( "Len" )

astring = "Hello world!"

printOutput( len( astring ), DESCRIPTIVE="" )
printOutput( "That prints out 12, because \"Hello world!\" is 12 characters long, including punctuation and spaces." )

printExercise( "Index" )

astring = "Hello world!"

printOutput( astring, DESCRIPTIVE="" )
printOutput( astring.index( "o" ), DESCRIPTIVE="" )
printOutput( "That prints out 4, because the location of the first occurrence of the letter \"o\" is 4 characters away from the first character. Notice how there are actually two o's in the phrase - this method only recognizes the first.\nBut why didn't it print out 5? Isn't \"o\" the fifth character in the string? To make things more simple, Python (and most other programming languages) start things at 0 instead of 1. So the index of \"o\" is 4." )

printExercise( "Count" )

astring = "Hello world!"

printOutput( astring.count( "l" ), DESCRIPTIVE="" )
printOutput( "For those of you using silly fonts, that is a lowercase L, not a number one. This counts the number of l's in the string. Therefore, it should print 3." )

printExercise( "Slice" )

astring = "Hello world!"

printOutput( astring, DESCRIPTIVE="" )
printOutput( astring[3:7], DESCRIPTIVE="" )
printOutput( "This prints a slice of the string, starting at index 3, and ending at index 6. But why 6 and not 7? Again, most programming languages do this - it makes doing math inside those brackets easier.\nIf you just have one number in the brackets, it will give you the single character at that index. If you leave out the first number but keep the colon, it will give you a slice from the start to the number you left in. If you leave out the second number, if will give you a slice from the first number to the end.\nYou can even put negative numbers inside the brackets. They are an easy way of starting at the end of the string instead of the beginning. This way, -3 means \"3rd character from the end\"" )

printExercise( "Step" )

astring = "Hello world!"

printOutput( astring, DESCRIPTIVE="" )
printOutput( astring[3:7:2], DESCRIPTIVE="" )
printOutput( "This prints the characters of string from 3 to 7 skipping one character. This is extended slice syntax. The general form is [start:stop:step]" )

printExercise( "Step: 1" )

astring = "Hello world!"

printOutput( astring, DESCRIPTIVE="" )
printOutput( astring[3:7], DESCRIPTIVE="" )
printOutput( astring[3:7:1], DESCRIPTIVE="" )
printOutput( "Note that both of them produce same output" )

printExercise( "Step: -1" )

astring = "Hello world!"

printOutput( astring, DESCRIPTIVE="" )
printOutput( astring[::-1], DESCRIPTIVE="" )
printOutput( "There is no function like strrev in C to reverse a string. But with the above mentioned type of slice syntax you can easily reverse a string like this" )

printExercise( "To Upper and Lower Case" )

astring = "Hello world!"

printOutput( astring, DESCRIPTIVE="" )
printOutput( astring.upper(), DESCRIPTIVE="" )
printOutput( astring.lower(), DESCRIPTIVE="" )
printOutput( "These make a new string with all letters converted to uppercase and lowercase, respectively." )

printExercise( "Startswith and Endswith" )

astring = "Hello world!"

printOutput( astring, DESCRIPTIVE="" )
printOutput( astring.startswith( "Hello" ), DESCRIPTIVE="" )
printOutput( astring.endswith( "asdfasdfasdf" ), DESCRIPTIVE="" )
printOutput( "This is used to determine whether the string starts with something or ends with something, respectively. The first one will print True, as the string starts with \"Hello\". The second one will print False, as the string certainly does not end with \"asdfasdfasdf\"" )

printExercise( "Split" )

astring = "Hello world!"
afewwords = astring.split( " " )

printOutput( astring, DESCRIPTIVE="" )
printOutput( afewwords, DESCRIPTIVE="" )
printOutput( "This splits the string into a bunch of strings grouped together in a list. Since this example splits at a space, the first item in the list will be \"Hello\", and the second will be \"world!\"." )

printLesson( "Exercise" )

printExercise( "Try to fix the code to print out the correct information by changing the string." )

s = "Hey there! what should this string be?"

printOutput( "Initial String: '%s'" % s, DESCRIPTIVE="" )

newString = "Strings are awesome!"

printOutput( "New String: '%s'" % newString, DESCRIPTIVE="" )

# Length should be 20
printOutput( "Length of s = %d" % len( newString ) , DESCRIPTIVE="" )

# First occurrence of "a" should be at index 8
printOutput( "The first occurrence of the letter a = %d" % newString.index( "a" ) , DESCRIPTIVE="" )

# Number of a's should be 2
printOutput( "a occurs %d times" % newString.count( "a" ) , DESCRIPTIVE="" )

# Slicing the string into bits
printOutput( "The first five characters are '%s'" % newString[:5] , DESCRIPTIVE="" )  # Start to 5
printOutput( "The next five characters are '%s'" % newString[5:10] , DESCRIPTIVE="" )  # 5 to 10
printOutput( "The thirteenth character is '%s'" % newString[12] , DESCRIPTIVE="" )  # Just number 12
printOutput( "The characters with odd index are '%s'" % newString[1::2] , DESCRIPTIVE="" )  # (0-based indexing)
printOutput( "The last five characters are '%s'" % newString[-5:] , DESCRIPTIVE="" )  # 5th-from-last to end

# Convert everything to uppercase
printOutput( "String in uppercase: %s" % newString.upper() , DESCRIPTIVE="" )

# Convert everything to lowercase
printOutput( "String in lowercase: %s" % newString.lower() , DESCRIPTIVE="" )

# Check how a string starts
if newString.startswith( "Str" ):
    printOutput( "String starts with 'Str'. Good!" , DESCRIPTIVE="" )

# Check how a string ends
if newString.endswith( "ome!" ):
    printOutput( "String ends with 'ome!'. Good!" , DESCRIPTIVE="" )

# Split the string into three separate strings,
# each containing only a word
printOutput( "Split the words of the string: %s" % str( newString.split( " " ) ), DESCRIPTIVE="" )




