'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Basic_Operators

printChapter( "Basic Operators", 4 )

printOutput( "This section explains how to use basic operators in Python." )

printLesson( "Arithmetic Operators" )

printExercise( "Just as any other programming languages, the addition, subtraction, multiplication, and division operators can be used with numbers" )

number = 1 + 2 * 3 / 4.0
printOutput( number, DESCRIPTIVE="" )

printExercise( "Another operator available is the modulo (%) operator, which returns the integer remainder of the division. dividend % divisor = remainder" )

remainder = 11 % 3
printOutput( remainder, DESCRIPTIVE="" )

printExercise( "Using two multiplication symbols makes a power relationship." )

squared = 7 ** 2
printOutput( squared, DESCRIPTIVE="" )

cubed = 2 ** 3
printOutput( cubed, DESCRIPTIVE="" )

printLesson( "Using Operators with Strings" )

printExercise( "Python supports concatenating strings using the addition operator:" )

helloworld = "hello" + " " + "world"
printOutput( helloworld, DESCRIPTIVE="" )

printExercise( "Python also supports multiplying strings to form a string with a repeating sequence" )

lotsofhellos = "hello" * 10
printOutput( lotsofhellos, DESCRIPTIVE="" )

printLesson( "Using Operators with Lists" )

printExercise( "Lists can be joined with the addition operators" )

even_numbers = [2, 4, 6, 8]
odd_numbers = [1, 3, 5, 7]
all_numbers = odd_numbers + even_numbers
printOutput( all_numbers, DESCRIPTIVE="" )

printExercise( "Just as in strings, Python supports forming new lists with a repeating sequence using the multiplication operator" )

printOutput( [1, 2, 3] * 3, DESCRIPTIVE="" )

printLesson( "Exercise" )

printExercise( "The target of this exercise is to create two lists" )

x = object()
y = object()

# TODO: change this code
x_list = [x] * 10
y_list = [y] * 10
big_list = x_list + y_list

printOutput( "x_list %s" % str( x_list ), DESCRIPTIVE="" )
printOutput( "x_list contains %d objects" % len( x_list ), DESCRIPTIVE="" )
printOutput( "y_list %s" % str( y_list ), DESCRIPTIVE="" )
printOutput( "y_list contains %d objects" % len( y_list ), DESCRIPTIVE="" )
printOutput( "big_list %s" % str( big_list ), DESCRIPTIVE="" )
printOutput( "big_list contains %d objects" % len( big_list ), DESCRIPTIVE="" )

# testing code
if x_list.count( x ) == 10 and y_list.count( y ) == 10:
    printOutput( "Almost there...", DESCRIPTIVE="" )
if big_list.count( x ) == 10 and big_list.count( y ) == 10:
    printOutput( "Great!", DESCRIPTIVE="" )
