'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Numpy_Arrays

printChapter( "Numpy Arrays", 13 )

printLesson( "Getting started" )

printOutput( "Numpy arrays are great alternatives to Python Lists. Some of the key advantages of Numpy arrays are that they are fast, easy to work with, and give users the opportunity to perform calculations across entire arrays." )

printExercise( "In the following example, you will first create two Python lists. Then, you will import the numpy package and create numpy arrays out of the newly created lists." )

# Create 2 new lists height and weight
height = [1.87, 1.87, 1.82, 1.91, 1.90, 1.85]
weight = [81.65, 97.52, 95.25, 92.98, 86.18, 88.45]

printOutput( "height: %s" % ( height ), DESCRIPTIVE="" )
printOutput( "weight: %s" % ( weight ), DESCRIPTIVE="" )

# Import the numpy package as np
import numpy as np

# Create 2 numpy arrays from height and weight
np_height = np.array( height )
np_weight = np.array( weight )

