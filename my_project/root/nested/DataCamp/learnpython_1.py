'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Hello%2C_World%21

printChapter( "Hello, World!", 1 )

printLesson( "Hello, World!" )

printOutput( "Python is a very simple language, and has a very straightforward syntax" )

printExercise( "To print a string." )

printOutput( "This line will be printed.", DESCRIPTIVE="" )

printLesson( "Indentation" )

printExercise( "Python uses indentation for blocks" )

my_variable = 10
x = 1
if x == 1:
    # indented four spaces
    printOutput( "x is 1.", DESCRIPTIVE="" )

printLesson( "Exercise" )

printExercise( "Use the 'print' command to print the line 'Hello, World!'" )

printOutput( "Hello, World!", DESCRIPTIVE="" )
printOutput( "Goodbye, World!", DESCRIPTIVE="" )
