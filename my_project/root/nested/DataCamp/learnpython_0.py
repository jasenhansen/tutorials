'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/XXXXXX

printChapter( "CHAPTER", 1 )

printLesson( "LESSON" )

printExercise( "EXERCISE" )

printOutput( "OUTPUT", DESCRIPTIVE="" )
