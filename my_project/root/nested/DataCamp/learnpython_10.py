'''
Created on September 7, 2018

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.learnpython.org/en/Classes_and_Objects

printChapter( "Classes and Objects", 10 )

printLesson( "Classes and Objects" )

printExercise( "Objects are an encapsulation of variables and functions into a single entity. Objects get their variables and functions from classes. Classes are essentially a template to create your objects." )

class MyClass:
    variable = "blah"

    def function( self ):
        printOutput( "This is a message inside the class.", DESCRIPTIVE="" )

printExercise( "We'll explain why you have to include that 'self' as a parameter a little bit later." )

class MyClass_1:
    variable = "blah"

    def function( self ):
        printOutput( "This is a message inside the class.", DESCRIPTIVE="" )

myobjectx = MyClass_1()
myobjectx.function()

printOutput( "Now the variable 'myobjectx' holds an object of the class 'MyClass' that contains the variable and the function defined within the class called 'MyClass'." )

printLesson( "Accessing Object Variables" )

printExercise( "To access the variable inside of the newly created object 'myobjectx' you would do the following" )

class MyClass_2:
    variable = "blah"

    def function( self ):
        printOutput( "This is a message inside the class.", DESCRIPTIVE="" )

myobjectx = MyClass_2()

local_variable = myobjectx.variable
printOutput( local_variable, DESCRIPTIVE="" )

printExercise( "So for instance the below would output the string 'blah'" )

class MyClass_3:
    variable = "blah"

    def function( self ):
        printOutput( "This is a message inside the class.", DESCRIPTIVE="" )

myobjectx = MyClass_3()

local_variable = myobjectx.variable
printOutput( local_variable, DESCRIPTIVE="" )

printExercise( "You can create multiple different objects that are of the same class(have the same variables and functions defined). However, each object contains independent copies of the variables defined in the class. For instance, if we were to define another object with the 'MyClass' class and then change the string in the variable above" )

class MyClass_4:
    variable = "blah"

    def function( self ):
        printOutput( "This is a message inside the class.", DESCRIPTIVE="" )

myobjectx = MyClass_4()
myobjecty = MyClass_4()

myobjecty.variable = "yackity"

printOutput( myobjectx.variable, DESCRIPTIVE="" )
printOutput( myobjecty.variable, DESCRIPTIVE="" )

printLesson( "Accessing Object Functions" )

printExercise( "To access a function inside of an object you use notation similar to accessing a variable" )

class MyClass_5:
    variable = "blah"

    def function( self ):
        printOutput( "This is a message inside the class.", DESCRIPTIVE="" )

myobjectx = MyClass_5()

myobjectx.function()

printLesson( "Exercise" )

printExercise( "We have a class defined for vehicles. Create two new vehicles called car1 and car2. Set car1 to be a red convertible worth $60,000.00 with a name of Fer, and car2 to be a blue van named Jump worth $10,000.00." )

# define the Vehicle class
class Vehicle:
    name = ""
    kind = "car"
    color = ""
    value = 100.00
    def description( self ):
        desc_str = "%s is a %s %s worth $%.2f." % ( self.name, self.color, self.kind, self.value )
        return desc_str

# your code goes here
car1 = Vehicle()
car2 = Vehicle()

car1.name = "Fer"
car1.kind = "convertible"
car1.color = "red"
car1.value = 60000

car2.name = "Jump"
car2.kind = "van"
car2.color = "blue"
car2.value = 10000

# test code
printOutput( car1.description(), DESCRIPTIVE="" )
printOutput( car2.description(), DESCRIPTIVE="" )









