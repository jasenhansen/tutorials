'''
Created on Sep 17, 2018

@author: jasen
'''

import sys
from multiprocessing.pool import MaybeEncodingError
from typing import overload
sys.path.insert( 0, '../../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput


# Source of setup
# http://www.pydev.org/manual.html

# https://www.udemy.com/python-the-complete-python-developer-course/learn/v4/t/lecture/3796218?start=0

printChapter( "The Basics of Python", 5 )

printLesson( "26. More About Variables And Strings", 26 )

printExercise( "Samples!" )

a = 12
b = 3
c = a + b
d = c / 3
e = d - 4
 
printOutput( "e * 12 =  %s" % str( e * 12 ) )