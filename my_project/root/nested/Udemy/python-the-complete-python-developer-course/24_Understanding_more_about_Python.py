'''
Created on Sep 17, 2018

@author: jasen
'''

import sys
from multiprocessing.pool import MaybeEncodingError
from typing import overload
sys.path.insert( 0, '../../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput


# Source of setup
# http://www.pydev.org/manual.html

# https://www.udemy.com/python-the-complete-python-developer-course/learn/v4/t/lecture/3796214?start=0

printChapter( "The Basics of Python", 5 )

printLesson( "Understanding more about Python", 24 )

printExercise( "Samples!" )

greeting = "Hello"
#name = input("Please enter your name ")
#printOutput( greeting + ' ' + name )

splitString = "This string has been\nsplit over\nseveral\nlines"
printOutput( splitString )

tabbedString = "1\t2\t3\t4\t5\t"
printOutput( tabbedString )

printOutput('The pet shop owner said "No, no, \'e\'s, ... he\s resting"')
printOutput("The pet shop owner said \"No, no, \'e\'s, ... he's resting\"")

anotherSplitString = """This string has been
split over
several lines"""

printOutput(anotherSplitString)

printOutput('''The pet shop owner said "No, no, 'e's' uh, ... he's resting"''')

printOutput("""The pet shop owner said "No, no, 'e's' uh, ... he's resting" """)

printExercise( "Coding Exercise 3: Printing tabs" )

printOutput( "Write a program that will produce the following output" )

printOutput("Number 1\tThe Larch")
printOutput("Number 2\tThe Horse Chestnut")
