'''
Created on Sep 17, 2018

@author: jasen
'''

import sys
from multiprocessing.pool import MaybeEncodingError
from typing import overload
sys.path.insert( 0, '../../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput


# Source of setup
# http://www.pydev.org/manual.html

# https://www.udemy.com/python-the-complete-python-developer-course/learn/v4/t/lecture/3796216?start=0

printChapter( "The Basics of Python", 5 )

printLesson( "Storing Items In Variables", 25 )

printExercise( "Samples!" )

greeting = "Bruce"
printOutput( greeting )

_myName = "Time"
printOutput( _myName )

Tim45 = "Good"
printOutput( Tim45 )

Tim_Was_57 = "Hello"
printOutput( Tim_Was_57 )

Greeting = "There"
printOutput( Greeting )

printOutput( Tim_Was_57 + ' ' + greeting )

age = 24
printOutput( age )

printOutput( greeting + str(age) )

printExercise( "Integer" )

a = 12
b = 3
printOutput( "a =  %s" % str( a ) )
printOutput( "b =  %s" % str( b ) )
printOutput( "a + b = %s" % str( a + b  ) )
printOutput( "a - b = %s" % str( a - b  ) )
printOutput( "a / b = %s" % str( a / b  ) )
printOutput( "a // b = %s" % str( a // b)  ) 
printOutput( "a %% b = %s" % str( a % b  ) )
printOutput( "a + b / 3 - 4 * 12 =  %s" % str( a + b /3 - 4 * 12  ) )
printOutput( "8 / 2 * 3  =  %s" % str( 8 / 2 * 3  ) )
printOutput( "8 * 3 / 2  =  %s" % str( 8 * 3 / 2  ) )
printOutput( "(((a + b) /3) - 4) * 12 =  %s" % str( (((a + b) /3) - 4) * 12  ) )

printExercise( "Float" )

c = 1.2
printOutput( b )

printExercise( "Loop with Int" )
for i in range (1,4):
    printOutput( i )

printExercise( "Loop with //" )
for i in range ( 1 , a//b ):
    printOutput( i )

printExercise( "Coding Exercise 4: Integer division" )

printOutput( "You have a shop selling buns for $2.40 each. A customer comes in with $15, and would like to buy as many buns as possible." )

printOutput( "15 // 2.4  =  %s" % str( 15 // 2.4  ) )
