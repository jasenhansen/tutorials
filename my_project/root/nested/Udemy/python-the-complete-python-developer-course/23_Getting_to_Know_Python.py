'''
Created on Sep 17, 2018

@author: jasen
'''

import sys
sys.path.insert( 0, '../../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput


# Source of setup
# http://www.pydev.org/manual.html

# https://www.udemy.com/python-the-complete-python-developer-course/learn/v4/t/lecture/3779674?start=0n

printChapter( "The Basics of Python", 5 )

printLesson( "Geting to Know Python", 23 )

printExercise( "Samples!" )

printOutput( "Hello, World!" )
printOutput( 1+2 )
printOutput( 7*6 )
printOutput( )
printOutput( "The End" )
printOutput( "Pytho's strings are easy to use" )
printOutput( "We can even include 'single quotes' in strings" )
printOutput( 'We can even include "double quotes" in strings' )
printOutput( "hello" + " world")
greeting = "Hello"
name = "Bruce"
printOutput(greeting + name )
# if we want a space, we can add that too
printOutput(greeting + ' ' + name )

printLesson( "Coding Exercise 1: Printing text" )

printExercise( "Write the code to print the string 'My hovercraft is full of eels'" )

printOutput( "My hovercraft is full of eels" )

printExercise( "Coding Exercise 2: Printing the result of a calculation" )

printOutput( "Write a print to answer the calculations 6 times 7" )

printOutput( "6 * 7  =  %s" % str( 6 * 7  ) )

