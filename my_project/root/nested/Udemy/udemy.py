'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import sys
sys.path.insert( 0, '../Framework' )

from Python_Library import printChapter
from Python_Library import printLesson
from Python_Library import printExercise
from Python_Library import printOutput

# Source of setup
# http://www.pydev.org/manual.html

# https://www.codecademy.com/learn/learn-python

printChapter( "Python Syntax", 1 )

printLesson( "Python Syntax", 1 )

# 1 Welcome!
printExercise( "Welcome!" )

printOutput( "Welcome to Python!" )

# 2 Variables
printExercise( "Variables" )

my_variable = 10

printOutput( my_variable, LABEL="Integer value: ", DESCRIPTIVE="" )

# 3 Booleans
printExercise( "Booleans" )

my_int = 7
my_float = 1.23
my_bool = True

printOutput( my_int, LABEL="Integer value: ", DESCRIPTIVE="" )
printOutput( my_float, LABEL="Float value: ", DESCRIPTIVE="" )
printOutput( my_bool, LABEL="Boolean value: ", DESCRIPTIVE="" )

# 4 You've Been Reassigned
printExercise( "You've Been Reassigned" )

my_int = 7
printOutput( my_int, LABEL="Initial value: ", DESCRIPTIVE="" )

my_int = 3
printOutput( my_int, LABEL="Reassigned value: ", DESCRIPTIVE="" )

# 5 Whitespace
printExercise( "Whitespace" )

# 6 Whitespace Means Right Space
printExercise( "Whitespace Means Right Space" )

def spam():
    eggs = 12
    return eggs

printOutput( "Calling 'spam()' with no input causes output '%s'" % ( spam() ) )

# 7 A Matter of Interpretation
printExercise( "A Matter of Interpretation" )

spam = True
eggs = False

printOutput( "'spam' is '%s'" % ( spam ) )
printOutput( "'eggs' is '%s'" % ( eggs ) )

# 8 Single Line Comments
printExercise( "Single Line Comments" )

# I am a comment

# 9 Multi-Line Commentss
printExercise( "Multi-Line Comments" )
"""
This is a multi like comment
"""

# 10 Math
printExercise( "Math" )

big_1 = 100
big_2 = 200
total = big_1 + big_2

printOutput( "'%d + %d'  is '%d'" % ( big_1, big_2, total ) )

# 11 Exponentiation
printExercise( "Exponentiation" )

eggs = 10

printOutput( "'eggs' is '%s'" % ( eggs ) )

eggs = eggs ** 2

printOutput( "'eggs ** 2' is '%s'" % ( eggs ) )

# 12 Modulo
printExercise( "Modulo" )

value_1 = 6
value_2 = 5
spam = 6 % 5

printOutput( "'%d %% %d'  is '%d'" % ( value_1, value_2, spam ) )

# 13 Bringing It All Together
printExercise( "Bringing It All Together" )

monty = True
python = 1.234
value_1 = 2
monty_python = python ** value_1

printOutput( "'monty' is '%s'" % ( monty ) )
printOutput( "'python' is '%s'" % ( python ) )
printOutput( "'monty_python = python ** %s' is '%s'" % ( value_1, eggs ) )
