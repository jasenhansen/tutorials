'''
Created on May 30, 2017

@author: Jasen Hansen
'''

import json, argparse, shutil, errno, os

class mathReader ( object ):

    def __init__( self, args ):
        self.PathTargetConfigurations = args.targetGameConfiguration
        self.PathConfigurations = args.sourceConfigurations
        self.PathPaymodels = args.sourcePaymodels
        self.game_dict = {}

    def addGameToGameDictionary( self, gameID ):
        self.game_dict[ gameID ] = {}
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = ''
        self.game_dict[ gameID ][ 'pathToConfigurations' ] = ''
        self.game_dict[ gameID ][ 'pathToTargetConfigurations' ] = ''
        self.game_dict[ gameID ][ 'paymodels' ] = []
        self.game_dict[ gameID ][ 'playdata' ] = []
        self.game_dict[ gameID ][ 'progressivepaymodels' ] = []
        self.game_dict[ gameID ][ 'gameMath' ] = []

    def loadJSON ( self, pathToFile, fileName ):

        print( 'full: %s/%s.json' % ( pathToFile, fileName ) )

        with open( '%s/%s.json' % ( pathToFile, fileName ), 'r' ) as file:
            values = json.loads( file.read() )

        return values

    def insertPaymodel ( self, math, sequence, gameID ):
        paymodel = '%s.%s.json' % ( math, sequence )
        pathToPaymodel = '%s/%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math, paymodel )
        pathToPlaydata = '%s/games/%s/configs-game/baseline-playdata/baseline-playdata.%s' % ( self.game_dict.get( gameID ).get( 'pathToConfigurations' ), gameID, paymodel )

        if os.path.isfile( pathToPaymodel ):
            if not '%s/%s' % ( math, paymodel ) in self.game_dict[ gameID ][ 'paymodels' ]:
                self.game_dict[ gameID ][ 'paymodels' ].append( '%s/%s' % ( math, paymodel ) )

        if os.path.isfile( pathToPlaydata ):
            if not 'baseline-playdata/baseline-playdata.%s' % ( paymodel ) in self.game_dict[ gameID ][ 'playdata' ]:
                self.game_dict[ gameID ][ 'playdata' ].append( 'baseline-playdata/baseline-playdata.%s' % ( paymodel ) )

    def insertProgressivePaymodel ( self, progressiveID, gameID ):
        pathToProgressivePaymodel = '%s-progressive/progressive-%s.json' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), progressiveID[ 'progressiveMathId' ] )

        if os.path.isfile( pathToProgressivePaymodel ):
            if not 'progressive-%s.json' % ( progressiveID[ 'progressiveMathId' ] ) in self.game_dict[ gameID ][ 'progressivepaymodels' ]:
                self.game_dict[ gameID ][ 'progressivepaymodels' ].append( 'progressive-%s.json' % ( progressiveID[ 'progressiveMathId' ] ) )

    def processC101Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c101Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c101Values[ 'components' ]
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID, gameID )

    def processC102Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c102Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        components = c102Values[ 'components' ]
        for component in components:
            self.processPaymodel ( component[ 'paymodel' ], gameID )

    def processC103Paymodel ( self, math, sequence, gameID ):
        self.insertPaymodel ( math, sequence, gameID )

        c103Values = self.loadJSON ( '%s/%s' % ( self.game_dict.get( gameID ).get( 'pathToPaymodels' ), math ), '%s.%s' % ( math, sequence ) )

        childPaymodels = c103Values[ 'components' ]
        for paymodelID in childPaymodels:
            self.processPaymodel ( paymodelID, gameID )

    def processPaymodel ( self, paymodel, gameID ):
        math, sequence = paymodel.split( '.' )

        complexPaymodels = [ 'C101', 'C102', 'C103' ]
        simplePaymodels = [ 'CTR', 'D101', 'D105', 'D106', 'D107', 'DNT', 'ITD', 'P101', 'S101' ]

        if math in complexPaymodels:
            getattr( self, 'process%sPaymodel' % ( math ) )( math, sequence, gameID )
        elif math in simplePaymodels:
            self.insertPaymodel ( math, sequence, gameID )
        else:
            print ( 'Undefined math type \'%s\'' % ( math ) )

    def processMath ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'paymodels' ]:
            math = paymodel[:paymodel.index( '/' )]

            if not  math in self.game_dict[ gameID ][ 'gameMath' ]:
                self.game_dict[ gameID ][ 'gameMath' ].append( math )

    def getConfigurationPaymodels ( self, gameID ):

        configValues = self.loadJSON ( self.game_dict.get( gameID ).get( 'pathToTargetConfigurations' ), 'game%s' % ( gameID ) )

        if 'validMathIds' in configValues:
            paymodelID = configValues[ 'validMathIds' ]

            for paymodelIDFromConfiguration in paymodelID:
                self.processPaymodel ( paymodelIDFromConfiguration, gameID )

        if 'progressiveConfiguration' in configValues:
            progressiveID = configValues[ 'progressiveConfiguration' ][ 'progressivePaymodelMappings' ]

            for progressiveIDFromConfiguration in progressiveID:
                self.insertProgressivePaymodel ( progressiveIDFromConfiguration, gameID )

    def processGameConfiguration ( self, gameID ):
        self.addGameToGameDictionary ( gameID )

        self.game_dict[ gameID ][ 'pathToTargetConfigurations' ] = '%s/configs-game' % ( self.PathTargetConfigurations )
        self.game_dict[ gameID ][ 'pathToPaymodels' ] = '%s' % ( self.PathPaymodels )
        self.game_dict[ gameID ][ 'pathToConfigurations' ] = '%s' % ( self.PathConfigurations )

        self.getConfigurationPaymodels( gameID )
        self.processMath( gameID )

    def printPaymodelsGame ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'paymodels' ]:
            print ( '%s' % ( paymodel ) )

    def printPlaydataGame ( self, gameID ):
        for playdata in self.game_dict[ gameID ][ 'playdata' ]:
            print ( '%s' % ( playdata ) )

    def printProgressivePaymodelsGame ( self, gameID ):
        for progressivepaymodel in self.game_dict[ gameID ][ 'progressivepaymodels' ]:
            print ( '%s' % ( progressivepaymodel ) )

    def printGameMath ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'gameMath' ]:
            print ( '%s' % ( paymodel ) )

    def printAll ( self, gameID ):
        if gameID in self.game_dict:
            self.printPaymodelsGame ( gameID )
            self.printPlaydataGame ( gameID )
            self.printProgressivePaymodelsGame ( gameID )
            self.printGameMath ( gameID )

    def copyPaymodelsGame ( self, gameID ):
        for paymodel in self.game_dict[ gameID ][ 'paymodels' ]:
            source = '%s/%s' % ( self.game_dict[ gameID ][ 'pathToPaymodels' ], paymodel )
            destination = '%s/configs-game/maths/%s' % ( self.PathTargetConfigurations, paymodel )
            try:
                shutil.copy( source, destination )
            except IOError as e:
                # ENOENT(2): file does not exist, raised also on missing destination parent dir
                if e.errno != errno.ENOENT:
                    raise
                # try creating parent directories
                os.makedirs( os.path.dirname( destination ) )
                shutil.copy( source, destination )

    def copyPlaydataGame ( self, gameID ):
        for playdata in self.game_dict[ gameID ][ 'playdata' ]:
            source = '%s/games/%s/configs-game/%s' % ( self.game_dict[ gameID ][ 'pathToConfigurations' ], gameID, playdata )
            destination = '%s/configs-game/%s' % ( self.PathTargetConfigurations, playdata )
            try:
                shutil.copy( source, destination )
            except IOError as e:
                # ENOENT(2): file does not exist, raised also on missing destination parent dir
                if e.errno != errno.ENOENT:
                    raise
                # try creating parent directories
                os.makedirs( os.path.dirname( destination ) )
                shutil.copy( source, destination )

    def copyProgressivePaymodelsGame ( self, gameID ):
        for progressivepaymodel in self.game_dict[ gameID ][ 'progressivepaymodels' ]:
            source = '%s-progressive/%s' % ( self.game_dict[ gameID ][ 'pathToPaymodels' ], progressivepaymodel )
            destination = '%s/configs-game/progressive/%s' % ( self.PathTargetConfigurations, progressivepaymodel )
            try:
                shutil.copy( source, destination )
            except IOError as e:
                # ENOENT(2): file does not exist, raised also on missing destination parent dir
                if e.errno != errno.ENOENT:
                    raise
                # try creating parent directories
                os.makedirs( os.path.dirname( destination ) )
                shutil.copy( source, destination )

    def copyAll ( self, gameID ):
        if gameID in self.game_dict:
            self.copyPaymodelsGame ( gameID )
            self.copyPlaydataGame ( gameID )
            self.copyProgressivePaymodelsGame ( gameID )

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument( "-c", "--sourceConfigurations", help="Source path to configurations.", type=str, required='True' )
    parser.add_argument( "-g", "--gameID", help="Game ID to process.", type=str, required='True' )
    parser.add_argument( "-p", "--sourcePaymodels", help="Source path to paymodels.", type=str, required='True' )
    parser.add_argument( "-t", "--targetGameConfiguration", help="Path to target Game Configurations.", type=str, required='True' )
    args = parser.parse_args()

    mathReaderObject = mathReader( args )

    mathReaderObject.processGameConfiguration ( args.gameID )
#    mathReaderObject.printAll ( args.gameID )
    mathReaderObject.copyAll ( args.gameID )
