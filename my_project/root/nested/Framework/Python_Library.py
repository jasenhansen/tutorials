'''
Created on May 30, 2017
Notes:
Setup:    http://www.vogella.com/tutorials/Python/article.html
@author: Jasen Hansen
'''

import sys

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

# we can explicitly make assignments on it
this.CHAPTER_TITLE = None
this.CHAPTER_NUMBER = None
this.LESSON_TITLE = None
this.LESSON_NUMBER = None
this.EXERCISE_TITLE = None
this.EXERCISE_NUMBER = None
this.DISPLAY_QUEUE = None

# Set Chapter
def getNumberFromDisplayQueue( DEPTH ):
    LENGTH = len( this.DISPLAY_QUEUE )

    if LENGTH <= DEPTH:
        for x in range( DEPTH - ( LENGTH - 1 ) ):
            this.DISPLAY_QUEUE.append( 1 )

    return this.DISPLAY_QUEUE[DEPTH]

# Set Chapter
def setChapter( TITLE, NUMBER ):
    # Chapter list level is 0 so remove all items after that
    if this.CHAPTER_TITLE is None:
        this.CHAPTER_TITLE = []
        this.CHAPTER_TITLE.append( TITLE )
    else:
        this.CHAPTER_TITLE[0] = TITLE

    if this.CHAPTER_NUMBER is None:
        this.CHAPTER_NUMBER = []
        this.CHAPTER_NUMBER.append( NUMBER )
    else:
        this.CHAPTER_NUMBER[0] = NUMBER

    if this.DISPLAY_QUEUE is None:
        this.DISPLAY_QUEUE = []
    else:
        this.DISPLAY_QUEUE[:] = this.DISPLAY_QUEUE[:0]

    this.DISPLAY_QUEUE.append( NUMBER )

# Print Chapter short circuit
def printChapter( TITLE=None, NUMBER=None ):
    if TITLE is not None and NUMBER is not None:
        setChapter( TITLE, NUMBER )

    OUTPUT = "Python Chapter " + str( this.CHAPTER_NUMBER[0] ) + ':\t' + this.CHAPTER_TITLE[0]
    print ( OUTPUT )

# Set Lesson
def setLesson( TITLE, NUMBER=None ):
    # Lesson level is 1 so remove all items after that
    DISPLAY_QUEUE_LEVEL = 1
    if NUMBER is None:
        LENGTH = len( this.DISPLAY_QUEUE )

        if LENGTH <= DISPLAY_QUEUE_LEVEL:
            NUMBER = getNumberFromDisplayQueue( DISPLAY_QUEUE_LEVEL )
        else:
            NUMBER = getNumberFromDisplayQueue( DISPLAY_QUEUE_LEVEL ) + 1

    if this.LESSON_TITLE is None:
        this.LESSON_TITLE = []
        this.LESSON_TITLE.append( TITLE )
    else:
        this.LESSON_TITLE[0] = TITLE

    if this.LESSON_NUMBER is None:
        this.LESSON_NUMBER = []
        this.LESSON_NUMBER.append( NUMBER )
    else:
        this.LESSON_NUMBER[0] = NUMBER

    if this.DISPLAY_QUEUE is None:
        this.DISPLAY_QUEUE = []
        this.DISPLAY_QUEUE.append( 1 )
    else:
        this.DISPLAY_QUEUE[:] = this.DISPLAY_QUEUE[:DISPLAY_QUEUE_LEVEL]

    this.DISPLAY_QUEUE.append( NUMBER )

# Print Lesson short circuit
def printLesson( TITLE=None, NUMBER=None ):
    if TITLE is not None:
        setLesson( TITLE, NUMBER )

    OUTPUT = '\n' + "Lesson: " + str( this.LESSON_NUMBER[0] ) + ':\t' + this.LESSON_TITLE[0]
    print ( OUTPUT )

# Set Exercise
def setExercise( TITLE, NUMBER=None ):
    # Exercise level is 2 so remove all items after that
    DISPLAY_QUEUE_LEVEL = 2

    if this.DISPLAY_QUEUE is None:
        this.DISPLAY_QUEUE = []
        this.DISPLAY_QUEUE.append( 1 )
        this.DISPLAY_QUEUE.append( 1 )
    else:
        this.DISPLAY_QUEUE[:] = this.DISPLAY_QUEUE[:DISPLAY_QUEUE_LEVEL + 1]

    if NUMBER is None:
        LENGTH = len( this.DISPLAY_QUEUE )

        if LENGTH <= DISPLAY_QUEUE_LEVEL:
            NUMBER = getNumberFromDisplayQueue( DISPLAY_QUEUE_LEVEL )
        else:
            NUMBER = getNumberFromDisplayQueue( DISPLAY_QUEUE_LEVEL ) + 1

    if this.EXERCISE_TITLE is None:
        this.EXERCISE_TITLE = []
        this.EXERCISE_TITLE.append( TITLE )
    else:
        this.EXERCISE_TITLE[0] = TITLE

    if this.EXERCISE_NUMBER is None:
        this.EXERCISE_NUMBER = []
        this.EXERCISE_NUMBER.append( NUMBER )
    else:
        this.EXERCISE_NUMBER[0] = NUMBER

    this.DISPLAY_QUEUE[DISPLAY_QUEUE_LEVEL] = NUMBER

# Print Exercise short circuit
def printExercise( TITLE=None, NUMBER=None ):
    if TITLE is not None:
        setExercise( TITLE, NUMBER )

    OUTPUT = '\n' + "Exercise: " + str( this.EXERCISE_NUMBER[0] ) + ':\t' + this.EXERCISE_TITLE[0]
    print ( OUTPUT )

    LENGTH = len( this.DISPLAY_QUEUE )

    if LENGTH == 3:
        incrementLevel()

# Print new line
def printNewLine():
    print( "" )

# Increment Print Level
def incrementLevel():
    this.DISPLAY_QUEUE.append( 1 )

# Decrement Print Level
def decrementLevel():
    this.DISPLAY_QUEUE.pop()

def functionCall( INCREMENT=None ):
    if not INCREMENT is None:
        incrementLabel()
    incrementLevel()

def functionCleanup():
    decrementLevel()

# Increment Print Level
def incrementLabel():
    LENGTH = len( this.DISPLAY_QUEUE )
    this.DISPLAY_QUEUE[LENGTH - 1] = this.DISPLAY_QUEUE[LENGTH - 1] + 1

# Print formatted Oputput
def printOutput( OUTPUT=None, LABEL=None, DESCRIPTIVE=None, IGNORE_HEADDING=None ):

    if this.DISPLAY_QUEUE is not None:
        LENGTH = len( this.DISPLAY_QUEUE )
        BASE = str( this.DISPLAY_QUEUE[0] )
        for x in range( LENGTH - 1 ):
            BASE += '.' + str( this.DISPLAY_QUEUE[x + 1] )
        BASE += ':\t'
    else:
        BASE = ""

    if OUTPUT is None:
        OUTPUT = ""
        
    if LABEL is None:
        print ( BASE, determine_type( OUTPUT, DESCRIPTIVE ) )
    else:
        print ( BASE, LABEL, determine_type( OUTPUT, DESCRIPTIVE ) )

    if 'LENGTH' in locals() and LENGTH > 2:
        incrementLabel()

def is_Int( value ):
    return type( value ) == int

def is_String( value ):
    return type( value ) == str

def is_Float( value ):
    return type( value ) == float

def is_numeric( value ):
    return is_Int( value ) or is_Float( value )

def inputInt( MESSAGE ):
    VALUE = None
    while not VALUE:
        OUTPUT = input( MESSAGE )
        try:
            OUTPUT = int( OUTPUT )
            VALUE = 1
        except ValueError:
            VALUE = None
    return OUTPUT

def determine_type( INPUT, DESCRIPTIVE=None ):
    if type( INPUT ) == int :
        if DESCRIPTIVE is None:
            OUTPUT = "%d" % INPUT
        else:
            OUTPUT = "int: %d" % INPUT
    elif type( INPUT ) == float :
        if DESCRIPTIVE is None:
            OUTPUT = "%f" % INPUT
        else:
            OUTPUT = "float: %f" % INPUT
    elif type( INPUT ) == bool :
        if DESCRIPTIVE is None:
            OUTPUT = "%s" % INPUT
        else:
            OUTPUT = "bool: %s" % INPUT
    elif type( INPUT ) == str :
        if DESCRIPTIVE is None:
            OUTPUT = "%s" % INPUT
        else:
            OUTPUT = "str: %s" % INPUT
    elif type( INPUT ) == list :
        if DESCRIPTIVE is None:
            OUTPUT = str( INPUT )
        else:
            OUTPUT = "list:", INPUT
    elif type( INPUT ) == dict :
        if DESCRIPTIVE is None:
            OUTPUT = str( INPUT )
        else:
            OUTPUT = "dict:", INPUT
    else:
        if DESCRIPTIVE is None:
            OUTPUT = INPUT
        else:
            OUTPUT = str( type( INPUT ) ) + ":" + str( INPUT )
    return OUTPUT

def helpMe():
    print( "String Printing" )
    print( "\tPrint a string: 'print(\"string\")" )
    print( "\tPrint a variable: 'print(variable)" )
#    https://docs.python.org/2/library/string.html
    print( "\tPrint a string with values inserted:" )
    print( "\t\tSample: \"Insert Binary '%b\' into string\" % (binary_value)" )
    print( "\t\tSample: \"Insert Character '%c\' into string\" % (character_value)" )
    print( "\t\tSample: \"Insert Decimal Integer '%d\' into string\" % (integer_value)" )
    print( "\t\tSample: \"Insert Float '%f\' into string\" % (float_value)" )
    print( "\t\tSample: \"Insert Octal '%o\' into string\" % (octal_value)" )
    print( "\t\tSample: \"Insert lower case Hexadecimal '%x\' into string\" % (hex_value)" )
    print( "\t\tSample: \"Insert upper case Hexadecimal '%X\' into string\" % (hex_value)" )
    print( "\t\tSample: \"Insert Number '%n\' into string\" % (number_value)" )
    print( "\tFloat Precision:" )
    print( "\t\tFormat Method: \"format(float_value, '.2f')\"" )
    print( "\t\tInsert Method: \"Insert Float '%.2f\' into string\" % (float_value)" )
    print( "\t\t Format as percent \"{0:.0%}\".format(float_value)" )
    print( "\tPrint a string with String format method:" )
    print( "\tPrint a string with String format method:" )
