'''
Created on May 30, 2017

@author: Jasen Hansen
'''

from collections import OrderedDict
import json

def printEntry ( tab, key, value ):
    tabs = '\t' * tab
    if ( type( value ) == str ):
        print ( "%s %s : %s" % ( tabs, key, value ) )
    elif ( type( value ) is OrderedDict ):
        print ( "%s %s :" % ( tabs, key ) )
        for key, value in value.items():
            printEntry ( tab + 1, key, value )
    else:
        print ( "Undefined" )

if __name__ == '__main__':
    json_data = open( '../Files/1395.json' )
    data2 = json.load( json_data, object_pairs_hook=OrderedDict )
    for key, value in data2.items():
        printEntry ( 0, key, value )
