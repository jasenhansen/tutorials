package classesInstance;

public class Tuple<X, Y> {
	public final X leftData;
	public final Y rightData;

	public Tuple(X leftData, Y rightData) {
		this.leftData = leftData;
		this.rightData = rightData;
	}

	@Override
	public String toString() {
		return String.format("(%s,%s)", this.leftData.toString(), this.rightData.toString());
	}
}
