package classesInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class BinaryMinHeap {
	/** The number of children each node has **/
	private static final int childrenPerNode = 2;
	private List<Integer> heap;

	/** Constructor **/
	public BinaryMinHeap() {
		heap = new ArrayList<Integer>();
	}

	/** Function to delete element at an index **/
	public int delete(int index) {
		if (isEmpty())
			throw new NoSuchElementException("Underflow Exception");
		int keyItem = heap.get(index);
		heap.set(index, heap.get(heap.size() - 1));
		heap.remove(index);
		heapifyDown(heap.size() - 1);
		return keyItem;
	}

	/** Function to delete min element **/
	public int deleteMin() {
		if (isEmpty())
			throw new NoSuchElementException("Underflow Exception");
		else {
			int keyItem = heap.get(0);
			delete(0);
			return keyItem;
		}
	}

	/** Function heapifyDown **/
	private void heapifyDown(int index) {
		if (index >= 0) {
			int child;
			int temp = heap.get(index);
			while (kthChild(index, 1) < heap.size()) {
				child = minChild(index);
				if (heap.get(child) < temp)
					heap.set(index, child);
				else
					break;
				index = child;
			}
			heap.set(index, temp);
		}
	}

	/** Function heapifyUp **/
	private void heapifyUp(int childIndex) {
		int temp = heap.get(childIndex);
		while (childIndex > 0 && temp < heap.get(parent(childIndex))) {
			heap.set(childIndex, heap.get(parent(childIndex)));
			childIndex = parent(childIndex);
		}
		heap.set(childIndex, temp);
	}

	/** Function to insert element */
	public void insert(int data) {
		/** Percolate up **/
		heap.add(data);
		heapifyUp(heap.size() - 1);
	}

	/** Function to check if heap is empty **/
	public boolean isEmpty() {
		return heap.isEmpty();
	}

	/** Function to check if heap size **/
	public int size() {
		return heap.size();
	}

	/** Function to get index of k th child of i **/
	private int kthChild(int i, int k) {
		return childrenPerNode * i + k;
	}

	/** Clear heap */
	public void makeEmpty() {
		heap.clear();
	}

	/** Function to get smallest child **/
	private int minChild(int ind) {
		int bestChild = kthChild(ind, 1);
		int k = 2;
		int pos = kthChild(ind, k);
		while ((k <= childrenPerNode) && (pos < heap.size())) {
			if (heap.get(pos) < heap.get(bestChild))
				bestChild = pos;
			pos = kthChild(ind, k++);
		}
		return bestChild;
	}

	/** Function to get index parent of i **/
	private int parent(int i) {
		return (i - 1) / childrenPerNode;
	}

	/** Function to print heap **/
	public void printHeap() {
		System.out.print("\nHeap = ");
		for (int index = 0; index < heap.size(); index++)
			System.out.print(heap.get(index) + " ");
		System.out.println();
	}
}