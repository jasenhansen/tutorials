package classesInstance;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTreeHeightBalanced<T extends Comparable<T>> {

	// Root node of tree
	private BinaryTreeNodeHeight<T> root;

	// Default constructor for empty tree
	public BinarySearchTreeHeightBalanced() {
		root = null;
	}

	public BinarySearchTreeHeightBalanced(T value) {
		root = new BinaryTreeNodeHeight<T>(value);
	}

	// removes root reference for garbage collection
	public void clear() {
		root = null;
	}

	// Entry point for node count. Uses forwarding logic.
	public int countNodes() {
		return countNodes(root);
	}

	// Recursive call to count number of nodes from current node
	private int countNodes(BinaryTreeNodeHeight<T> entryNode) {
		// If node is not assigned it can not have a count
		if (entryNode == null) {
			return 0;
		} else {
			// Assign value for self
			int count = 1;
			// Increment count by count of left child
			count += countNodes(entryNode.getLeftChild());
			// Increment count by count of right child
			count += countNodes(entryNode.getRightChild());
			return count;
		}
	}

	public String dataBreadth() {
		return dataBreadth(root).trim();
	}

	private String dataBreadth(BinaryTreeNodeHeight<T> entryNode) {
		Queue<BinaryTreeNodeHeight<T>> myQueue = new LinkedList<BinaryTreeNodeHeight<T>>();
		String returnValue = "";
		if (entryNode != null) {
			myQueue.add(entryNode);
		}
		while (!myQueue.isEmpty()) {
			BinaryTreeNodeHeight<T> node = myQueue.remove();
			returnValue += node.getData() + " ";
			if (node.getLeftChild() != null) {
				myQueue.add(node.getLeftChild());
			}
			if (node.getRightChild() != null) {
				myQueue.add(node.getRightChild());
			}
		}
		return returnValue;
	}

	public String dataInorder() {
		return dataInorder(root).trim();
	}

	private String dataInorder(BinaryTreeNodeHeight<T> entryNode) {
		String returnValue = "";
		if (entryNode != null) {
			returnValue += dataInorder(entryNode.getLeftChild());
			returnValue += entryNode.getData() + " ";
			returnValue += dataInorder(entryNode.getRightChild());
		}
		return returnValue;
	}

	public String dataPostorder() {
		return dataPostorder(root).trim();
	}

	private String dataPostorder(BinaryTreeNodeHeight<T> entryNode) {
		String returnValue = "";
		if (entryNode != null) {
			returnValue += dataPostorder(entryNode.getLeftChild());
			returnValue += dataPostorder(entryNode.getRightChild());
			returnValue += entryNode.getData() + " ";
		}
		return returnValue;
	}

	public String dataPreorder() {
		return dataPreorder(root).trim();
	}

	private String dataPreorder(BinaryTreeNodeHeight<T> entryNode) {
		String returnValue = "";
		if (entryNode != null) {
			returnValue += entryNode.getData() + " ";
			returnValue += dataPreorder(entryNode.getLeftChild());
			returnValue += dataPreorder(entryNode.getRightChild());
		}
		return returnValue;
	}

	public BinaryTreeNodeHeight<T> getRoot() {
		return root;
	}

	private int height(BinaryTreeNodeHeight<T> entryNode) {
		return entryNode == null ? -1 : entryNode.getHeight();
	}

	// Insert data from root and allow internal logic to handle the sorting
	public void insert(T data) {
		root = insert(data, root);
	}

	// recursive logic to insert data
	private BinaryTreeNodeHeight<T> insert(T data, BinaryTreeNodeHeight<T> entryNode) {
		if (entryNode == null) {
			entryNode = new BinaryTreeNodeHeight<T>(data);
		} else if (data.compareTo(entryNode.getData()) < 0) {
			entryNode.setLeftChild(insert(data, entryNode.getLeftChild()));

			if (height(entryNode.getLeftChild()) - height(entryNode.getRightChild()) == 2) {
				if (data.compareTo(entryNode.getLeftChild().getData()) < 0) {
					entryNode = rotateRight(entryNode);
				} else {
					entryNode = rebalanceWithLeftChild(entryNode);
				}
			}
		} else if (data.compareTo(entryNode.getData()) > 0) {
			entryNode.setRightChild(insert(data, entryNode.getRightChild()));

			if (height(entryNode.getRightChild()) - height(entryNode.getLeftChild()) == 2) {
				if (data.compareTo(entryNode.getRightChild().getData()) > 0) {
					entryNode = rotateLeft(entryNode);
				} else {
					entryNode = rebalanceWithRightChild(entryNode);
				}
			}
		}
		entryNode.setHeight(maximumValue(height(entryNode.getLeftChild()), height(entryNode.getRightChild())) + 1);
		return entryNode;
	}

	// Test to see if tree is empty
	public boolean isEmpty() {
		return (root == null);
	}

	private int maximumValue(int leftValue, int rightValue) {
		return leftValue > rightValue ? leftValue : rightValue;
	}

	public T maxValue() {
		return maxValue(root);
	}

	private T maxValue(BinaryTreeNodeHeight<T> entryNode) {
		T maxValue = entryNode.getData();
		while (entryNode.getRightChild() != null) {
			maxValue = entryNode.getRightChild().getData();
			entryNode = entryNode.getRightChild();
		}
		return maxValue;
	}

	public T minValue() {
		return minValue(root);
	}

	private T minValue(BinaryTreeNodeHeight<T> entryNode) {
		T minValue = entryNode.getData();
		while (entryNode.getLeftChild() != null) {
			minValue = entryNode.getLeftChild().getData();
			entryNode = entryNode.getLeftChild();
		}
		return minValue;
	}

	public void outputBreadth() {
		System.out.print(dataBreadth());
	}

	public void outputInorder() {
		System.out.print(dataInorder());
	}

	public void outputPostorder() {
		System.out.print(dataPostorder());
	}

	public void outputPreorder() {
		System.out.print(dataPreorder());
	}

	private BinaryTreeNodeHeight<T> rebalanceWithLeftChild(BinaryTreeNodeHeight<T> entryNode) {
		entryNode.setLeftChild(rotateLeft(entryNode.getLeftChild()));
		return rotateRight(entryNode);
	}

	private BinaryTreeNodeHeight<T> rebalanceWithRightChild(BinaryTreeNodeHeight<T> entryNode) {
		entryNode.setRightChild(rotateRight(entryNode.getRightChild()));
		return rotateLeft(entryNode);
	}

	// Insert data from root and allow internal logic to handle the sorting
	public void remove(T data) {
		remove(data, root);
	}

	private BinaryTreeNodeHeight<T> remove(T data, BinaryTreeNodeHeight<T> entryNode) {
		if (entryNode == null) {
			return entryNode;
		} else if (data.compareTo(entryNode.getData()) == 0) {
			// if right child is null
			if (entryNode.getRightChild() == null) {
				return entryNode.getLeftChild();
			} else if (entryNode.getLeftChild() == null) {
				return entryNode.getRightChild();
			} else {
				// node with two children: Get the inorder successor (smallest
				// in the right subtree)
				entryNode.setData(minValue(entryNode.getRightChild()));
				// Delete the inorder successor
				entryNode.setRightChild(remove(entryNode.getData(), entryNode.getRightChild()));
			}
		} else if (data.compareTo(entryNode.getData()) < 0) {
			entryNode.setLeftChild(remove(data, entryNode.getLeftChild()));
		} else if (data.compareTo(entryNode.getData()) > 0) {
			entryNode.setRightChild(remove(data, entryNode.getRightChild()));
		}

		// I need to rebalance here
		if (height(entryNode.getLeftChild()) - height(entryNode.getRightChild()) == 2) {
			if (data.compareTo(entryNode.getLeftChild().getData()) < 0) {
				entryNode = rotateRight(entryNode);
			} else {
				entryNode = rebalanceWithLeftChild(entryNode);
			}
		} else if (height(entryNode.getRightChild()) - height(entryNode.getLeftChild()) == 2) {
			if (data.compareTo(entryNode.getRightChild().getData()) > 0) {
				entryNode = rotateLeft(entryNode);
			} else {
				entryNode = rebalanceWithRightChild(entryNode);
			}
		}
		return entryNode;
	}

	public T rootValue() {
		return root.getData();
	}

	private BinaryTreeNodeHeight<T> rotateLeft(BinaryTreeNodeHeight<T> entryNode) {
		BinaryTreeNodeHeight<T> rightChild = entryNode.getRightChild();
		entryNode.setRightChild(rightChild.getLeftChild());
		rightChild.setLeftChild(entryNode);
		entryNode.setHeight(maximumValue(height(entryNode.getLeftChild()), height(entryNode.getRightChild())) + 1);
		rightChild.setHeight(maximumValue(height(rightChild.getRightChild()), entryNode.getHeight()) + 1);
		return rightChild;
	}

	private BinaryTreeNodeHeight<T> rotateRight(BinaryTreeNodeHeight<T> entryNode) {
		BinaryTreeNodeHeight<T> leftChild = entryNode.getLeftChild();
		entryNode.setLeftChild(leftChild.getRightChild());
		leftChild.setRightChild(entryNode);
		entryNode.setHeight(maximumValue(height(entryNode.getLeftChild()), height(entryNode.getRightChild())) + 1);
		leftChild.setHeight(maximumValue(height(leftChild.getLeftChild()), entryNode.getHeight()) + 1);
		return leftChild;
	}

	// recursive method to search tree for data
	private boolean search(BinaryTreeNodeHeight<T> entryNode, T data) {
		if (entryNode == null) {
			return false;
		}
		// If data is less than searchNode data than traverse tree down the
		// left child path
		else if (data.compareTo(entryNode.getData()) < 0) {
			return search(entryNode.getLeftChild(), data);
		}
		// If data is greater than searchNode data than traverse tree down
		// the right child path
		else if (data.compareTo(entryNode.getData()) > 0) {
			return search(entryNode.getRightChild(), data);
		}
		// If data is equal searchNode data than value found
		else if (data.compareTo(entryNode.getData()) == 0) {
			return true;
		}

		return false;
	}

	// Entry point for data search
	public boolean search(T data) {
		return search(root, data);
	}

	@Override
	public String toString() {
		return String.format("(root: %s)", this.root.toString());
	}

}
