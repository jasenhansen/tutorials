package classesInstance;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class GraphNode<X extends Comparable<X>, Y, Z> implements Comparable<GraphNode<X, Y, Z>> {
	private X id;
	private Y data;
	private Map<X, Edge<X, Z>> adjacent = new HashMap<>();

	public GraphNode(Object id) {
		assertNotNull(id);
		this.id = (X) id;
	}

	public GraphNode(Object id, Object data) {
		this(id);
		this.data = (Y) data;
	}

	public void addAdjacent(Object... values) {
		X id = null;
		Object edgeData = null;

		if (values.length > 0) {
			if (values[0] instanceof GraphNode<?, ?, ?>) {
				id = (X) ((GraphNode<?, ?, ?>) values[0]).getId();
			} else {
				id = (X) values[0];
			}
		}

		if (values.length > 1) {
			edgeData = values[1];
		}

		this.adjacent.put(id, new Edge<>(id, edgeData));
	}

	@Override
	public int compareTo(GraphNode<X, Y, Z> target) {
		return this.id.compareTo(target.getId());
	}

	public int getAdjacencyCount() {
		return adjacent.size();
	}

	public List<X> getAdjacencyList() {
		List<X> forSorting = new ArrayList<>(this.adjacent.keySet());
		Collections.sort(forSorting);
		return forSorting;
	}

	public Y getData() {
		return this.data;
	}

	public X getId() {
		return this.id;
	}

	public boolean isAdjacent(Object value) {
		Object id = null;

		if (value instanceof GraphNode<?, ?, ?>) {
			id = ((GraphNode<?, ?, ?>) value).getId();
		} else {
			id = value;
		}

		return this.adjacent.containsKey(id);
	}

	public void removeAdjacent(Object value) {
		Object id = null;

		if (value instanceof GraphNode<?, ?, ?>) {
			id = ((GraphNode<?, ?, ?>) value).getId();
		} else {
			id = value;
		}

		this.adjacent.remove(id);
	}

	public void setAdjacent(Object... values) {
		X id = null;
		Object data = null;

		if (values.length > 0) {
			if (values[0] instanceof GraphNode<?, ?, ?>) {
				id = (X) ((GraphNode<?, ?, ?>) values[0]).getId();
			} else {
				id = (X) values[0];
			}
		}

		if (values.length > 1) {
			if (values[1] instanceof GraphNode<?, ?, ?>) {
				data = ((GraphNode<?, ?, ?>) values[1]).getId();
			} else {
				data = values[1];
			}
		}
		this.adjacent.put(id, new Edge<>(id, data));
	}

	public void setData(Object data) {
		this.data = (Y) data;
	}

	public void setId(Object id) {
		this.id = (X) id;
	}

	@Override
	public String toString() {
		if (this.adjacent.isEmpty()) {
			return String.format("(id: %s, data: %s)", this.id.toString(), this.data.toString());
		} else {
			return String.format("(id: %s, data: %s, adjacent to: %s)", this.id.toString(), this.data.toString(),
					getAdjacencyList().toString().replace("[", "").replace("]", ""));
		}
	}

}