package classesInstance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class GraphAdjacency<X extends Comparable<X>, Y> {
	HashMap<X, LinkedList<Edge<X, Y>>> map;

	public GraphAdjacency() {
		this.map = new HashMap<>();
	}

	public void addEdge(X source, X target, Y targetData) {
		if (map.get(source) == null) {
			map.put(source, new LinkedList<>());
		}
		map.get(source).add(new Edge<>(target, targetData));
	}

	public ArrayList<X> getNodeIDs() {
		return new ArrayList<>(map.keySet());
	}

	public void printBredthFirst(X source) {
		List<X> path = new ArrayList<>();
		Queue<X> queue = new LinkedList<>();
		Set<X> visited = new HashSet<>();
		queue.add(source);
		visited.add(source);
		while (!queue.isEmpty()) {
			X local = queue.remove();
			path.add(local);
			LinkedList<Edge<X, Y>> edges = map.get(local);
			for (Edge<X, Y> edge : edges) {
				if (!visited.contains(edge.getId())) {
					queue.add(edge.getId());
					visited.add(edge.getId());
				}
			}
		}
		
		System.out.format("%s: -> %s\n", source, path.toString().replace("[", "").replace("]", ""));
	}

	@Override
	public String toString() {
		String returnString = "";
		for (X key : map.keySet()) {
			returnString += String.format("key: %s, adjacencies: %s\n", key.toString(), map.get(key).toString());
		}
		return returnString;
	}
}
