package classesInstance;

public class BinaryTreeNodeHeight<T extends Comparable<T>> {
	private T data;
	private int height;
	private BinaryTreeNodeHeight<T> leftChild;
	private BinaryTreeNodeHeight<T> rightChild;

	public BinaryTreeNodeHeight() {
		this.leftChild = null;
		this.rightChild = null;
		this.data = null;
		this.height = 0;
	}

	public BinaryTreeNodeHeight(T input) {
		this.leftChild = null;
		this.rightChild = null;
		this.data = input;
		this.height = 0;
	}

	@Override
	public String toString() {
		return String.format("(data: %s, height: %d )", this.data.toString(), this.height);
	}

	public T getData() {
		return this.data;
	}

	public int getHeight() {
		return this.height;
	}

	public BinaryTreeNodeHeight<T> getLeftChild() {
		return this.leftChild;
	}

	public BinaryTreeNodeHeight<T> getRightChild() {
		return this.rightChild;
	}

	public void setData(T input) {
		this.data = input;
	}

	public void setHeight(int input) {
		this.height = input;
	}

	public void setLeftChild(BinaryTreeNodeHeight<T> input) {
		this.leftChild = input;
	}

	public void setRightChild(BinaryTreeNodeHeight<T> input) {
		this.rightChild = input;
	}
}