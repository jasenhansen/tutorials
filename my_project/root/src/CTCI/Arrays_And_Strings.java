package CTCI;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

public class Arrays_And_Strings {

	public static String convertAndSort(String input) {
		char[] content = input.toCharArray();
		Arrays.sort(content);
		return new String(content);
	}

	public static void getDescription() {
		System.out.println("Section 1. Arrays and Strings");
	}

	public static boolean singleEdit(String left, String right) {

		String longString;
		String shortString;
		if (left.length() > right.length()) {
			longString = left;
			shortString = right;
		} else {
			longString = right;
			shortString = left;
		}
		int longPointer = 0;
		int shortPointer = 0;

		boolean singleEdit = false;
		if (longString.length() != shortString.length()) {
			singleEdit = true;
		}

		while ((longPointer < longString.length()) && (shortPointer < shortString.length())) {
			// Base case no change
			if (longString.charAt(longPointer) != shortString.charAt(shortPointer)) {
				// Test to see if the difference is an insert
				if (((longPointer + 1) < longString.length()) && (longString.length() != shortString.length())) {
					// Check character at pointer + 1
					if (longString.charAt(longPointer + 1) == shortString.charAt(shortPointer)) {
						longPointer++;
					} else {
						singleEdit = false;
						break;
					}
				} else {
					if (singleEdit) {
						singleEdit = false;
						break;
					}
					singleEdit = true;
				}
			}
			longPointer++;
			shortPointer++;
		}
		return singleEdit;
	}

	public static boolean isPermutation(String left, String right) {
		return convertAndSort(left).equals(convertAndSort(right));
	}

	public static boolean isUnique(String input) {
		Set<Character> charSet = new HashSet<Character>();
		for (char character : input.toCharArray()) {
			charSet.add(character);
		}
		return (charSet.size() == input.length()) ? true : false;
	}

	public static boolean palendromePermutation(String input) {
		HashMap<String, Integer> letterMap = new HashMap<String, Integer>();
		String temp = input.toLowerCase().replaceAll("\\s+", "");
		for (int i = 0; i < temp.length(); i++) {
			String letter = temp.substring(i, i + 1);
			if (letterMap.containsKey(letter)) {
				letterMap.put(letter, letterMap.get(letter) + 1);
			} else {
				letterMap.put(letter, 1);
			}
		}
		// if Size is odd 1 odd character is required.
		boolean even = (temp.length() % 2 == 0);
		boolean firstOddFound = false;
		for (Entry<String, Integer> test : letterMap.entrySet()) {
			boolean oddCount = (test.getValue() % 2 == 1);
			if (oddCount && even) {
				return false;
			} else if (oddCount && firstOddFound) {
				return false;
			} else if (oddCount) {
				firstOddFound = true;
			}
		}
		if (even && firstOddFound) {
			return false;
		}
		if (!even && !firstOddFound) {
			return false;
		}
		return true;
	}

	public static String URLif(String string) {
		int writePointer = string.length() - 1;
		int readPointer = 0;

		char[] process = string.toCharArray();
		for (int i = writePointer; i >= 0; i--) {
			if (process[i] != ' ') {
				readPointer = i;
				break;
			}
		}

		for (; readPointer >= 0; readPointer--) {
			if (readPointer == writePointer) {
				break;
			} else if (process[readPointer] == ' ') {
				// space detected
				process[writePointer--] = '0';
				process[writePointer--] = '2';
				process[writePointer--] = '%';
			} else {
				process[writePointer--] = process[readPointer];
			}
		}
		return new String(process);
	}

	public static String compressedString(String word2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
