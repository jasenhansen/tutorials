package interfaces;

public interface MathOperation {
	int operation(int a, int b);

	static int operate(int a, int b, MathOperation mathOperation) {
		return mathOperation.operation(a, b);
	}
}