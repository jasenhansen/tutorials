package interfaces;

public interface GreetingService {

	final static String salutation = "Hello! ";
	
	void sayMessage(String message);

}