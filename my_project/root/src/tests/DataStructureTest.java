package tests;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runners.MethodSorters;

import classesInstance.BinarySearchTreeHeightBalanced;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataStructureTest {
	@Rule
	public TestName name = new TestName();

	@Test
	public void BinarySearchTreeHeightBalanced_10() {

		BinarySearchTreeHeightBalanced<Integer> HeightBalancedBinarySearchTree = new BinarySearchTreeHeightBalanced<>();
		int count = 10;

		for (int index = 0; index < count; index++)
			HeightBalancedBinarySearchTree.insert(Integer.valueOf(index));

		Integer intTest = HeightBalancedBinarySearchTree.countNodes();
		System.out.println("Node Count   : " + intTest);
		assertEquals("Node Count: 10", intTest, Integer.valueOf(count));

		intTest = HeightBalancedBinarySearchTree.rootValue();
		System.out.println("Root Value   : " + intTest);
		assertEquals("Root Value: 3", intTest, Integer.valueOf(3));

		intTest = HeightBalancedBinarySearchTree.minValue();
		System.out.println("Minumum Value   : " + intTest);
		assertEquals("Minumum Value: 0", intTest, Integer.valueOf(0));

		intTest = HeightBalancedBinarySearchTree.maxValue();
		System.out.println("Maxumum Value   : " + intTest);
		assertEquals("Maxumum Value: 0", intTest, Integer.valueOf(count - 1));

		Boolean boolTest = HeightBalancedBinarySearchTree.search(3);
		System.out.println("Contains 3  : " + boolTest);
		assertEquals("Contains 3: true", boolTest, true);

		boolTest = HeightBalancedBinarySearchTree.search(11);
		System.out.println("Contains 11  : " + boolTest);
		assertEquals("Contains 11: false", boolTest, false);

		System.out.println("Pre-order  :");
		HeightBalancedBinarySearchTree.outputPreorder();
		String testValue = HeightBalancedBinarySearchTree.dataPreorder();
		assertEquals("Pre-order output: 3 1 0 2 7 5 4 6 8 9", testValue, "3 1 0 2 7 5 4 6 8 9");

		System.out.println("\nIn-order   :");
		HeightBalancedBinarySearchTree.outputInorder();
		testValue = HeightBalancedBinarySearchTree.dataInorder();
		assertEquals("In-orde output: 0 1 2 3 4 5 6 7 8 9", testValue, "0 1 2 3 4 5 6 7 8 9");

		System.out.println("\nPost-order :");
		HeightBalancedBinarySearchTree.outputPostorder();
		testValue = HeightBalancedBinarySearchTree.dataPostorder();
		assertEquals("Post-order output: 0 2 1 4 6 5 9 8 7 3", testValue, "0 2 1 4 6 5 9 8 7 3");

		System.out.println("\nBreadth first :");
		HeightBalancedBinarySearchTree.outputBreadth();

		testValue = HeightBalancedBinarySearchTree.dataBreadth();
		assertEquals("Breadth first output: 3 1 7 0 2 5 8 4 6 9", testValue, "3 1 7 0 2 5 8 4 6 9");

		System.out.println("\nRemove  : 5");
		HeightBalancedBinarySearchTree.remove(5);

		boolTest = HeightBalancedBinarySearchTree.search(5);
		System.out.println("Contains 5  : " + boolTest);
		assertEquals("Contains 5: false", boolTest, false);

		System.out.println("Pre-order  :");
		HeightBalancedBinarySearchTree.outputPreorder();
		testValue = HeightBalancedBinarySearchTree.dataPreorder();
		assertEquals("Pre-order output: 3 1 0 2 7 6 4 8 9", testValue, "3 1 0 2 7 6 4 8 9");

		System.out.println("\nIn-order   :");
		HeightBalancedBinarySearchTree.outputInorder();
		testValue = HeightBalancedBinarySearchTree.dataInorder();
		assertEquals("In-orde output: 0 1 2 3 4 6 7 8 9", testValue, "0 1 2 3 4 6 7 8 9");

		System.out.println("\nPost-order :");
		HeightBalancedBinarySearchTree.outputPostorder();
		testValue = HeightBalancedBinarySearchTree.dataPostorder();
		assertEquals("Post-order output: 0 2 1 4 6 9 8 7 3", testValue, "0 2 1 4 6 9 8 7 3");

		System.out.println("\nBreadth first :");
		HeightBalancedBinarySearchTree.outputBreadth();
		testValue = HeightBalancedBinarySearchTree.dataBreadth();
		assertEquals("Breadth first output: 3 1 7 0 2 8 4 6 9", testValue, "3 1 7 0 2 6 8 4 9");
	}

	@Test
	public void BinarySearchTreeHeightBalanced_100() {

		BinarySearchTreeHeightBalanced<Integer> HeightBalancedBinarySearchTree = new BinarySearchTreeHeightBalanced<>();
		int count = 100;

		for (int index = 0; index < count; index++)
			HeightBalancedBinarySearchTree.insert(Integer.valueOf(index));

		Integer intTest = HeightBalancedBinarySearchTree.countNodes();
		System.out.println("Node Count   : " + intTest);
		assertEquals("Node Count: 100", intTest, Integer.valueOf(count));

		intTest = HeightBalancedBinarySearchTree.rootValue();
		System.out.println("Root Value   : " + intTest);

		intTest = (Integer) HeightBalancedBinarySearchTree.minValue();
		System.out.println("Minumum Value   : " + intTest);
		assertEquals("Minumum Value: 0", intTest, Integer.valueOf(0));

		intTest = HeightBalancedBinarySearchTree.maxValue();
		System.out.println("Maxumum Value   : " + intTest);
		assertEquals("Maxumum Value: 0", intTest, Integer.valueOf(count - 1));

		Boolean boolTest = HeightBalancedBinarySearchTree.search(3);
		System.out.println("Contains 3  : " + boolTest);
		assertEquals("Contains 3: true", boolTest, true);

		System.out.println("Pre-order  :");
		HeightBalancedBinarySearchTree.outputPreorder();

		System.out.println("\nIn-order   :");
		HeightBalancedBinarySearchTree.outputInorder();

		System.out.println("\nPost-order :");
		HeightBalancedBinarySearchTree.outputPostorder();

		System.out.println("\nBreadth first :");
		HeightBalancedBinarySearchTree.outputBreadth();

		System.out.println("\nRemove  : 5");
		HeightBalancedBinarySearchTree.remove(5);

		boolTest = HeightBalancedBinarySearchTree.search(5);
		System.out.println("Contains 5  : " + boolTest);
		assertEquals("Contains 5: false", boolTest, false);

		System.out.println("Pre-order  :");
		HeightBalancedBinarySearchTree.outputPreorder();

		System.out.println("\nIn-order   :");
		HeightBalancedBinarySearchTree.outputInorder();

		System.out.println("\nPost-order :");
		HeightBalancedBinarySearchTree.outputPostorder();

		System.out.println("\nBreadth first :");
		HeightBalancedBinarySearchTree.outputBreadth();

	}
}
