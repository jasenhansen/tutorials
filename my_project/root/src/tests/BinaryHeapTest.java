package tests;

//import java.util.Scanner;
import classesInstance.BinaryMaxHeap;
import classesInstance.BinaryMinHeap;

/** Class BinaryHeapTest **/
public class BinaryHeapTest {
	public static void main(String[] args) {
		// Scanner scan = new Scanner(System.in);
		System.out.println("Binary Heap Test\n\n");
		/** Make object of BinaryHeap **/
		System.out.println("Max Heap");
		BinaryMaxHeap maxHeap = new BinaryMaxHeap();
		maxHeap.insert(24);
		maxHeap.printHeap();
		maxHeap.insert(6);
		maxHeap.printHeap();
		maxHeap.insert(28);
		maxHeap.printHeap();
		maxHeap.insert(5);
		maxHeap.printHeap();
		maxHeap.insert(63);
		maxHeap.printHeap();
		maxHeap.insert(19);
		System.out.println("Max Element : " + maxHeap.getMax());
		maxHeap.printHeap();
		System.out.println("Max Element : " + maxHeap.getMax());
		maxHeap.insert(94);
		maxHeap.printHeap();
		System.out.println("Min Element : " + maxHeap.deleteMin());
		maxHeap.printHeap();
		System.out.println("Min Element : " + maxHeap.deleteMin());
		maxHeap.printHeap();
		System.out.println("Min Element : " + maxHeap.deleteMin());
		maxHeap.printHeap();
		System.out.println("Min Element : " + maxHeap.deleteMin());
		maxHeap.printHeap();
		System.out.println("Min Element : " + maxHeap.deleteMin());
		maxHeap.printHeap();
		System.out.println("Min Element : " + maxHeap.deleteMin());
		maxHeap.printHeap();
		System.out.println("Min Element : " + maxHeap.deleteMin());
		maxHeap.printHeap();
//		System.out.println("Min Element : " + maxHeap.deleteMin());
//		maxHeap.printHeap();

		System.out.println("Min Heap");
		BinaryMinHeap minHeap = new BinaryMinHeap();
		minHeap.insert(5);
		minHeap.printHeap();
		minHeap.insert(3);
		minHeap.printHeap();
		minHeap.insert(17);
		minHeap.printHeap();
		minHeap.insert(10);
		minHeap.printHeap();
		minHeap.insert(84);
		minHeap.printHeap();
		minHeap.insert(19);
		minHeap.printHeap();
		minHeap.insert(6);
		minHeap.printHeap();
		minHeap.insert(22);
		minHeap.printHeap();
		minHeap.insert(9);
		minHeap.printHeap();
	}
}