package classesMain;

public class StairCase {

	// Set Default Values
	static char defaultSymbol1 = '#';
	static char defaultSymbol2 = '%';
	static char defaultInscribedSymbol = ' ';
	static boolean defaultInscribed = true;

	private static void diamond(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			if (count >= 1) {
				String padding = "";
				for (int j = count - 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + symbol);
			}
			if (count > 2) {
				for (int i = 2; i <= count; i++) {
					String padding = "";
					for (int j = 0; j < count - i; j++) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine((2 * i) - 1, symbolIn));
				}
			}

			if (count > 3) {
				for (int i = 0; i < count - 2; i++) {
					String padding = "";
					for (int j = i; j >= 0; j--) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine(2 * (count - 1 - i) - 1, symbolIn));
				}
			}
			if (count >= 2) {
				String padding = "";
				for (int j = count - 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + symbol);
			}
		} else {
			for (int i = 1; i <= count; i++) {
				String padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 0; j < (2 * i) - 1; j++) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
			for (int i = 0; i < count - 1; i++) {
				String padding = "";
				for (int j = i; j >= 0; j--) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 2 * (count - 1 - i) - 1; j > 0; j--) {
					step += symbol;
				}
				System.out.println(padding + step);
			}

		}
	}

	private static void diamond(int count, char... symbolIn) {
		diamond(count, false, symbolIn);
	}

	private static void hourglass(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			String step = "";
			for (int i = (2 * count) - 1; i > 0; i--) {
				step += symbol;
			}
			System.out.println(step);
			if (count > 2) {
				for (int i = count - 1; i > 1; i--) {
					String padding = "";
					for (int j = count - i; j > 0; j--) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine((2 * i) - 1, symbolIn));
				}
			}
			if (count > 1) {
				String padding = "";
				for (int j = count - 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + symbol);
			}
			if (count > 2) {
				for (int i = 2; i <= count - 1; i++) {
					String padding = "";
					for (int j = 0; j < count - i; j++) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine((2 * i) - 1, symbolIn));
				}
			}
			if (count > 1) {
				step = "";
				for (int i = (2 * count) - 1; i > 0; i--) {
					step += symbol;
				}
				System.out.println(step);
			}

		} else {
			for (int i = 0; i < count; i++) {
				String padding = "";
				for (int j = i; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 2 * (count - i) - 1; j > 0; j--) {
					step += symbol;
				}
				System.out.println(padding + step);
			}

			for (int i = 2; i <= count; i++) {
				String padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 0; j < (2 * i) - 1; j++) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
		}
	}

	private static void hourglass(int count, char... symbolIn) {
		hourglass(count, false, symbolIn);
	}

	private static String inscribedLine(int count, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		char inscribedSymbo = symbolIn.length > 1 ? symbolIn[1] : defaultInscribedSymbol;

		String step = "";

		if (count == 0) {
			return step;
		} else if (count == 0) {
			return step + symbol;
		}
		step += symbol;

		for (int i = count - 2; i > 0; i--) {
			step += inscribedSymbo;
		}
		step += symbol;

		return step;
	}

	private static void inscribedSquare(int count, char... symbolIn) {
		inscribedSquare(count, false, symbolIn);
	}

	private static void inscribedSquare(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;

		char symbol2;
		if (inscribed) {
			symbol2 = defaultInscribedSymbol;
		} else {
			symbol2 = symbolIn.length > 1 ? symbolIn[01] : defaultSymbol2;
		}

		for (int i = 1; i <= count; i++) {
			String padding = "";
			for (int j = 0; j < count - i; j++) {
				padding += symbol;
			}
			String step = "";
			for (int j = 0; j < (2 * i) - 1; j++) {
				step += symbol2;
			}
			System.out.println(padding + step + padding);
		}
		for (int i = 0; i < count - 1; i++) {
			String padding = "";
			for (int j = i; j >= 0; j--) {
				padding += symbol;
			}
			String step = "";
			for (int j = 2 * (count - 1 - i) - 1; j > 0; j--) {
				step += symbol2;
			}
			System.out.println(padding + step + padding);
		}
	}

	public static void main(String[] argv) throws Exception {

		// Variable Definition and declaration
		int methodCount = 80;
		int defaultCount = 7;
		char firstSymbol = '@';
		char secondSymbol = '$';

		for (int methodLoopCount = 1; methodLoopCount <= methodCount; methodLoopCount++) {

			switch (methodLoopCount) {
			case 1:
				System.out.format("\nPrint Case %d: Left Justified Triangle Point Down\n", methodLoopCount);
				triangleLeftJustifiedPointDown(defaultCount);
				break;
			case 2:
				System.out.format("\nPrint Case %d: Left Justified Triangle Point Down with Selected Symbol\n",
						methodLoopCount);
				triangleLeftJustifiedPointDown(defaultCount, firstSymbol);
				break;
			case 3:
				System.out.format("\nPrint Case %d: Left Justified Hollow Triangle Point Down\n", methodLoopCount);
				triangleLeftJustifiedPointDown(defaultCount, defaultInscribed);
				break;
			case 4:
				System.out.format("\nPrint Case %d: Left Justified Hollow Triangle Point Down with Selected Symbol\n",
						methodLoopCount);
				triangleLeftJustifiedPointDown(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 5:
				System.out.format("\nPrint Case %d: Left Justified Filled Triangle Point Down with Selected Symbols\n",
						methodLoopCount);
				triangleLeftJustifiedPointDown(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 6:
				System.out.format("\nPrint Case %d: Left Justified Triangle Point Up\n", methodLoopCount);
				triangleLeftJustifiedPointUp(defaultCount);
				break;
			case 7:
				System.out.format("\nPrint Case %d: Left Justified Triangle Point Up with Selected Symbol\n",
						methodLoopCount);
				triangleLeftJustifiedPointUp(defaultCount, firstSymbol);
				break;
			case 8:
				System.out.format("\nPrint Case %d: Left Justified Hollow Triangle Point Up\n", methodLoopCount);
				triangleLeftJustifiedPointUp(defaultCount, defaultInscribed);
				break;
			case 9:
				System.out.format("\nPrint Case %d: Left Justified Hollow Triangle Point Up with Selected Symbol\n",
						methodLoopCount);
				triangleLeftJustifiedPointUp(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 10:
				System.out.format("\nPrint Case %d: Left Justified Filled Triangle Point Up with Selected Symbols\n",
						methodLoopCount);
				triangleLeftJustifiedPointUp(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 11:
				System.out.format("\nPrint Case %d: Right Justified Triangle Point Down\n", methodLoopCount);
				triangleRightJustifiedPointDown(defaultCount);
				break;
			case 12:
				System.out.format("\nPrint Case %d: Right Justified Triangle Point Down with Selected Symbol\n",
						methodLoopCount);
				triangleRightJustifiedPointDown(defaultCount, firstSymbol);
				break;
			case 13:
				System.out.format("\nPrint Case %d: Right Justified Hollow Triangle Point Down\n", methodLoopCount);
				triangleRightJustifiedPointDown(defaultCount, defaultInscribed);
				break;
			case 14:
				System.out.format("\nPrint Case %d: Right Justified Hollow Triangle Point Down with Selected Symbol\n",
						methodLoopCount);
				triangleRightJustifiedPointDown(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 15:
				System.out.format("\nPrint Case %d: Right Justified Filled Triangle Point Down with Selected Symbols\n",
						methodLoopCount);
				triangleRightJustifiedPointDown(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 16:
				System.out.format("\nPrint Case %d: Right Justified Triangle Point Up\n", methodLoopCount);
				triangleRightJustifiedPointUp(defaultCount);
				break;
			case 17:
				System.out.format("\nPrint Case %d: Right Justified Triangle Point Up with Selected Symbol\n",
						methodLoopCount);
				triangleRightJustifiedPointUp(defaultCount, firstSymbol);
				break;
			case 18:
				System.out.format("\nPrint Case %d: Right Justified Hollow Triangle Point Up\n", methodLoopCount);
				triangleRightJustifiedPointUp(defaultCount, defaultInscribed);
				break;
			case 19:
				System.out.format("\nPrint Case %d: Right Justified Hollow Triangle Point Up with Selected Symbol\n",
						methodLoopCount);
				triangleRightJustifiedPointUp(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 20:
				System.out.format("\nPrint Case %d: Right Justified Filled Triangle Point Up with Selected Symbols\n",
						methodLoopCount);
				triangleRightJustifiedPointUp(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 21:
				System.out.format("\nPrint Case %d: Triangle Point Down\n", methodLoopCount);
				trianglePointDown(defaultCount);
				break;
			case 22:
				System.out.format("\nPrint Case %d:Triangle Point Down with Selected Symbol\n", methodLoopCount);
				trianglePointDown(defaultCount, firstSymbol);
				break;
			case 23:
				System.out.format("\nPrint Case %d: Hollow Triangle Point Down Down\n", methodLoopCount);
				trianglePointDown(defaultCount, defaultInscribed);
				break;
			case 24:
				System.out.format("\nPrint Case %d: Hollow Triangle Point Down with Selected Symbol\n",
						methodLoopCount);
				trianglePointDown(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 25:
				System.out.format("\nPrint Case %d: Filled In Triangle Point Down with Selected Symbol\n",
						methodLoopCount);
				trianglePointDown(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 26:
				System.out.format("\nPrint Case %d: Triangle Point Up\n", methodLoopCount);
				trianglePointUp(defaultCount);
				break;
			case 27:
				System.out.format("\nPrint Case %d: Triangle Point Up with Selected Symbol\n", methodLoopCount);
				trianglePointUp(defaultCount, firstSymbol);
				break;
			case 28:
				System.out.format("\nPrint Case %d: Hollow Triangle Point Up\n", methodLoopCount);
				trianglePointUp(defaultCount, defaultInscribed);
				break;
			case 29:
				System.out.format("\nPrint Case %d: Hollow Triangle Point Up with Selected Symbol\n", methodLoopCount);
				trianglePointUp(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 30:
				System.out.format("\nPrint Case %d: Filled In Triangle Point Up with Selected Symbols\n",
						methodLoopCount);
				trianglePointUp(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 31:
				System.out.format("\nPrint Case %d: Left Facing Triangle\n", methodLoopCount);
				trianglePointLeft(defaultCount);
				break;
			case 32:
				System.out.format("\nPrint Case %d: Left Facing Triangle with Selected Symbol\n", methodLoopCount);
				trianglePointLeft(defaultCount, firstSymbol);
				break;
			case 33:
				System.out.format("\nPrint Case %d: Left Facing Hollow Triangle\n", methodLoopCount);
				trianglePointLeft(defaultCount, defaultInscribed);
				break;
			case 34:
				System.out.format("\nPrint Case %d: Left Facing Hollow Triangle with Selected Symbol\n",
						methodLoopCount);
				trianglePointLeft(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 35:
				System.out.format("\nPrint Case %d: Left Facing Filled In Triangle with Selected Symbols\n",
						methodLoopCount);
				trianglePointLeft(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 36:
				System.out.format("\nPrint Case %d: Right Facing Triangle\n", methodLoopCount);
				trianglePointRight(defaultCount);
				break;
			case 37:
				System.out.format("\nPrint Case %d: Right Facing Triangle with Selected Symbol\n", methodLoopCount);
				trianglePointRight(defaultCount, firstSymbol);
				break;
			case 38:
				System.out.format("\nPrint Case %d: Right Facing Hollow Triangle\n", methodLoopCount);
				trianglePointRight(defaultCount, defaultInscribed);
				break;
			case 39:
				System.out.format("\nPrint Case %d: Right Facing Hollow Triangle with Selected Symbol\n",
						methodLoopCount);
				trianglePointRight(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 40:
				System.out.format("\nPrint Case %d: Right Facing Filled In Triangle with Selected Symbols\n",
						methodLoopCount);
				trianglePointRight(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 41:
				System.out.format("\nPrint Case %d: Right Leaning Parallelogram\n", methodLoopCount);
				parallelogramPointRight(defaultCount);
				break;
			case 42:
				System.out.format("\nPrint Case %d: Right Leaning Parallelogram with Selected Symbol\n",
						methodLoopCount);
				parallelogramPointRight(defaultCount, firstSymbol);
				break;
			case 43:
				System.out.format("\nPrint Case %d: Right Leaning Hollow Parallelogram\n", methodLoopCount);
				parallelogramPointRight(defaultCount, defaultInscribed);
				break;
			case 44:
				System.out.format("\nPrint Case %d: Right Leaning Hollow Parallelogram with Selected Symbol\n",
						methodLoopCount);
				parallelogramPointRight(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 45:
				System.out.format("\nPrint Case %d: Right Leaning Filled In Parallelogram with Selected Symbols\n",
						methodLoopCount);
				parallelogramPointRight(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;
			case 46:
				System.out.format("\nPrint Case %d: Left Leaning Parallelogram\n", methodLoopCount);
				parallelogramPointLeft(defaultCount);
				break;
			case 47:
				System.out.format("\nPrint Case %d: Left Leaning Parallelogram with Selected Symbol\n",
						methodLoopCount);
				parallelogramPointLeft(defaultCount, firstSymbol);
				break;
			case 48:
				System.out.format("\nPrint Case %d: Left Leaning Hollow Parallelogram\n", methodLoopCount);
				parallelogramPointLeft(defaultCount, defaultInscribed);
				break;
			case 49:
				System.out.format("\nPrint Case %d: Left Leaning Hollow Parallelogram with Selected Symbol\n",
						methodLoopCount);
				parallelogramPointLeft(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 50:
				System.out.format("\nPrint Case %d: Left Leaning Filled In Parallelogram with Selected Symbols\n",
						methodLoopCount);
				parallelogramPointLeft(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;
			case 51:
				System.out.format("\nPrint Case %d: Diamond\n", methodLoopCount);
				diamond(defaultCount);
				break;
			case 52:
				System.out.format("\nPrint Case %d: Diamond with Selected Symbol\n", methodLoopCount);
				diamond(defaultCount, firstSymbol);
				break;
			case 53:
				System.out.format("\nPrint Case %d: Hollow Diamond\n", methodLoopCount);
				diamond(defaultCount, defaultInscribed);
				break;
			case 54:
				System.out.format("\nPrint Case %d: Hollow Diamond with Selected Symbol\n", methodLoopCount);
				diamond(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 55:
				System.out.format("\nPrint Case %d: Filled In Diamond with Selected Symbols\n", methodLoopCount);
				diamond(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 56:
				System.out.format("\nPrint Case %d: Hourglass\n", methodLoopCount);
				hourglass(defaultCount);
				break;
			case 57:
				System.out.format("\nPrint Case %d: Hourglass with Selected Symbol\n", methodLoopCount);
				hourglass(defaultCount, firstSymbol);
				break;
			case 58:
				System.out.format("\nPrint Case %d: Hollow Hourglass\n", methodLoopCount);
				hourglass(defaultCount, defaultInscribed);
				break;
			case 59:
				System.out.format("\nPrint Case %d: Hollow Hourglass with Selected Symbol\n", methodLoopCount);
				hourglass(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 60:
				System.out.format("\nPrint Case %d: Filled In Hourglass with Selected Symbols\n", methodLoopCount);
				hourglass(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			case 61:
				System.out.format("\nPrint Case %d: X Square\n", methodLoopCount);
				xSquare(defaultCount);
				break;
			case 62:
				System.out.format("\nPrint Case %d: X Square with Selected Symbol\n", methodLoopCount);
				xSquare(defaultCount, firstSymbol);
				break;
			case 63:
				System.out.format("\nPrint Case %d: Filled In X Square with Selected Symbols\n", methodLoopCount);
				xSquare(defaultCount, firstSymbol, secondSymbol);
				break;

			case 64:
				System.out.format("\nPrint Case %d: Split Square Point Down\n", methodLoopCount);
				splitSquarePointDown(defaultCount);
				break;
			case 65:
				System.out.format("\nPrint Case %d: Split Square Point Down with Selected Symbol\n", methodLoopCount);
				splitSquarePointDown(defaultCount, firstSymbol);
				break;
			case 66:
				System.out.format("\nPrint Case %d: Split Square Point Down with Selected Symbols\n", methodLoopCount);
				splitSquarePointDown(defaultCount, firstSymbol, secondSymbol);
				break;

			case 67:
				System.out.format("\nPrint Case %d: Split Square Point Up\n", methodLoopCount);
				splitSquarePointUp(defaultCount);
				break;
			case 68:
				System.out.format("\nPrint Case %d: y Point Up with Selected Symbol\n", methodLoopCount);
				splitSquarePointUp(defaultCount, firstSymbol);
				break;
			case 69:
				System.out.format("\nPrint Case %d: Split Square Point Up with Selected Symbols\n", methodLoopCount);
				splitSquarePointUp(defaultCount, firstSymbol, secondSymbol);
				break;
			case 70:
				System.out.format("\nPrint Case %d: Inscribed Square\n", methodLoopCount);
				inscribedSquare(defaultCount);
				break;
			case 71:
				System.out.format("\nPrint Case %d: Inscribed Hollow Square\n", methodLoopCount);
				inscribedSquare(defaultCount, defaultInscribed);
				break;
			case 72:
				System.out.format("\nPrint Case %d: Inscribed Hollow Square with Selected Symbols\n", methodLoopCount);
				inscribedSquare(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 73:
				System.out.format("\nPrint Case %d: Inscribed Square Point Up with Selected Symbol\n", methodLoopCount);
				inscribedSquare(defaultCount, firstSymbol);
				break;
			case 74:
				System.out.format("\nPrint Case %d: Inscribed Square Point Up with Selected Symbols\n",
						methodLoopCount);
				inscribedSquare(defaultCount, firstSymbol, secondSymbol);
				break;
			case 75:
				System.out.format("\nPrint Case %d: Hexagobn\n", methodLoopCount);
				hexagon(defaultCount);
				break;
			case 76:
				System.out.format("\nPrint Case %d: Hexagobn with Selected Symbol\n", methodLoopCount);
				hexagon(defaultCount, firstSymbol);
				break;
			case 77:
				System.out.format("\nPrint Case %d: Hollow Hexagobn\n", methodLoopCount);
				hexagon(defaultCount, defaultInscribed);
				break;
			case 78:
				System.out.format("\nPrint Case %d: Hollow Hexagobn with Selected Symbol\n", methodLoopCount);
				hexagon(defaultCount, defaultInscribed, firstSymbol);
				break;
			case 79:
				System.out.format("\nPrint Case %d: Filled Hexagobn with Selected Symbols\n", methodLoopCount);
				hexagon(defaultCount, defaultInscribed, firstSymbol, secondSymbol);
				break;

			// hexagon
			// Inscribed hexagon

			default:
				System.out.format("\nPrint Case %d:Invalid", methodLoopCount);
				break;
			}
		}
	}

	private static void hexagon(int count, char... symbolIn) {
		hexagon(count, false, symbolIn);
	}

	private static void hexagon(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		char symbol2 = symbolIn.length > 01 ? symbolIn[1] : defaultInscribedSymbol;

		String base = "";
		String baseInner = "";
		String padding = "";
		String step = "";

		if (inscribed) {
			for (int i = 0; i < count; i++) {
				base += symbol;
				baseInner += symbol2;
			}
			for (int i = 1; i < count; i++) {
				padding += defaultInscribedSymbol;
			}
			System.out.println(padding + base);
			for (int i = 1; i < count; i++) {
				padding = "";
				for (int j = 1; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				step = "";
				for (int j = 1; j < i; j++) {
					step += symbol2;
				}
				System.out.println(padding + symbol + step + baseInner + step + symbol);
			}
			for (int i = 1; i < count; i++) {
				System.out.println(padding + symbol + step + baseInner + step + symbol);
			}

			for (int i = 0; i < count - 2; i++) {
				padding = "";
				for (int j = i + 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				step = "";
				for (int j = count - i - 3; j > 0; j--) {
					step += symbol2;
				}
				System.out.println(padding + symbol + step + baseInner + step + symbol);
			}

			padding = "";
			for (int i = 1; i < count; i++) {
				padding += defaultInscribedSymbol;
			}

			System.out.println(padding + base);

		} else {
			for (int i = 0; i < count; i++) {
				base += symbol;
			}
			for (int i = 1; i < count; i++) {
				padding += defaultInscribedSymbol;
			}
			System.out.println(padding + base);
			for (int i = 1; i < count; i++) {
				padding = "";
				for (int j = 1; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				step = "";
				for (int j = 0; j < i; j++) {
					step += symbol;
				}
				System.out.println(padding + step + base + step);
			}
			for (int i = 1; i < count; i++) {
				System.out.println(padding + step + base + step);
			}
			for (int i = 0; i < count - 1; i++) {
				padding = "";
				for (int j = i + 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				step = "";
				for (int j = count - i - 2; j > 0; j--) {
					step += symbol;
				}
				System.out.println(padding + step + base + step);
			}

		}
	}

	private static void parallelogramPointLeft(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			String padding = "";
			for (int j = 0; j < count - 1; j++) {
				padding += defaultInscribedSymbol;
			}
			System.out.println(padding + symbol);
			for (int i = 2; i <= count; i++) {
				padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + inscribedLine(i, symbolIn));
			}
			for (int i = count - 1; i > 1; i--) {
				System.out.println(inscribedLine(i, symbolIn));
			}
			System.out.println(symbol);
		} else {
			for (int i = 1; i <= count; i++) {
				String padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 0; j < i; j++) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
			for (int i = 1; i < count; i++) {
				String step = "";
				for (int j = count - i; j > 0; j--) {
					step += symbol;
				}
				System.out.println(step);
			}
		}
	}

	private static void parallelogramPointLeft(int count, char... symbolIn) {
		parallelogramPointLeft(count, false, symbolIn);
	}

	private static void parallelogramPointRight(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			System.out.println(symbol);
			for (int i = 2; i <= count; i++) {
				System.out.println(inscribedLine(i, symbolIn));
			}
			String padding = "";
			for (int i = count - 1; i > 1; i--) {
				padding = "";
				for (int j = count - i; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + inscribedLine(i, symbolIn));
			}
			padding = "";
			for (int j = 0; j < count - 1; j++) {
				padding += defaultInscribedSymbol;
			}
			System.out.println(padding + symbol);
		} else {
			for (int i = 1; i <= count; i++) {
				String step = "";
				for (int j = 0; j < i; j++) {
					step += symbol;
				}
				System.out.println(step);
			}
			for (int i = 1; i < count; i++) {
				String padding = "";
				for (int j = 0; j < i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = count - i; j > 0; j--) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
		}
	}

	private static void parallelogramPointRight(int count, char... symbolIn) {
		parallelogramPointRight(count, false, symbolIn);
	}

	private static void splitSquarePointDown(int count, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		char symbol2 = symbolIn.length > 1 ? symbolIn[1] : defaultSymbol2;

		for (int i = 0; i < count; i++) {
			String padding = "";
			for (int j = i; j > 0; j--) {
				padding += symbol2;
			}
			String step = "";
			for (int j = count - i; j > 0; j--) {
				step += symbol;
			}
			System.out.println(padding + step);
		}
	}

	private static void splitSquarePointUp(int count, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		char symbol2 = symbolIn.length > 1 ? symbolIn[1] : defaultSymbol2;
		for (int i = 1; i <= count; i++) {
			String padding = "";
			for (int j = 0; j < count - i; j++) {
				padding += symbol2;
			}
			String step = "";
			for (int j = 0; j < i; j++) {
				step += symbol;
			}
			System.out.println(padding + step);
		}
	}

	private static void triangleLeftJustifiedPointDown(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			String step = "";
			for (int i = count; i > 0; i--) {
				step += symbol;
			}
			System.out.println(step);
			if (count > 2) {
				for (int i = count - 1; i > 1; i--) {
					System.out.println(inscribedLine(i, symbolIn));
				}
			}
			if (count > 1) {
				System.out.println(symbol);
			}
		} else {
			for (int i = 0; i < count; i++) {
				String step = "";
				for (int j = count - i; j > 0; j--) {
					step += symbol;
				}
				System.out.println(step);
			}
		}
	}

	private static void triangleLeftJustifiedPointDown(int count, char... symbolIn) {
		triangleLeftJustifiedPointDown(count, false, symbolIn);
	}

	private static void triangleLeftJustifiedPointUp(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			String step = "";
			if (count > 0) {
				System.out.println(symbol);
			}
			if (count > 1) {
				for (int i = 2; i < count; i++) {
					System.out.println(inscribedLine(i, symbolIn));
				}
			}
			if (count > 2) {
				step = "";
				for (int i = 0; i < count; i++) {
					step += symbol;
				}
				System.out.println(step);
			}
		} else {
			for (int i = 1; i <= count; i++) {
				String step = "";
				for (int j = 0; j < i; j++) {
					step += symbol;
				}
				System.out.println(step);
			}
		}
	}

	private static void triangleLeftJustifiedPointUp(int count, char... symbolIn) {
		triangleLeftJustifiedPointUp(count, false, symbolIn);
	}

	private static void trianglePointDown(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			String step = "";
			for (int i = (2 * count) - 1; i > 0; i--) {
				step += symbol;
			}
			System.out.println(step);
			if (count > 2) {
				for (int i = count - 1; i > 1; i--) {
					String padding = "";
					for (int j = count - i; j > 0; j--) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine((2 * i) - 1, symbolIn));
				}
			}
			if (count > 1) {
				String padding = "";
				for (int j = count - 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + symbol);
			}
		} else {
			for (int i = 0; i < count; i++) {
				String padding = "";
				for (int j = i; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 2 * (count - i) - 1; j > 0; j--) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
		}
	}

	private static void trianglePointDown(int count, char... symbolIn) {
		trianglePointDown(count, false, symbolIn);
	}

	private static void trianglePointLeft(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			String padding = "";
			for (int j = 0; j < count - 1; j++) {
				padding += defaultInscribedSymbol;
			}
			System.out.println(padding + symbol);
			for (int i = 2; i <= count; i++) {
				padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + inscribedLine(i, symbolIn));
			}
			for (int i = count - 1; i > 1; i--) {
				padding = "";
				for (int j = count - i; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + inscribedLine(i, symbolIn));
			}
			padding = "";
			for (int j = 0; j < count - 1; j++) {
				padding += defaultInscribedSymbol;
			}
			System.out.println(padding + symbol);
		} else {
			for (int i = 1; i <= count; i++) {
				String padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 0; j < i; j++) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
			for (int i = 1; i < count; i++) {
				String padding = "";
				for (int j = 0; j < i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = count - i; j > 0; j--) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
		}
	}

	private static void trianglePointLeft(int count, char... symbolIn) {
		trianglePointLeft(count, false, symbolIn);
	}

	private static void trianglePointRight(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {

			System.out.println(symbol);
			for (int i = 2; i <= count; i++) {
				System.out.println(inscribedLine(i, symbolIn));
			}
			for (int i = count - 1; i > 1; i--) {

				System.out.println(inscribedLine(i, symbolIn));
			}

			System.out.println(symbol);
		} else {
			for (int i = 1; i <= count; i++) {
				String step = "";
				for (int j = 0; j < i; j++) {
					step += symbol;
				}
				System.out.println(step);
			}
			for (int i = 1; i < count; i++) {
				String step = "";
				for (int j = count - i; j > 0; j--) {
					step += symbol;
				}
				System.out.println(step);
			}
		}
	}

	private static void trianglePointRight(int count, char... symbolIn) {
		trianglePointRight(count, false, symbolIn);
	}

	private static void trianglePointUp(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			if (count >= 1) {
				String padding = "";
				for (int j = count - 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + symbol);
			}
			if (count > 2) {
				for (int i = 2; i <= count - 1; i++) {
					String padding = "";
					for (int j = 0; j < count - i; j++) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine((2 * i) - 1, symbolIn));
				}
			}
			if (count > 1) {
				String step = "";
				for (int i = (2 * count) - 1; i > 0; i--) {
					step += symbol;
				}
				System.out.println(step);
			}
		} else {
			for (int i = 1; i <= count; i++) {
				String padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 0; j < (2 * i) - 1; j++) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
		}
	}

	private static void trianglePointUp(int count, char... symbolIn) {
		trianglePointUp(count, false, symbolIn);
	}

	private static void triangleRightJustifiedPointDown(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;

		if (inscribed) {
			String step = "";
			for (int i = count; i > 0; i--) {
				step += symbol;
			}
			System.out.println(step);
			if (count > 2) {
				for (int i = count - 1; i > 1; i--) {
					String padding = "";
					for (int j = count - i; j > 0; j--) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine(i, symbolIn));
				}
			}
			if (count > 1) {
				String padding = "";
				for (int j = count - 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + symbol);
			}
		} else
			for (int i = 0; i < count; i++) {
				String padding = "";
				for (int j = i; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = count - i; j > 0; j--) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
	}

	private static void triangleRightJustifiedPointDown(int count, char... symbolIn) {
		triangleRightJustifiedPointDown(count, false, symbolIn);
	}

	private static void triangleRightJustifiedPointUp(int count, boolean inscribed, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		if (inscribed) {
			if (count >= 1) {
				String padding = "";
				for (int j = count - 1; j > 0; j--) {
					padding += defaultInscribedSymbol;
				}
				System.out.println(padding + symbol);
			}
			if (count > 2) {
				for (int i = 2; i <= count - 1; i++) {
					String padding = "";
					for (int j = 0; j < count - i; j++) {
						padding += defaultInscribedSymbol;
					}
					System.out.println(padding + inscribedLine(i, symbolIn));
				}
			}
			if (count > 1) {
				String step = "";
				for (int i = count; i > 0; i--) {
					step += symbol;
				}
				System.out.println(step);
			}
		} else {
			for (int i = 1; i <= count; i++) {
				String padding = "";
				for (int j = 0; j < count - i; j++) {
					padding += defaultInscribedSymbol;
				}
				String step = "";
				for (int j = 0; j < i; j++) {
					step += symbol;
				}
				System.out.println(padding + step);
			}
		}
	}

	private static void triangleRightJustifiedPointUp(int count, char... symbolIn) {
		triangleRightJustifiedPointUp(count, false, symbolIn);
	}

	private static void xSquare(int count, char... symbolIn) {
		char symbol = symbolIn.length > 0 ? symbolIn[0] : defaultSymbol1;
		char symbol2 = symbolIn.length > 1 ? symbolIn[1] : defaultSymbol2;

		for (int i = 0; i < count; i++) {
			String padding = "";
			for (int j = i; j > 0; j--) {
				padding += symbol2;
			}
			String step = "";
			for (int j = 2 * (count - i) - 1; j > 0; j--) {
				step += symbol;
			}
			System.out.println(padding + step + padding);
		}

		for (int i = 2; i <= count; i++) {
			String padding = "";
			for (int j = 0; j < count - i; j++) {
				padding += symbol2;
			}
			String step = "";
			for (int j = 0; j < (2 * i) - 1; j++) {
				step += symbol;
			}
			System.out.println(padding + step + padding);
		}
	}

}
