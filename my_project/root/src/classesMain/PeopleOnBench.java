package classesMain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import classesInstance.Tuple;

public class PeopleOnBench {

	// Comparator to sort by value
	static Comparator<Tuple<Integer, Integer>> compairGaps = new Comparator<Tuple<Integer, Integer>>() {

		@Override
		public int compare(Tuple<Integer, Integer> o1, Tuple<Integer, Integer> o2) {
			Integer v1 = o1.leftData - o1.rightData;
			Integer v2 = o2.leftData - o2.rightData;
			return v1.compareTo(v2);
		}
	};

	private static int benchSeats = 0;
	private static boolean benchLeftEdge = true;
	private static boolean benchRightEdge = true;
	private static List<Tuple<Integer, Integer>> gaps;
	private static List<Integer> people;

	private static void buildBench(int seats) {
		benchSeats = seats;
	}

	private static void getDescription() {
		System.out.println(
				"Given a bench with n seats and few people sitting, tell the seat number each time when a new person goes to sit on the bench such that his distance from others is maximum..");
	}

	public static void main(String[] argv) {
		getDescription();
		buildBench(10);
		personSits();
		personSits();
		personSits();
		personSits();
		personSits();
		personSits();
		personSits();
		personSits();
		personSits();
		personSits();
		personSits();

	}

	private static void personSits() {
		if (people == null) {
			people = new ArrayList<Integer>();
			int seat = new Random().nextInt(benchSeats) + 1;
			people.add(seat);
			// Edge case checks
			if (seat == 1) {
				benchLeftEdge = false;
				System.out.format("The first person sits at seat '%d' (the far left edge)\n", seat);
			} else if (seat == (benchSeats)) {
				benchRightEdge = false;
				System.out.format("The first person sits at seat '%d' (the far right edge)\n", seat);
			}
			else{
				System.out.format("The first person sits at seat '%d'\n", seat);
			}
		}
		else if (people.size()==benchSeats){
			System.out.println("Sorry, the bench is full");
			return;
		}
		else if (gaps == null) {
			gaps = new ArrayList<Tuple<Integer, Integer>>();
			int firstSeat = people.get(0);
			if ((firstSeat) > (benchSeats - firstSeat)) {
				benchLeftEdge = false;
				gaps.add(new Tuple<Integer, Integer>(1, firstSeat));
				people.add(1);
				System.out.format(
						"The 2nd person sits at seat '%d' (the far left edge), '%d' spaces from the person to his right\n",
						1, firstSeat - 1);
			} else {
				benchRightEdge = false;
				gaps.add(new Tuple<Integer, Integer>(firstSeat, benchSeats));
				people.add(benchSeats);
				System.out.format(
						"The 2nd person sits at seat '%d' (the far right edge), '%d' spaces from the person to his left\n",
						benchSeats, benchSeats - firstSeat);
			}
		} else {
			Collections.sort(gaps, compairGaps);
			int gap = gaps.get(0).rightData - gaps.get(0).leftData - 1;
			// Edge case. a edge is still open.
			if (benchLeftEdge) {
				int gap2 = gaps.get(0).leftData - 1;
				if (gap2 >= gap) {
					benchLeftEdge = false;
					gaps.add(new Tuple<Integer, Integer>(1, gaps.get(0).leftData));
					people.add(1);
					System.out.format(
							"Person '%d' sits at seat '%d' (the far left edge), '%d' spaces from the person to his right n",
							people.size(), 1, gap2);
					return;
				}
			} else if (benchRightEdge) {
				int gap2 = benchSeats - gaps.get(0).rightData;
				if (gap2 >= gap) {
					benchRightEdge = false;
					gaps.add(new Tuple<Integer, Integer>(gaps.get(0).rightData, benchSeats));
					people.add(benchSeats);
					System.out.format(
							"Person '%d' sits at seat '%d' (the far right edge), '%d' spaces from the person to his left\n",
							people.size(), benchSeats, gap2);
					return;
				}
			}
			// normal operation
			Tuple<Integer, Integer> origianalGap = gaps.remove(0);
			int newSeat = (origianalGap.leftData + origianalGap.rightData) / 2;
			gaps.add(new Tuple<Integer, Integer>(origianalGap.leftData, newSeat));
			gaps.add(new Tuple<Integer, Integer>(newSeat, origianalGap.rightData));
			people.add(newSeat);

			System.out.format(
					"Person '%d' sits at seat '%d', '%d' spaces from the person to his left and '%d' spaces from the person to his right\n",
					people.size(), newSeat, newSeat - origianalGap.leftData, origianalGap.rightData - newSeat);
		}
	}
}
